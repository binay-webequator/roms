# JC OMS Royale

## Introduction 

This application is for get data from 3PL API.

## Software Requirement

 - PHP >= 7.2.0
 - MySQL 5.5
 - Apache 2.2 OR IIS 7 onward
 - Laravel 6.*
 - Laravel Passport
 - Nodejs
 
## PHP Extensions 

  Followings extensions must be enabled 
    
- php_intl.dll
- php_pdo_mysql.dll
- php_openssl.dll
- php_ftp.dll
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

## Apache Modules
- LoadModule ssl_module modules/mod_ssl.so
- LoadModule rewrite_module modules/mod_rewrite.so


##Installation  

if you are using the AWS ec2 instance

First following instructions in installation/production-server-build.md


### Library Installation 
 run `composer install`  to install laravel and other required libraries
 run `npm install` to install node modules and vuejs 

### Configuration of database and local setting
 - copy the `env.example` to `.env.{PC-NAME}` and change parameters e.g. database, email and ftp setting
 - create application key `php artisan key:generate`
 - For database schema run command (`php artisan migrate`)
 - To install passport ```php artisan passport:install```
 - update 3PL configuration in `evn.{system-name}`

```
 3PL_API_BASE_URL=https://secure-wms.com
 3PL_API_GRANT_TYPE=client_credentials
 3PL_API_TPL="{API-TPL}"
 3PL_API_USERNAME=api-username
 3PL_API_PASSWORD=api-password
 3PL_API_USER_LOGIN_ID=api-user-id
 pushed for this customer only
 3PL_API_CUSTOMER_ID=customer-id
 3PL_API_FACILITY_ID=customer-warehouse-id
```


## For cache

- Default cache is file type
- For redis do changes in `.env.{PC-NAME}` and changes `CACHE_DRIVER=redis`


## Cron Commands
- For live to run api set `APP_EXE_CURL` value to true in .ENV.{system} file
- Go to project root and run the command:

- `php artisan product:import` (Get products from 3PL and save in table)
- `php artisan order:import` (Get orders from 3PL and save in table)
- `php artisan asn:import` (Get asns from 3PL and save in table)
- `php artisan dpd:order-report` (send email for admin persons of orders updated on last 24 hours)
- `php artisan dpd:tracking` (Get orders tracking from 3PL and save in table)


