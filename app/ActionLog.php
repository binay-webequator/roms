<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class ActionLog extends Model
{
    use Notifiable;
    protected $table = 'action_logs';
    public $timestamps = true;

    /**hasone to user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(){
        return $this->hasOne(\App\User::class,'id', 'user_id');
    }

    /**
     * @param $param
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Exception
     */
    public static function getNotifications($param){
        DB::enableQueryLog();
        try {

            $per_page = 50;
            $order_by = 'id';
            $sort_by = 'desc';
            if(!empty($param['limit'])) {
                $per_page =  $param['limit'];
            }

            if(!empty($param['byColumn'])) {
                $sort_by = $param['byColumn'];
            }

            if(!empty($param['orderBy'])) {
                $param['orderBy'] = str_replace("__slot:",'',$param['orderBy']);
                $order_by = $param['orderBy'];
            }

            $notifs =  self::with('user')->whereRaw('1=1')->whereHas('user', function($q) use($param) {
                if(!empty($param['user_name'])) {
                    $name = trim($param['user_name']);
                    $ex_name=explode(' ',$name);
                    $name = implode('',$ex_name);
                    $q->where("name", 'like', '%' . $name . '%');
                    if($param['orderBy'] == "userName") {
                        $q->orderBy('name', $param['byColumn']);
                    }
                }
            });

            // status filter
            if(isset($param->status)) {
                $notifs->where('status', $param['status']);
            }

            if(!empty($param['startDate']) && !empty($param['endDate'])) {
                $notifs->whereBetween('created_at', [date('Y-m-d H:i', strtotime($param['startDate'])), date('Y-m-d H:i', strtotime($param['endDate']))]);
            }else{
                if(!empty($param['startDate'])){
                    $notifs->where('created_at', '>=', date('Y-m-d H:i', strtotime($param['startDate'])));
                }
                else if(!empty($param['endDate'])){
                    $notifs->where('created_at', '<=', date('Y-m-d H:i', strtotime($param['endDate'])));
                }
            }

            if($order_by!='userName') {
                $notifs->orderBy($order_by, $sort_by);
            }

            return $notifs->paginate($per_page);

        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}
