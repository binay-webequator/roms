<?php

namespace App\Console\Commands\FTP;

use App\Traits\Util;
use App\Traits\ThreePL;
use Illuminate\Console\Command;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Storage;

class ftpSyn extends Command
{
    use Util, ThreePL;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ftp:syn {m?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download and Upload csv files to FTP server';

    /**
    * The storage driver, possible values : ftp, sftp
    */
    protected $storageDriver = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->storageDriver = Flysystem::connection(Flysystem::getDefaultConnection());

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // command to run //
        $m = $this->argument('m');
        // check and set config//
        $this->apiConfigValidation();
        $this->threePLConfigValidation();
        // connection ftp and download files //
        if (empty($this->storageDriver)) {
            $this->error('FTP connection driver not defined');
            exit(0);
        }

        if(strtolower($m) == 'in') { //download files ftp server //
            $this->info('Downloading files start....');
            $this->downloadFiles($this->ftp_in_folder);
            $this->info('Downloading files end ....');

        } else if(strtolower($m) == 'out') { //upload files ftp server //
            //upload files ftp server //
            $this->info('Moving files from local to FTP start....');
            $this->upload($this->ftp_out_folder);
            $this->info('Moving files from local to FTP end....');

        } else {

            //download files ftp server //
            //$this->info('Downloading files start....');
            //$this->downloadFiles($this->ftp_in_folder);
            //$this->info('Downloading files end ....');

            //upload files ftp server //
            $this->info('Moving files from local to FTP start....');
            $this->upload($this->ftp_out_folder);
            $this->info('Moving files from local to FTP end....');
        }
    }

    /**
     * Will download the All files from a given folder to local, default directory is root (/)
     * @param string $directory
     *
     */
    private function downloadFiles($directory='/') {

        try{
            $files = $this->storageDriver->listContents($directory);
            if(!empty($files)) {
                foreach ($files as $key => $file) {
                    if($file['type'] == 'file') {
                        $this->downloadFile($file);
                        $this->moveToArchive($file);
                    }
                }
            }
        }catch (\Exception $e){
            $this->error($e->getMessage());
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['ftp_down'],
                    'subject'=>'FtpSyn',
                    'message'=>$e->getMessage()
                ]
            );
        }


    }

    /** Download single from ftp to local
     * @param $file
     * @return bool
     */
    private function downloadFile($file) {

        try {
            $this->info($file['path']." - Downloading ...");
            Storage::disk('local')->put($file['path'], $this->storageDriver->read($file['path']));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'FtpSyn',
                    'message'=>$e->getMessage()
                ]
            );
        }

        return true;

    }

    /**
     * Move file to archive folder
     * @param $file
     * @param string $archiveDir
     * @return bool
     */
    private function moveToArchive($file, $archiveDir='Archive'){
        try {
            $this->info($file['path']." - Moving to archive ...");
            $archive_dir_path = $file['dirname'].DIRECTORY_SEPARATOR.$archiveDir.DIRECTORY_SEPARATOR;

            // has archive folder
            if(!$this->storageDriver->has($archive_dir_path)) {
                if(!$this->storageDriver->createDir($archive_dir_path)) { // dir created
                    $this->sendSystemErrorEmail(
                        [
                            'to'=>$this->api_emails['exception'],
                            'subject'=>'FtpSyn',
                            'message'=>'Not able to create Archive dir('.$archive_dir_path.') on FTP server'
                        ]
                    );
                    throw new \Exception('Not able to create Archive dir('.$archive_dir_path.') on FTP server');
                }
            }
            $this->storageDriver->rename($file['path'], $archive_dir_path.$file['basename'].'.'.date('YmdHis'));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        return true;
    }

    /**
     * upload file to FTP server
     * @param $dir
     */
    private function upload($dir) {
        $outpath = $filepath = storage_path('app').DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR;
        if(!file_exists($outpath)) {
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'FtpSyn',
                    'message'=>$outpath." Dir does not exits"
                ]
            );
            exit(0);
        }

        $xml_files = [];
        $this->getDirFiles($outpath, $xml_files);

        if(!empty($xml_files)) {

            // get server file list to check duplicate file name //
            $ftp_files = $this->storageDriver->listContents($this->ftp_out_folder);
            $ftp_xml_files = [];
            if(!empty($ftp_files)) {
                $ftp_xml_files = array_column($ftp_files, 'basename');
            }

            foreach ($xml_files as $file) {
                try {
                    $finfo = pathinfo($file);

                    // read file //
                    $handle = fopen($file, 'r');
                    $file_size = filesize($file);
                    $resources = fread($handle, $file_size);
                    fclose($handle);

                    // upload to ftp server //
                    $filename = $this->getFilename($ftp_xml_files, $finfo['basename']);
                    $this->storageDriver->put(DIRECTORY_SEPARATOR.$filename, $resources);

                    // move to local archive //
                    $archive_path = $finfo['dirname'].DIRECTORY_SEPARATOR.'Archive'.DIRECTORY_SEPARATOR;
                    if(!file_exists($archive_path)) {
                        $this->createFolder($archive_path);
                    }
                    rename($file, $archive_path.$finfo['basename'].'.'.date('YmdHis'));
                    $this->info($file.' - uploaded successfully');
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
                    $this->sendSystemErrorEmail(
                        [
                            'to'=>$this->api_emails['exception'],
                            'subject'=>'FtpSyn',
                            'message'=>$e
                        ]
                    );
                }
            }

        } else {
            $this->warn('No xlsx file found');
        }
    }

}