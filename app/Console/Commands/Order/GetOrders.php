<?php

namespace App\Console\Commands\Order;

use Illuminate\Console\Command;
use App\Lib\Order\GetOrders as PullOrders;

class GetOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get orders from 3PL and save in table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get orders api //
         $order = new PullOrders();
         $order->init();
    }
}
