<?php

namespace App\Console\Commands\Order;

use App\Traits\Order;
use Illuminate\Console\Command;

class ImportCommand extends Command
{
    public $csv_path = null;
    use Order;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read csv files form IN/orders folder, create 3pl data and queue to create order job ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // set config //
        $this->apiConfigValidation();
        $this->threePLConfigValidation();
        $this->csv_path = storage_path('/app/').$this->ftp_in_folder.'/Orders/';

        // read and process csv file //
        try {
            $this->readAndProcessFiles();

        } catch (\Exception $e) {
            report($e);
        }


        return 0;
    }

    /**
     * read order csv files and process them
     * @return bool
     */
    public function readAndProcessFiles()
    {
        $csv_files = [];
        $this->getDirFiles($this->csv_path, $csv_files, '', 'csv');

        if(!empty($csv_files)) {

            foreach ($csv_files as $file) {
                $this->processFile($file);
            }

        } else {
            echo "No csv file found";
        }

        return true;
    }


}
