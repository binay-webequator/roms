<?php

namespace App\Console\Commands\Product;

use Illuminate\Console\Command;
use App\Lib\Product\GetProducts as ProductImport;

class GetProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get products from 3PL and save in table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            // start product
            $product = new ProductImport();
            $product->init();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
