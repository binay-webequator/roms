<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Foundation\Exceptions\Handler as baseHandler;
use App\Mail\JamesCargoEmail;
use Illuminate\Support\Facades\Mail;

class ExceptionHandler extends baseHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        // check if we should mail this exception
        if ($this->shouldMail($exception)) {

            $this->mailException($exception);
        }

        parent::report($exception);
    }

    /**
     * Determine if the exception should be mailed
     *
     * @param Throwable $exception
     * @return bool
     */
    protected function shouldMail(Throwable $exception)
    {
        // exception email is empty in the config or should not report
        if (empty(config('threepl.api_emails.exception')) || $this->shouldntReport($exception)) {
            return false;
        }

        return true;
    }

    /**
     * mail the exception
     *
     * @param Throwable $exception
     */
    protected function mailException(Throwable $exception)
    {

        $params = [
            'subject' => config('app.name').' - Some thing went wrong',
            'template' => 'jc',
            'message' => $exception
        ];

        Mail::to(config('threepl.api_emails.exception'))->send(new JamesCargoEmail($params));
    }

}
