<?php

namespace App\Http\Controllers\API\V1;

use App\ActionLog;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\BatchOrders;
use App\Jobs\PushBatchesToThreePL;
use Carbon\Carbon;


class HomeController extends Controller
{
    protected $batch_jobs=[
        'BatchOrders'=>'Batch Orders',
        'PushBatchesToThreePL'=>'Push Batches To 3PL'
    ];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Save Queue jobs and save to process queue result
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function queues(Request $request){

        $batchno = time();
        $res = [
           "status" => 400,
           "msg" => ''
        ];

        // create and set batch no //
        try {

            if(!empty($request->bt)) {
                $bdate = '';
                if(!empty($request->bd)) {
                    $bdates = explode("|", $request->bd);
                    if(!empty($bdates)) {
                        $b= [];
                        foreach ($bdates as $bd) {
                            $b[] = Carbon::parse($bd)->format("Y-m-d");
                        }
                        $bdate = implode("|", $b);
                    }
                }

                $batch_params = ['batch' => $batchno, 'bt' => $request->bt, 'bdate'=>$bdate, 'command' => 'BatchOrders'];
                BatchOrders::dispatch($batch_params);

                $batch = new \App\Models\QueueBatch();
                $batch->fill([
                    'BatchNo' => $batchno,
                ]);
                $batch->save();

                foreach ($this->batch_jobs as $k => $job) {
                    $process_queue = new \App\Models\ProcessQueueResults();
                    $data['JobName'] = $k;
                    $data['Request'] = json_encode($batch_params);
                    $data['batch_type'] = $request->bt;
                    $data['user_id'] = $request->user()->id;
                    $data['queue_batch_BatchNo'] = $batchno;
                    $process_queue->fill($data);
                    $process_queue->save();
                }

                $res['status'] = 200;

            } else {
                $res['msg'] = "Batch type missing";
                $res['status'] = 500;
            }

        }catch (\Exception $e){
            $res['msg'] = $e->getMessage();
            $res['status'] = 500;
        }


        return response()->json([
            'batchno'=>$batchno,
            "msg" => $res['msg']
        ], $res['status']);
    }

    /**
     * Get queue jobs
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQueuejobs(Request $request) {
        $res = [
            'cntr' => 0,
            'jobList' => [],
            'error' => '',
            'hasDone' => 0,
            'startTime' => ''
        ];
        $queueDate = \App\Models\ProcessQueueResults::max('created_at');
        $res['startTime'] = \Carbon\Carbon::parse($queueDate)->format('d F Y \@ H:i:s');
        $batch = \App\Models\QueueBatch::with(['Jobs'])->orderBy('created_at', 'desc')->first();

        if(!empty($batch)) {
            try {
                if (!empty($batch->jobs)) {
                    $res['cntr'] = count($batch['jobs']);
                    $scntr = 0;
                    foreach ($batch->jobs as $job) {

                        $percentage = 0;
                        $bgcolor = '';
                        if($job->Status == 200) {
                            $percentage = 100;
                            $bgcolor = 'success';
                            $scntr++;

                        } else if($job->Status == 102) {
                            $percentage = $job->Result;
                            $bgcolor = 'warning';

                        } else if($job->Status == 500) {
                            $percentage = 100;
                            $bgcolor = 'danger';
                        }

                        if(!empty($job->Result)) {
                            $r = json_decode($job->Result, 1);

                            if(!empty($r) && is_array($r)) {
                                unset($r['pushed']);
                                unset($r['total']);
                                $result = [];
                                foreach ($r as $k=>$d) {
                                    $result[] = $k.':'.(is_array($d) ? implode(",", $d) : $d) ;
                                }
                                $job->Result = implode(", ", $result);
                            }
                        }

                        $res['jobList'][] = [
                            'name' => $this->batch_jobs[$job->JobName],
                            'percentage' => $percentage,
                            'request' => $job->Request,
                            'result' => $job->Result,
                            'bgcolor' => $bgcolor,
                            'batchid' => $batch->BatchNo,
                            'command' => $job->JobName
                        ];
                    }

                    // has all jobs done //
                    if($res['cntr'] == $scntr) {
                        $res['hasDone'] = 1;
                    }
                }
            } catch (\Exception $e) {
                $res['error'] = $e->getMessage();
            }
        } else {
            $res['hasDone'] = 1;
            $res['error'] = 'BatchNo not found';
        }

        return response()->json($res);

    }

    /**
     * Requeue jobs
     * @param Request $request
     */
    public function requeue(Request $request) {
        $res = [
            'status' => 422,
            'msg' => ''
        ];

        try {
            if(!empty($request->batch) && !empty($request->command)) {
                $class = '\App\Jobs\\'.$request->command;

                $class::dispatch($request->all());
                \App\Models\ProcessQueueResults::updateJobStatus([
                        'batch' => $request->batch,
                        'command' => $request->command,
                        'status' => 0,
                        'data' => ''
                    ]
                );
            } else {
                $res['msg'] = "Parameters missing";
                $res['status'] = 422;
            }

        } catch (\Exception $e) {
            $res['msg'] = $e->getMessage();
            $res['status'] = 500;
        }
    }

    /**
     *Getting ageing batch data
     * @return \Illuminate\Http\JsonResponse
     */
    function getAgeingBatchOrder(){
        $status=100;
        $results=[];

        try {

            $results = Order::getAgeingBatchOrders();

            $status = 200;

        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return response()->json($results, $status);
    }

    /**
     * Get Today/Created Shipped
     * @return \Illuminate\Http\JsonResponse
     */
    public function dashboard(){
        $status=100;
        $results='No data found';

        try {
            $cache_time = config("local.cache_time");
            $results = Delivery::getDashboardData($cache_time);
            $status = 200;

        } catch (\Exception $e) {
            echo $e->getMessage();
            $results='Unexpected error happens, please try after sometimes';
        }

        return response()->json($results, $status);
    }

    /**
     * Get dashboard graph data
     * @return \Illuminate\Http\JsonResponse
     */
    public function dashboardGraph(Request $request){
        $status=100;
        $results='No data found';

        try {
            $cache_time = config("local.cache_time");
            if(strtolower($request->type)=='csdata'){
                $results = Delivery::getCreatedShippedData($cache_time);
            }else if(strtolower($request->type)=='oscdata'){
                $results = Delivery::getOpenShippedCancelledData($cache_time);
            }

            $status = 200;

        } catch (\Exception $e) {
            echo $e->getMessage();
            $results='Unexpected error happens, please try after sometimes';
        }

        return response()->json($results, $status);
    }

    /**
     * Get Dashboard details
     * @return \Illuminate\Http\JsonResponse
     */
    public function dashboardDetails(Request $request){
        $status=100;
        $results='No data found';

        try {
            $cache_time = config("local.cache_time");
            if(!empty($request->typ)){
                $results= Delivery::dashboardDetails($request->typ, $cache_time, $request);
                $status = 200;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            $results='Unexpected error happens, please try after sometimes';
        }

        return response()->json($results, $status);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotifications(Request $request){
        $status=100;
        $results='No data found';

        try {
            $results = ActionLog::getNotifications($request)->toArray();
            $results['count'] = $results['total'];
            $status = 200;

        } catch (\Exception $e) {
            echo $e->getMessage();
            $results='Unexpected error happens, please try after sometimes';
        }

        return response()->json($results, $status);
    }
}
