<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Traits\Order as OrderTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImportOrdersController extends Controller
{
    protected $status = 500;
    protected $msg = null;

    use OrderTrait;

    /**
     * upload csv and create excel file
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){

        $errors = [];
        $this->status = 422;
        try{
            $ftp_in_folder = config('ftp.ftp_in_folder');
            $path = storage_path('app').DIRECTORY_SEPARATOR.$ftp_in_folder.DIRECTORY_SEPARATOR.'Orders'.DIRECTORY_SEPARATOR;

            $files = $request->file('files');
            $rules = [
                'OrderNo' => 'required|unique:orders,ExternalId',
                'SKU' => 'required',
                'Qty' => 'required',
                'DeliveryForeNames' => 'required',
                'DeliverySurname' => 'required',
                'DelAdd1' => 'required',
                'DelAddTown' => 'required',
                'DelPostcode' => 'required',
                'DelCountryCode' => 'required',
            ];

            foreach($files as $file) {
                $name = $file->getClientOriginalName();
                $tmp_path = $file->getRealPath();
                $csv_data = $this->csvToArray($tmp_path);

                foreach ($csv_data as $i=>$row) {
                    $validator = Validator::make($row, $rules);
                    $rerrors = $validator->errors()->toArray();
                    if(!empty($rerrors)) {
                        $errors[$name][$i] = $rerrors;
                    }
                }

                // errors empty for file then process //
                if(empty($errors[$name])) {

                    // move file //
                    $file->move($path, $name);

                    // process file to 3pl //
                    $this->processFile($path.$name);
                }
            }

            if(!empty($errors)) {
                if(count($files) == count($errors)) {
                    $this->status = 422;

                } else {
                    $this->status = 200;
                    $this->msg  = "File(s) uploaded successfully";
                }

            } else {
                $this->status = 200;
                $this->msg  = "File(s) uploaded successfully";
            }

        } catch(\Exception $e){
            report($e);
            $this->msg = "Unexpected error happen, please try again";
        }

        return response()->json(['errors'=>$errors, 'message'=>$this->msg],  $this->status);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadSampleFile(Request $request)
    {
        $pathToFile='';
        try{
            $pathToFile=public_path().'/ImportOrder/sample.csv';

        }catch (\Exception $e) {
            report($e);
        }

        return response()->download($pathToFile);
    }

}
