<?php
namespace App\Http\Controllers\API\V1;


use App\ActionLog;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Traits\ThreePL;
use Illuminate\Support\Facades\Crypt;


class OrdersController extends Controller
{

    use ThreePL;
    public function __construct()
    {
        //
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){
        $orders = $temp_array = [];
        $status = 100;

        try {
            $results = $orders = Order::getOrderList($request);
            $orders = $orders->toArray();
           $counter = 0;
            foreach($results as $row){
                $temp_array[$counter]['SoldToName'] = $row->SoldToName;
                $temp_array[$counter]['SoldToPhoneNumber'] = $row->SoldToPhoneNumber;
                $temp_array[$counter]['SoldToAddress1'] = $row->SoldToAddress1;
                $temp_array[$counter]['ShipToName'] = $row->ShipToName;
                $temp_array[$counter]['OrderId'] = $row->OrderId;
                $temp_array[$counter]['ReferenceNum'] = $row->ReferenceNum;
                $temp_array[$counter]['created_at'] = $row->created_at;
                $temp_array[$counter]['Status'] = $row->Status;
                $temp_array[$counter]['PoNum'] = $row->PoNum;
                $temp_array[$counter]['TrackingNumber'] = $row->TrackingNumber;
                $counter++;
            }
            $orders['data'] = $temp_array;
            $orders['count'] = $orders['total'];
            $status = 200;

        } catch (\Exception $e) {
            $status = 500;
            echo $e->getMessage();
        }

        return response()->json($orders, $status);
    }

    /**
     * For order details
     * @param Request $request
     */
    function details(Request $request){
        $status=100;
        $results='No data found';
        try{

            if(!empty($request->id)){
                $order= Order::with('items')->where('OrderId',$request->id)->first();
                $results = $order->toArray();
                $results['SoldToName'] = $order->SoldToName;
                $results['ShipToName'] = $order->ShipToName;
                $results['SoldToAddress1'] = $order->SoldToAddress1;
                $results['ShipToAddress1'] = $order->ShipToAddress1;
                $results['SoldToPhoneNum'] = $order->SoldToPhoneNumber;
                $results['ShipToPhoneNum'] = $order->ShipToPhoneNumber;
                $results['SoldToEmail'] = $order->SoldToEmail;
                $results['ShipToEmail'] = $order->ShipToEmail;
                $results['SoldToPostalcode'] = $order->SoldToPostalcode;
                $results['ShipToPostalcode'] = $order->ShipToPostalcode;
                $results['TrackingNumber'] = $order->TrackingNumber;


                $status=200;
            }

        }catch (\Exception $e){
            //echo $e->getMessage();
            // echo $e->getFile().$e->getLine();
            $results='Unexpected error happens, please try after sometimes';
        }

        return response()->json($results, $status);
    }

    /**
     * get actions logs
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getActionLogs(Request $request) {
        $res = ['status' => 100, 'msg'=> '', 'data' => []];

        try {
            $action_logs = ActionLog::orderBy('created_at', 'desc')->take(10)->get();
            $res['new_cntr'] = ActionLog::where('status', '0')->count();
            if(!empty($action_logs)) {
                foreach ($action_logs as $row) {
                    if(!empty($row->label)) {
                        $l = [];
                        foreach (json_decode($row->label, 1) as $k=>$v) {
                            $l[] = $k." : ".$v;
                        }
                        $row->label = implode(", ", $l);
                    }
                    $res['data'][] = $row;
                }
            }
            $res['status'] = 200;

        } catch (\Exception $e) {
            report($e);
            $res['msg'] = $e->getMessage();
            $res['status'] = 500;
        }

        return response()->json($res, $res['status']);
    }

}
