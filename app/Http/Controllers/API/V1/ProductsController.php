<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Product;
use App\Traits\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    use Util;

    public function __construct()
    {
        // check and set config //
        $this->apiConfigValidation();
    }

    /**
     * For product listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){
        $orders=[];
        $status = 100;

        try {
            $products = Product::getProductList($request)->toArray();

            $products['count'] = $products['total'];
            $status = 200;

        } catch (\Exception $e) {
            $status = 500;
            echo $e->getMessage();
        }

        return response()->json($products, $status);
    }

    /**
     * For downloading the csv
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function download(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=product-".date("Y-m-d-H-s").".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $products = Product::getProductList($request,false)->toArray();
        $columns = [];
        if(!empty($products)) {
            $columns = array_keys((array)$products[0]);
        }

        $callback = function() use ($products, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            if(!empty($products)) {
                foreach ($products as $pr) {
                    fputcsv($file, array_values((array)$pr));
                }
            } else {
                fputcsv($file, ["No data found"]);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

}
