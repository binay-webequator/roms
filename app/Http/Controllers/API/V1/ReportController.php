<?php

namespace App\Http\Controllers\API\V1;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OTC\DeliveryItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\OTC\Delivery;
use Carbon\Carbon;

class ReportController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function batches(Request $request){
        $batches=[];
        $status = 100;

        try {
            $batches =  Order::getBatches($request->toArray());
            $status = 200;

        } catch (\Exception $e) {
            $status = 500;
            echo $e->getMessage();
        }

        return response()->json($batches, $status);
    }

    /**
     * For downloading the csv
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function download(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=batch-report-".date("Y-m-d-H-s").".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $batches = Order::getBatchesDownloadData($request->toArray());
        $columns = [];
        if(!empty($batches)) {
            $columns = array_keys((array)$batches[0]);
        }

        $callback = function() use ($batches, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            if(!empty($batches)) {
                foreach ($batches as $batch) {
                    fputcsv($file, array_values((array)$batch));
                }
            } else {
                fputcsv($file, ["No data found"]);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function orders(Request $request){
        $order_report=[];
        $status = 100;

        try {
            $order_report = OrderItem::getOrderItemList($request,true)->toArray();
            $status = 200;

        } catch (\Exception $e) {
            $status = 500;
            echo $e->getMessage();
        }

        return response()->json($order_report, $status);
    }

    /**
     * For downloading the csv
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function orderDownload(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=order-report-".date("Y-m-d-H-s").".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $reports = OrderItem::getOrderItemList($request->toArray() ,false);
        $columns = ['OrderId', 'Order Number', 'Created Date', 'Order Date', 'SKU', 'Line Number'];
        $callback = function() use ($reports, $columns)
        {
            if(!empty($reports)) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                foreach($reports as $row){
                    $deliveryData = [];

                    $deliveryitemData = [$row['Sku'], $row['OrderItemId']];
                    $d = $row['order'];
                    if(!empty($d)){
                        $c_date = \Carbon\Carbon::parse($d['created_at'])->format('d-m-Y H:i:s');
                        $o_date = Carbon::parse($d['OrderDate'])->format('d-m-Y H:i:s');
                        $deliveryData = [$d['OrderId'],$d['OrderNo'], $c_date, $o_date];
                    }

                    fputcsv($file, array_merge($deliveryData,$deliveryitemData));

                }
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
