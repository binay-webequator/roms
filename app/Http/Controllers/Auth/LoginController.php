<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $remember_me = false;
        if(!empty($request->remember)) {
            $remember_me = true;
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember_me)) {
            $user = $request->user();
            $password_changed_at = new Carbon(($user->password_changed_at) ? $user->password_changed_at : $user->created_at);

            if (Carbon::now()->diffInDays($password_changed_at) >= config('auth.password_expires_days')) {

                // Log the user out.
                $this->logout($request);

                session()->put('pwd_uid', $user->id);

                return redirect()->route('password.expired');
            }

            return  redirect()->to('/');
        }

        return $this->sendFailedLoginResponse($request);
    }
}
