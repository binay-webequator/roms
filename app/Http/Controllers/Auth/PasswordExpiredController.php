<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordExpiredRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\User;

class PasswordExpiredController extends Controller
{

    /**
     * change password form
     */
    public function index() {

        // get session user //
        $user_id = session()->get('pwd_uid') ?? auth()->user()->id;
        if(empty($user_id)) {
            return redirect()->route('login');
        }

        return view('auth.passwords.expired');
    }

    /**
     * change password
     * @param PasswordExpiredRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postExpired(PasswordExpiredRequest $request)
    {

        // validate current password must not be new password //
        if($request->current_password == $request->password) {
            return redirect()->back()->withErrors(['password' => 'Current and New password must not be same']);
        }

        // get session user //
        $user_id = session()->get('pwd_uid') ?? auth()->user()->id;
        if(empty($user_id)) {
            return redirect()->back()->withErrors(['404' => 'User not found']);
        }

        $user = User::find($user_id);

        // Checking current password //
        if (!Hash::check($request->current_password, $user->password)) {
            return redirect()->back()->withErrors(['current_password' => 'Current password is not correct']);
        }

        $user->update([
            'password' => bcrypt($request->password),
            'password_changed_at' => Carbon::now()->toDateTimeString()
        ]);

        // remove user session //
        session()->forget('pwd_uid');

        $route='login';
        if(auth()->user()) {
            $route='changepasswordform';
        }

        return redirect()->route($route)->with(['msg' => 'Password changed successfully', 'class'=>'success']);
    }
}
