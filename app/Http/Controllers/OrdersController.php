<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Response;
use App\Models\OTC\Delivery;

class OrdersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->date_formate = config('applocal.date_formate');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('orders.index');
    }

    /**
     * For order details
     * @param Request $request
     */
    function details(Request $request){
        $status=100;
        $results='No data found';
        try{

            if(!empty($request->DeliveryNumber)){
                $results= Delivery::where('DeliveryNumber',$request->DeliveryNumber)->first()->toArray();
                $status=200;
            }

        }catch (\Exception $e){
            $results='Unexpected error happens, please try after sometimes';
        }

        //return ['results'=>$results, 'status'=>$status];
        return view('orders.details');
    }
}
