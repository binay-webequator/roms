<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sku;
use App\Models\SkuPrice;
use Illuminate\Support\Facades\Redis;

class SkuController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        try{
            $sku_data=Sku::getSkus();
            Redis::setex('sku',60 * 60 * 24, json_encode($sku_data));
            $skus= Redis::get('sku');

dd(json_decode($skus));
        }catch(\Exception $e){
            echo $e->getMessage();
        }

        return view('sku.index')->with([
            'skus'=> ''
        ]);
    }
}
