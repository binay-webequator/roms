<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Response;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\ValidateUser;
use File;



class UserController extends Controller
{
    private $filepath = '';
    private $date_formate = '';

    public function __construct()
    {
        $this->middleware('auth');
        $this->filepath = storage_path('app').DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'profile'.DIRECTORY_SEPARATOR;
        $this->date_formate = config('applocal.date_formate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('show', User::class);
        return view('users.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $user = new User();
        $this->authorize('create', User::class);
        $roles = \App\Models\Role::all();
        return view("users.ae")->with(['roles'=>$roles, 'user'=>$user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateUser $request)
    {dd($request->all());
        $this->authorize('store', User::class);
        $filename=''; $status = 100;
        $msg = $p_pic = '';
       /* $request->validate([
            'password' => 'sometimes|required|same:confirm_password',
            'confirm_password' => 'sometimes|required_with:password|same:password'
        ]);*/
        if($request->hasFile('pic')) {

            try{
                $image       = $request->file('pic');
                $filename    = time().$image->getClientOriginalName();

                if (!is_dir($this->filepath)) {
                    mkdir($this->filepath, 0777,true);
                }

                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(80, 80);
                $image_resize->save($this->filepath .$filename);

            }catch(\Exception $e){
                return response()->json(['errors'=>$e->getMessage(), 'status'=>100]);
            }
        }

        $data = request()->except(['_token','_method', 'password_confirmation']);


        try{
            $uObj= User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => $data['password'],
                'role_id' =>$data['role_id'],
                'pic'=>$filename
            ]);

            $uObj->save();
            $msg = 'User added successfully';
            $status = 200;
        }catch(\Exception $e){
            return response()->json(['errors'=>$e->getMessage(), 'status'=>100]);
        }


        return response()->json(['msg'=>$msg, 'p_pic'=>$p_pic, 'status'=>$status]);
        //return redirect()->route('users.index')->with($this->msgDefaultClass, $msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update', $user);
        $roles = \App\Models\Role::all();
        return view("users.ae")->with(['user'=>$user, 'roles'=>$roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateUser $request, $id)
    {

        $filename=''; $status = 100;
        $msg = $p_pic =''; $pflag = 0;

        $uObj = User::findOrFail($id);
        $this->authorize('update', $uObj);
        if($request->hasFile('pic_hidden')) {

            try{
                $image       = $request->file('pic_hidden');
                $filename    = time().$image->getClientOriginalName();

                if (!is_dir($this->filepath)) {
                    mkdir($this->filepath, 0777,true);
                }

                $image_resize = Image::make($image->getRealPath());
                $image_resize->resize(80, 80);
                $image_resize->save($this->filepath .$filename);

                //Delete pld pic
                File::delete($this->filepath.$uObj->pic);
                $pflag = 1;
            }catch(\Exception $e){
                return response()->json(['errors'=>$e->getMessage(), 'status'=>100]);
            }

        }else{
            if(empty($request->pic)){
                if(!empty($uObj->pic)){
                    File::delete($this->filepath.$uObj->pic);
                    //unlink($this->filepath.$uObj->pic);
                }
            }

            $filename=$request->pic;
        }

        $data = request()->except(['_token','_method', 'password_confirmation']);

        if(!empty($data['pic_hidden'])){
            unset($data['pic_hidden']);
        }

        try{

            if(!empty($data['password'])){
                $user_data['password'] = $data['password'];
            }

            $user_data=[
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'role_id' =>$data['role_id'],
                'pic'=>$filename
            ];

            $uObj->update($user_data);
            $msg = 'User has been updated successfully';
            $status = 200;


            if(Auth::user()->id == $id){
                $p_pic = $filename;
            }



        }catch(\Exception $e){
            return response()->json(['errors'=>$e->getMessage(), 'status'=>100]);
        }

        return response()->json(['msg'=>$msg, 'p_pic'=>$p_pic, 'status'=>$status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('view', $user);
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->authorize('delete', User::class);
        $status = 200;
        try{
            User::find($id)->delete($id);
            $msg = 'User deleted successfully';

        }catch(\Exception $e){
            $msg = $e->getMessage();
            $status=100;
        }

        return response()->json(['msg'=>$msg, 'status'=>$status]);

    }


}
