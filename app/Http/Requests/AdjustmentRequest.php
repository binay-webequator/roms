<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdjustmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adjustment_type'=> 'required',
            'inventory_type'=> 'required',
            'reference_no'=> 'required',
            'transaction_id'=> ['nullable', 'required_if:adjustment_type,1'],
            'items.*.sku' => ['required', 'exists:products,sku'],
            'items.*.qty' => ['required', 'integer'],
            'items.*.reasoncode' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'transaction_id.required_if' => 'Transaction id field required ',
        ];
    }
}
