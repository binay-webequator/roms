<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordExpiredRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'current_password' => 'required',
            'password' => 'required|min:8|regex:/^(?=\S*[a-z])(?=\S*[%&*@])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
            'password_confirmation' =>'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'The password must be a combination of uppercase letters, numbers and special characters (allowed : @%&*)',
        ];
    }
}