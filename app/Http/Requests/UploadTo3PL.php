<?php

namespace App\Http\Requests;

use App\Rules\ValidateFileExtension;
use Illuminate\Foundation\Http\FormRequest;

class UploadTo3PL extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'=>['required', new ValidateFileExtension(['xls', 'xlsx'])],
            'customer_id' => ['required', 'integer']
        ];
    }
}
