<?php
namespace App\Imports;

use App\ActionLog;
use App\Jobs\CreateOrder;
use App\Models\Country;
use App\Models\Customer;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class OrderImport implements ToCollection
{
    protected $_customer_details = null;
    protected $_file_name = null;
    public $total_orders = [
        'orders' => 0,
        'lines' => 0
    ];

    /**
     * OrderImport constructor.
     * @param $customer
     * @param $file
     */
    public function __construct($customer,$file)
    {
        $this->_customer_details = $customer;
        $this->_file_name = $file;
    }

    /**
     * @param Collection $rows
     * @return array
     * @throws \Exception
     */
    public function collection(Collection $rows)
    {
        $excel_data = $headers = $orders = [];

        foreach ($rows->toArray() as $i=>$row) {
            if($i > 0){
                $row = array_combine($headers, $row);
                $excel_data[$row['ReferenceNumber']][] = $row;
                $this->total_orders['lines']+=1;
            }else{
                $headers = $row;
            }
        }

        $this->total_orders['orders'] = count($excel_data);

        //3PL data
        //Get carrier and mode
        $carrier_mode = Country::getCarrierAndMode();
        $ship_zip_point = config('threepl.api_credentials')['shipzippoint'];
        foreach($excel_data as $row) {
            $order=$row[0];
            $shiptocountry = $order['ShipToCountry'];

            // change ship to country code to GB if NB or NI shiptocountry
            if(in_array(strtolower($shiptocountry), ['nb', 'ni', 'uk', 'united kingdom'])) {
                $shiptocountry = "GB";
            }

            $carrier = 'UPS'; $mode = 'UPS_STANDARD';
            $strtoupper = strtoupper($shiptocountry);
            if(!empty($carrier_mode[$strtoupper])) {
                $carrier = $carrier_mode[$strtoupper]['Carrier'];
                $mode = $carrier_mode[$strtoupper]['Mode'];
            }

            $threepl_order = [
                "customerIdentifier" => [
                    "id" =>$this->_customer_details->customer_id
                ],
                "facilityIdentifier" => [
                    "id" =>$this->_customer_details->facility_id
                ],
                "referenceNum" => $order['ReferenceNumber'],
                "externalId" => $order['ReferenceNumber'],
                "poNum" => '',
                "routingInfo" => [
                    "carrier"=> $carrier,
                    "mode"=> $mode,
                    "shipPointZip"=> $ship_zip_point
                ],
                "shipTo" => [
                    "companyName" => $order['ShipToCompany'] ?? "",
                    "name" => $order['ShipToName '] ?? "",
                    "address1" => $order['ShipToAddress1'] ?? "",
                    "address2" => $order['ShipToAddress2'] ?? "",
                    "city" => $order['ShipToCity'] ?? "",
                    "state" => "",
                    "zip" => $order['ShipToZip'] ?? "",
                    "country" => $shiptocountry,
                    'phonenumber' => $order['ShipToPhone'] ?? "",
                    'emailaddress' =>""

                ]

            ];

            // order items //
            foreach ($row as $item) {
                $threepl_order['OrderItems'][] = [
                    "itemIdentifier" => [
                        "sku" => $item['SKU']
                        //"sku" => 'SS19BB13-PRL-L'
                    ],
                    "qty" => (int)$item['Quantity'],
                ];
            }

            $orders[] = $threepl_order;

            $or_data=['waybill_no'=>$order['ReferenceNumber'],'order_lines'=>count($threepl_order['OrderItems'])];
            //Save in action log
            $action_log= new ActionLog();
            $action_log->rid=time();
            $action_log->module = "Waybills";
            $action_log->record_id=$order['ReferenceNumber'];
            $action_log->user_id=auth()->user()->id;
            $action_log->label = json_encode(['Waybill'=>$order['ReferenceNumber']]);
            $action_log->filename=$this->_file_name;
            $action_log->order_data=json_encode($or_data);
            $action_log->save();
            $aid=$action_log->id;

            // queue order to post //
           CreateOrder::dispatch([
                'threepl_data' => $threepl_order,
                'action_log_id'=>$aid
            ]);
        }

        return $orders;
    }
}
