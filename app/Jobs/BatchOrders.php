<?php

namespace App\Jobs;

use App\Models\Order;
use App\Traits\ThreePL;
use App\Traits\Util;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use App\Models\ProcessQueueResults;

class BatchOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Util, ThreePL;

    public $batch;
    public $tries = 3;
    public $retryAfter = 3;
    protected $payload_data;
    protected $sp = "";
    protected $sp_params = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param=[])
    {
        $this->batch=$param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //get payload data
        $this->payload_data =  unserialize($this->job->payload()['data']['command']);

        echo "Data updating in table start....\n";
        try {

            $queue_batch_id = $this->payload_data->batch['batch'];
            $this->sp = "V2_Trigger()";
            if(!empty($this->payload_data->batch['bt'])) {
                if($this->payload_data->batch['bt'] == 2) {
                    $this->sp = "V2_Trigger_Priority()";

                } else if($this->payload_data->batch['bt'] == 3 && !empty($this->payload_data->batch['bdate'])) {
                    $this->sp = "V2_Trigger_Ageing(?)";
                    $this->sp_params = [
                        $this->payload_data->batch['bdate']
                    ];
                }
            }

            // orders batching  //
            echo "Batching orders -  start ....\n";
            //DB::select(DB::raw("call {$this->sp}"), $this->sp_params)[0]->{'cntr'};
            DB::select(DB::raw("call {$this->sp}"), $this->sp_params);
            echo "Batching orders - end ....\n";

            // update queue batch id to deliveries //
            Order::where('LocalStatus', 9)->whereNotNull('BatchID')->update(['QueueBatchID'=>$queue_batch_id]);

            // call call again //
            $this->callSP($queue_batch_id);

            // get count of import //
            $result = Order::getRecordCount($this->payload_data->batch['batch'], 'BatchOrders');

            $params = $this->payload_data->batch;
            $params['status'] = 200;
            $params['data'] = ['Batched Orders'=>number_format($result[0]->cntr)];
            ProcessQueueResults::updateJobStatus($params);

        } catch (\Exception $e) {
            //echo $e->getMessage().$e->getLine();die;
            throw new Exception($e->getMessage());
        }
        echo "Data updating in table end ....";

        //Call next job in queue
        PushBatchesToThreePL::dispatch(['batch'=>$this->payload_data->batch['batch'], 'command'=>'PushBatchesToThreePL'])->delay(now()->addMinutes(0.5));
    }

    /**
     *  Call SP for repeat batching
     * @param $batchid
     * @throws Exception
     */
    public function callSP($batchid) {

        try {
            // check for file name if any null shipment left //
            $cntr = DB::select(DB::raw("SELECT count(*) as cntr FROM orders Where uniqueidentifier is null and QueueBatchID=?"), [$batchid])[0]->{'cntr'};

            if (!empty($cntr)) {

                // sleep for 1 min //
                sleep(60);

                echo "Batching orders  -  start ....\n";
                DB::select(DB::raw("call {$this->sp}"), $this->sp_params)[0]->{'cntr'};
                echo "Batching orders - end ....\n";

                // update queue batch id to orders //
                Order::where('LocalStatus', 9)->update(['QueueBatchID'=>$batchid]);

                // call again //
                $this->callSP($batchid);
            }
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

}
