<?php

namespace App\Jobs;

use App\Models\OTC\Delivery;
use App\Traits\ThreePL;
use App\Traits\Util;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\ProcessQueueResults;
use App\ActionLog;

class CancelOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Util, ThreePL;

    public $params;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param=[])
    {
        $this->params=$param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // check 3pl settings //
        $this->threePLCongigValidation();

        // send cancel order to 3pl
        $this->sendCancelOrder3PL($this->params);
    }

    /**
     * Cacel order from 3PL
     * @param $order
     * @return bool
     * @throws \Exception
     */
    function sendCancelOrder3PL($order) {

        try {
            //Get access token //
            $threepl_id = isset($order['threeplid']) ? $order['threeplid'] : 0;
            if(!$threepl_id){
                $delivery = Delivery::find($order['delno']);
                $threepl_id = $delivery->ThreePLID;
            }
            $this->getAccessToken($this->threepl_credentials, $this->api_emails);

            $post_data = [
                'reason' => $order['reason']
            ];

            $threepl_params = [
                'url' => $this->threepl_credentials['url'] . "/orders/{$threepl_id}/canceler",
                'method' => 'POST',
                'content' => $post_data,
                'content-type' => 'application/json',
                'header-params' => [
                    'Authorization' => 'Bearer ' . $this->access_token,
                ]
            ];
            // get ETag from 3pl api //
            $return = $this->getThreePLData([
                'url'=> $this->threepl_credentials['url'] . "/orders/{$threepl_id}?detail=All",
                'token'=>$this->access_token,
                'email'=>$this->api_emails
            ]);

            if(!empty($return)) {
                $threepl_params['header-params']['If-Match'] = $return['etag'];

                // post to three pl //
                echo "Posting to 3PL - start\n";
                $response = $this->postTo3PL($threepl_params, '');
                echo "Posting to 3PL - end \n";

                // update order status //
                Delivery::updateDeliveryLocalStatus($this->params['delno'], 0);

                //update status in delivery table
                Delivery::updateDeliveryLocalStatus($this->params['delno'], 2, true);

                // update process queue result //
                $this->params['status'] = 200;
                $this->params['data'] = ["Order cancelled successfully"];

                ActionLog::where('rid', $this->params['batch'])->update(['status'=>200]);

            } else { // tag not found
                $response = $msg = "Cancel Order - No ETag found for Order ID - {$order['delno']} -".$threepl_id;
                echo $msg."\n";
                $this->sendSystemErrorEmail(
                    [
                        'to'=>$this->api_emails['exception'],
                        'message'=>$msg
                    ]
                );

                throw new \Exception($response);
            }

        } catch(\Exception $e) {
            echo $e->getMessage()."\n";
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'message'=>$e->getMessage()
                ]
            );

            // update order and queue log status //
            try {
                Delivery::updateDeliveryLocalStatus($this->params['delno'], 0);
                ActionLog::where('rid', $this->params['batch'])->update(['status'=>500, 'result'=>$e->getMessage()]);
            } catch(\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            throw new \Exception($e->getMessage());
        }
    }

}
