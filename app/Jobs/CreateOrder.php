<?php

namespace App\Jobs;

use App\Models\Order;
use App\WMS\Facades\WMS;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ActionLog;

class CreateOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payload = [];
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params=[])
    {
        $this->payload = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->payload =  unserialize($this->job->payload()['data']['command'])->payload ?? [];
        if(empty($this->payload['threepl_data'])) {
            throw new \Exception("Order data missing");
        }

        // post to 3pl //
        $log_action_id = $this->payload['action_log_id'];
        $response = WMS::Orders()->pushOrder($this->payload['threepl_data']);
        $data = $response->json();
        if($response->successful()) {

            // save to order and its item //
            Order::saveOrders([$data]);
            $status = 200;

        } else {
            $status = 500;
        }

        $response_data=[
            'post_data'=>$this->payload['threepl_data'],
            'return_data'=>$data,
            'status'=>$response->status()
        ];

        // update action log //
        $action_log = ActionLog::find($log_action_id);
        $action_log->status = $status;
        $action_log->result = json_encode($data);
        $action_log->save();

        if($status == 500) {
            throw new \Exception(json_encode($response_data));
        }
    }
}
