<?php

namespace App\Jobs;

use App\Models\Order;
use App\Traits\Util;
use App\Traits\ThreePL;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;
use Exception;

class PushBatchesToThreePL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Util, ThreePL;

    public $batch;
    public $tries = 3;
    public $retry = 3;
    protected $payload_data;
    protected $total_batches = 0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($param=[])
    {
        $this->batch=$param;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //get payload data
        $this->payload_data = unserialize($this->job->payload()['data']['command']);

        // check 3pl settings //
        $this->threePLConfigValidation();

        // call batches api //
        $cntr = 0;
        $cntr += $this->createBatches();

        // get count of import //
        $bdata = Order::getRecordCount($this->payload_data->batch['batch'], 'PushBatchesToThreePL');

        echo 'Data updating in table start....';
        $params           = $this->payload_data->batch;
        $params['status'] = 200;
        $params['data']   = ['Total Batches'=>$bdata[0]->bcntr, 'Batch Orders Pushed' => $bdata[0]->cntr];
        \App\Models\ProcessQueueResults::updateJobStatus($params);
        echo 'Data updating in table end ....';

    }

    public function createBatches() {
        echo "\n Pushing batches - start";
        // get batch shipments //
        try {
            $cntr      = 0;
            $deliveries = DB::table('batches')
                           ->join('orders', 'batches.batchID', '=', 'orders.batchID')
                           ->select('orders.ExternalId', 'orders.OrderId', 'orders.batchID',
                               'orders.uniqueidentifier')
                           ->whereNull('orders.threeplbatchid')
                           ->where('orders.LocalStatus', 9)
                           ->where('orders.QueueBatchID', $this->payload_data->batch['batch'])
                           ->where('batches.batchcreated', '>=', date('Y-m-d'))
                            ->get();

            // batches orders
            if (!empty($deliveries)) {
                $batch_orders = [];
                foreach ($deliveries as $s) {
                    $batch_orders[$s->batchID][] = [
                        'id' => $s->OrderId
                    ];
                }

                // create batches to 3pl //
                if (!empty($batch_orders)) {

                    $tb = count(array_keys($batch_orders));
                    $this->total_batches = $this->total_batches + $tb;
                    echo "Pushing batch order Total: $tb \n";

                    // get access token //
                    $this->getAccessToken($this->threepl_credentials, $this->api_emails);

                    // create one by one batch //
                    foreach ($batch_orders as $batchno => $orders) {
                        $threepl_params = [
                            'url'           => $this->threepl_credentials['url'] . '/orders/batches',
                            'method'        => 'POST',
                            'content-type'  => 'application/json',
                            'content'       => [
                                'customerIdentifier' => [
                                    "id" => $this->threepl_credentials['customer_id']
                                ],
                                'description'        => $batchno,
                                'orderIdentifiers'   => $orders
                            ],
                            'header-params' => [
                                'Authorization' => 'Bearer ' . $this->access_token,
                            ]
                        ];
                        echo "Posting to 3PL - start \n";
                        $return = $this->postTo3PL($threepl_params, 'batch');
                        $response = json_decode($return['response'], 1) ?? [];
/*                        $response = [
                            'ReadOnly' => [
                                'BatchOrderId' => time()
                            ]
                        ];*/
                        if (!empty($response)) {
                            Order::updateDeliveryBatchNo($batchno, $response);
                        }
                        echo "Posting to 3PL - end \n";

                        $cntr = $cntr + count($orders);
                    }
                }

            } else {
                echo "No data found\n\n";
            }


        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $cntr;
    }

}
