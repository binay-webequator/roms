<?php
namespace App\Lib\Order;

use App\Models\Customer;
use App\Models\Order;
use App\Traits\ThreePL;
use App\Traits\Util;
use Carbon\Carbon;

class GetOrders
{
    use Util, ThreePL;
    protected $page_no = 1;
    protected $last_modified = null;
    protected $page_size = 1000;

    /**
     * Start processing
     */
    public function init()
    {
        try {
            // config validation
            echo "Checking and setting config validation - start \n";
            $this->threePLConfigValidation();
            echo "Checking and setting config validation - end \n\n";

            // get access token
            $this->getAccessToken($this->threepl_credentials, $this->api_emails);

            // get all customers //
            $customers = Customer::all();
            if(empty($customers)) {
                throw new \Exception('3PL customer not found');
            }

            // loop throw each customer for orders //
            foreach ($customers as $customer) {

                $customer_id = $customer->customer_id;
                $this->page_no = 1;

                // get last modified //
                $this->last_modified = Order::getLastUpdatedDate($customer_id);

                echo "Get orders : {$customer->name}, from 3pl and save - start \n";
                $this->getOrders($customer_id);
                echo "Get orders : {$customer->name}, from 3pl and save - end \n\n";
            }

        } catch(\Exception $e) {
            report($e);
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'Get Orders',
                    'message'=>$e
                ]
            );

            echo "Error happen, please check log\n";
        }

        return true;
    }

    /**
     * create api params
     * @param $customer_id
     * @return array
     */
    public function createApiParams($customer_id) {
        $date='';
        if(!empty($this->last_modified)){
            $date=";lastModifiedDate=ge={$this->last_modified}";
        }

        return [
            'url' => $this->threepl_credentials['url']."/orders?pgsiz={$this->page_size}&pgnum={$this->page_no}&rql=ReadOnly.CustomerIdentifier.Id=={$customer_id}{$date}&detail=All&sort=LastModifiedDate",
            'token' => $this->access_token
        ];
    }

    /**
     * get orders from 3pl and save them
     * @param $customer_id
     * @return boolean
     */
    public function getOrders($customer_id) {

        try {

            // create pl params //
            $params = $this->createApiParams($customer_id);

            // get data //
            echo "Getting page no : {$this->page_no} data - start\n";
            $orders = $this->getThreePLData($params);
            echo "Getting page no : {$this->page_no} data - end\n";

            if(!empty($orders['data']['ResourceList'])) {
                if ($this->saveOrders($orders['data']['ResourceList'])) {
                    $this->page_no += 1;
                    return $this->getOrders($customer_id);
                }

            } else {
                echo "No data found\n";
            }

        }  catch (\Exception $e) {
            report($e);

            return false;
        }

        return true;
    }

    /**
     * save orders to db
     * @param $orders
     * @return bool
     */
    public function saveOrders($orders) {

        try {

            if(empty($orders)) {
                return false;
            }

            // save to db //
            echo "Saving order to db for page no: {$this->page_no} - start\n";
            Order::saveOrders($orders);
            echo "Saving order to db for page no: {$this->page_no} - end\n\n";

        }  catch (\Exception $e) {
            report($e);

            return false;
        }

        return true;
    }


}
