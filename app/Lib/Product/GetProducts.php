<?php


namespace App\Lib\Product;

use App\Models\Customer;
use App\Models\Product;
use App\Traits\ThreePL;
use App\Traits\Util;
use Carbon\Carbon;

class GetProducts
{
    use Util,ThreePL;

    protected $page_no=1;
    protected $last_modified_date='';
    protected $page_size = 100;

    /**
     * Start processing
     */
    public function init()
    {
        try {
            // config validation
            echo "Checking and setting config validation - start \n";
            $this->threePLConfigValidation();
            echo "Checking and setting config validation - end \n\n";

            //Check for access token
            $this->getAccessToken($this->threepl_credentials, $this->api_emails);

            // get all customers //
            $customers = Customer::all();
            if(empty($customers)) {
                throw new \Exception('3PL customer not found');
            }

            // loop throw each customer for products //
            foreach ($customers as $customer) {

                $customer_id = $customer->customer_id;
                $this->page_no = 1;

                // get last modified //
                $this->last_modified_date = Product::getLastModifiedDate($customer_id);

                echo "Get and save product : {$customer->name}, from 3PL - start \n";
                $this->get3PLProduct($customer_id);
                echo "Get and save product: {$customer->name}, from 3PL - end \n\n";
            }

        } catch(\Exception $e) {
            report($e);
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'3PL Product',
                    'message'=>$e
                ]
            );

            echo "Error happen, please check log\n";
        }

        return true;
    }

    /**
     * for setting 3pl api parameters
     * @param $cid
     * @return array
     */
    public function set3PLApiParam($cid){
        $date='';

        if(!empty($this->last_modified_date)){
            $date="&rql=LastModifiedDate=ge={$this->last_modified_date}";
        }

        return [
            'url' =>$this->threepl_credentials['url'] . "/customers/{$cid}/items?pgsiz={$this->page_size}&pgnum={$this->page_no}{$date}",
            'token' => $this->access_token
        ];
    }

    /**
     * Get products from 3pl and save data
     * @return bool
     * @param $customer_id
     * @throws \Exception
     */
    public function get3PLProduct($customer_id){
        try {

            //For set 3pl api param
            $params = $this->set3PLApiParam($customer_id);

            // get products //
            $products = $this->getThreePLData($params);
            if(!empty($products['data']['ResourceList'])) {
                if ($this->saveProduct($products['data']['ResourceList'])) {
                    $this->page_no += 1;
                    return $this->get3PLProduct($customer_id);
                }

            } else {
                echo "No data found\n";
            }

        }catch (\Exception $e){
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'3PL Product',
                    'message'=>$e
                ]
            );

            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * For saving data
     * @param $param
     * @return bool
     */
    public function saveProduct($param){

        try{
            if(empty($param)) {
                return false;
            }

            echo "Saving data for page no: {$this->page_no} - start\n";
            Product::saveData($param);
            echo "Saving data for page no: {$this->page_no} - end \n\n";

        }catch(\Exception $e){
            report($e);
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'3PL Product',
                    'message'=>$e
                ]
            );
        }

        return true;
    }
}
