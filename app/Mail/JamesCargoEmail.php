<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JamesCargoEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $emailData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailData)
    {
        $this->emailData = $emailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $mailable = $this->subject($this->emailData['subject'])
            ->view('mails.'.$this->emailData['template'])
            ->with([
                'time'     => $this->emailData['timeframe'] ?? '', //this works without queue
            ]);

        // attach files //
        if(!empty($this->emailData['attachments'])) {

            if(!is_array($this->emailData['attachments'])) {
                $this->emailData['attachments'] = [
                    $this->emailData['attachments']
                ];
            }

           foreach ($this->emailData['attachments'] as $file) {
               $mailable->attach($file);
           }
        }

        return $mailable;

    }
}
