<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ASN extends Model
{
    protected $table = 'asns';
    protected $primaryKey = "ReceiverId";
    public $timestamps=true;
    public $incrementing = false;
    protected $guarded = [];

    /**
     * with ASNItem model
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function asnItem(){
        return $this->hasMany('ASNItem','ReceiverId','ReceiverId');
    }

    /**
     * @param string $customer_id
     * @throws \Exception
     */
    public static function getLastModifiedDate($customer_id){
        $last_modified = '';
        try {
            $qb = self::whereRaw('1=1');
            $qb->where('CustomerId', $customer_id);
            $rdata = $qb->max('LastModifiedDate');
            $last_modified = str_replace(' ','T', $rdata);
        } catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return $last_modified;
    }

    /**
     * save asns
     * @param $asns
     * @return bool
     */
    public static function saveASNData($asns) {

        if(empty($asns)) {
            return false;
        }

        try {

            foreach ($asns as $row) {
                $asn_data = [
                    'ReceiverId' => $row['ReadOnly']['ReceiverId'],
                    'CustomerId' => $row['ReadOnly']['CustomerIdentifier']['Id'],
                    'ReferenceNum' => $row['ReferenceNum'],
                    'PoNum' => $row['PoNum'] ?? '',
                    'ExternalId' => $row['ExternalId'] ?? (explode('-',$row['ReferenceNum'])[0]),
                    'ReceiptAdviceNumber' => $row['ReceiptAdviceNumber'] ?? '',
                    'ArrivalDate' => (!empty($row['ArrivalDate']) ? Carbon::parse($row['ArrivalDate'])->format('Y-m-d H:i:s') : null),
                    'ExpectedDate' => (!empty($row['ExpectedDate']) ? Carbon::parse($row['ExpectedDate'])->format('Y-m-d H:i:s') : null),
                    'Notes' => $row['Notes'] ?? '',
                    'ScacCode' => $row['ScacCode'] ?? '',
                    'Carrier' => $row['Carrier'] ?? '',
                    'BillOfLading' => $row['BillOfLading'] ?? '',
                    'DoorNumber' => $row['DoorNumber'] ?? '',
                    'TrackingNumber' => $row['TrackingNumber'] ?? '',
                    'TrailerNumber' => $row['TrailerNumber'] ?? '',
                    'SealNumber' => $row['SealNumber'] ?? '',
                    'NumUnits1' =>$row['NumUnits1'] ?? '0',
                    'NumUnits2' =>$row['NumUnits2'] ?? '0',
                    'TotalWeight' => $row['TotalWeight'] ?? '0',
                    'TotalVolume' => $row['TotalVolume'] ?? '0',
                    'LastModifiedDate' => (!empty($row['ReadOnly']['LastModifiedDate']) ? Carbon::parse($row['ReadOnly']['LastModifiedDate'])->format('Y-m-d H:i:s') : null)
                ];

                $asn_items = [];
                if(!empty($row['ReceiveItems'])) {
                    foreach ($row['ReceiveItems'] as $ro) {
                        $on_hold = 0;
                        if( $ro['OnHold']=='true'){
                            $on_hold = 1;
                        }
                        $asn_items[] = [
                            'ReceiveItemId' => $ro['ReadOnly']['ReceiveItemId'],
                            'ReceiverId' => $row['ReadOnly']['ReceiverId'],
                            'FullyShippedDate' => (!empty($ro['ReadOnly']['FullyShippedDate']) ? Carbon::parse($ro['ReadOnly']['FullyShippedDate'])->format('Y-m-d H:i:s') : null),
                            'ExpectedQty' => $ro['ReadOnly']['ExpectedQty'] ?? '0',
                            'OnHoldDate' => (!empty($ro['OnHoldDate']) ? Carbon::parse($ro['OnHoldDate'])->format('Y-m-d H:i:s') : null),
                            'Qualifier' => $ro['Qualifier'] ?? '',
                            'ExternalId' => $ro['ExternalId'] ?? '',
                            'Qty' => $ro['Qty'],
                            'SecondaryQty' => $ro['SecondaryQty'] ?? '0',
                            'LotNumber' => $ro['LotNumber'] ?? '',
                            'SerialNumber' => $ro['SerialNumber'] ?? '',
                            'ExpirationDate' => (!empty($ro['ExpirationDate']) ? Carbon::parse($ro['ExpirationDate'])->format('Y-m-d H:i:s') : null),
                            'Cost' => $ro['Cost'] ?? '0',
                            'WeightImperial' => $ro['WeightImperial'] ?? '0',
                            'WeightMetric' => $ro['WeightMetric'] ?? '0',
                            'OnHold' => $on_hold,
                            'OnHoldReason' => $ro['OnHoldReason'] ?? '',
                            'created_at'=>Carbon::now(),
                            'updated_at'=>Carbon::now()
                        ];
                    }
                }

                // save to tables
                self::updateOrCreate(['ReceiverId'=>$row['ReadOnly']['ReceiverId']], $asn_data);
                if(!empty($asn_items)) {
                    ASNItem::where('ReceiverId', $row['ReadOnly']['ReceiverId'])->delete();
                    ASNItem::insert($asn_items);
                }

            }

        } catch (\Exception $e){
            report($e);
            return false;
        }

        return true;
    }
}
