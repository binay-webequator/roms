<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ASNItem extends Model
{
    protected $table = 'asn_items';
    protected $primaryKey = "ReceiveItemId";
    public $timestamps=true;
    public $incrementing = false;
    protected $guarded = [];
}
