<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Country extends Model
{
    protected $table = 'country';
    public $primaryKey  = 'countrycode';
    public $incrementing= false;
    public $timestamps=false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'countryiso',
        'countrycode',
        'countryname',
        'pickgroup',
        'upscode'
    ];
    private static $cgroup = 'country.'; // cache group

    /** get country
     * @param $ccode
     * @return mixed
     */
    public static function getCountry($ccode) {

        $ckey = Self::$cgroup.$ccode;
        $cache_data = [];
        try {
            if (Cache::has($ckey)) {
                $cache_data = Cache::get($ckey);
            } else {
                $cache_data = Self::where('countrycode', $ccode)->first();
                if (!empty($cache_data)) {
                    $cache_data = $cache_data->toArray();
                }
                Cache::forever($ckey, $cache_data);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $cache_data;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function getCountryCodes(){
        $countries = [];
        try {
            $cdata = Self::select('countrycode','countryname')->get();
            foreach($cdata as $row){
                $countries[strtolower($row->countryname)] = $row->countrycode;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $countries;
    }

    /**
     * For getting carrier mapping data
     * @param $param
     * @throws \Exception
     * service added by Abdul
     */
    public static function getCarrierAndMode(){
        $shipViaArray =[];
        try {
            $data = Self::all();
            foreach($data as $row){
                if(!empty($row->upscode)){
                    $explode = explode(" ",$row->upscode);
                    $shipViaArray[$row->countrycode] =['Carrier'=>$explode[0], 'Mode'=>$explode[1],'Service'=>trim(str_replace(' ', '_', $row->upscode))];
                }

            }

            return $shipViaArray;
        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }
    }
}
