<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourierMapping extends Model
{
    protected $table='courier_mapping';

    /**
     * Get listing of courier mapping
     * @return array
     * @throws \Exception
     */
    public static function getCourierMapping(){
        $couriers=[];
        try{
            $results=self::all();
            if($results->count() > 0){
                $results->toArray();
                foreach($results as $row){
                    $couriers[$row['3pl']]=$row['aftership'];
                }
            }
        }catch (\Exception $e){
            throw new \Exception('Unexpected error happens');
        }

        return $couriers;
    }
}
