<?php

namespace App\Models;

use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Order extends Model
{
    protected $primaryKey = "OrderId";
    public $timestamps=true;
    public $incrementing = false;
    protected $guarded = [];


    /**hasmany to Order items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items(){
        return $this->hasMany(OrderItem::class, 'OrderId', 'OrderId');
    }

    /**
     * get last modified
     * @param $customer_id
     * @return string
     * @throws \Exception
     */
    public static function getLastUpdatedDate($customer_id) {
        $last_modified = '';
        try {
            $qb = self::whereRaw('1=1');
            $qb->where('CustomerId', $customer_id);
            $rdata = $qb->max('LastModifiedDate');
            $last_modified = str_replace(' ','T', $rdata);
        } catch (\Exception $e){
            report($e);
        }

        return $last_modified;
    }

    /**
     * save orders
     * @param $orders
     * @return bool
     */
    public static function saveOrders($orders) {

        if(empty($orders)) {
            return false;
        }

        try {

           foreach ($orders as $order) {

               $tracking_no = $order['RoutingInfo']['TrackingNumber'] ?? '';
               if(!empty($tracking_no)){
                   $tracking_no = explode(',', $tracking_no)[0];
               }

                $odata = [
                    'OrderId' => $order['ReadOnly']['OrderId'],
                    'CustomerId' => $order['ReadOnly']['CustomerIdentifier']['Id'],
                    'ReferenceNum' => $order['ReferenceNum'],
                    'ExternalId' => $order['ExternalId'] ?? (explode('-',$order['ReferenceNum'])[0]),
                    'Description' => $order['Description'] ?? '',
                    'PoNum' => $order['PoNum'] ?? '',
                    'TrackingNumber' =>$tracking_no,
                    'Carrier'=>$order['RoutingInfo']['Carrier'] ?? '',
                    'Mode'=>$order['RoutingInfo']['Mode'] ?? '',
                    'Notes' => $order['Notes'] ?? '',
                    'EarliestShipDate' => (!empty($order['EarliestShipDate']) ? Carbon::parse($order['EarliestShipDate'])->format('Y-m-d H:i:s') : null),
                    'ShipCancelDate' => (!empty($order['ShipCancelDate']) ? Carbon::parse($order['ShipCancelDate'])->format('Y-m-d H:i:s') : null),
                    'NumUnits1' =>$order['NumUnits1'] ?? '0.00',
                    'NumUnits2' =>$order['NumUnits2'] ?? '0.00',
                    'TotalWeight' => $order['TotalWeight'] ?? '0.00',
                    'TotalVolume' => $order['TotalVolume'] ?? '0.00',
                    'BillingCode' => $order['BillingCode'] ?? '',
                    'AsnNumber' => $order['AsnNumber'] ?? '',
                    'UpsServiceOptionCharge' => $order['UpsServiceOptionCharge'] ?? '0.00',
                    'UpsTransportationCharge' => $order['UpsTransportationCharge'] ?? '0.00',
                    'AddFreightToCod' => $order['AddFreightToCod'] ?? false,
                    'UpsIsResidential' => $order['UpsIsResidential'] ?? false,
                    'ShippingNotes' => $order['ShippingNotes'] ?? '',
                    'MasterBillOfLadingId' => $order['MasterBillOfLadingId'] ?? '',
                    'InvoiceNumber' => $order['InvoiceNumber'] ?? '',
                    'ShipToCompanyName' => $order['ShipTo']['CompanyName'] ?? '',
                    'ShipToTitle' => $order['ShipTo']['Title'] ?? '',
                    'ShipToName' => $order['ShipTo']['Name'] ?? '',
                    'ShipToAddress1' => $order['ShipTo']['Address1'] ?? '',
                    'ShipToAddress2' => $order['ShipTo']['Address2'] ?? '',
                    'ShipToCity' => $order['ShipTo']['City'] ?? '',
                    'ShipToCountry' => $order['ShipTo']['Country'] ?? '',
                    'ShipToPhoneNumber' => $order['ShipTo']['PhoneNumber'] ?? '',
                    'ShipToEmailAddress' => $order['ShipTo']['EmailAddress'] ?? '',
                    'ShipToFax' => $order['ShipTo']['Fax'] ?? '',
                    'SoldToCompanyName' => $order['SoldTo']['CompanyName'] ?? '',
                    'SoldToTitle' => $order['SoldTo']['Title'] ?? '',
                    'SoldToName' => $order['SoldTo']['Name'] ?? '',
                    'SoldToAddress1' => $order['SoldTo']['Address1'] ?? '',
                    'SoldToAddress2' => $order['SoldTo']['Address2'] ?? '',
                    'SoldToCity' => $order['SoldTo']['City'] ?? '',
                    'SoldToCountry' => $order['SoldTo']['Country'] ?? '',
                    'SoldToPhoneNumber' => $order['SoldTo']['PhoneNumber'] ?? '',
                    'SoldToEmailAddress' => $order['SoldTo']['EmailAddress'] ?? '',
                    'SoldToFax' => $order['SoldTo']['Fax'] ?? '',
                    'PackageInfo' => (!empty($order['ReadOnly']['Packages']) ? json_encode($order['ReadOnly']['Packages']) : ''),
                    'Status' => $order['ReadOnly']['Status'] ?? 0,
                    'SavedElements' => json_encode(($order['SavedElements'] ?? [])),
                    'LastModifiedDate' => (!empty($order['ReadOnly']['LastModifiedDate']) ? Carbon::parse($order['ReadOnly']['LastModifiedDate'])->format('Y-m-d H:i:s') : null)
                ];

                // delivery type //
                $delivery_type=$order_no=$order_date = null;
                if(!empty($order['SavedElements'])) {
                    foreach ($order['SavedElements'] as $savedElement) {
                        if($savedElement['Name'] == "DeliveryTyp") {
                            $delivery_type = $savedElement['Value'];

                        }else if($savedElement['Name'] == "OrderNo") {
                            $order_no = $savedElement['Value'];

                        } else if($savedElement['Name'] == "OrderDate") {
                            $order_date = (!empty($savedElement['Value']) ? Carbon::parse($savedElement['Value'])->format("Y-m-d H:i:s") : null);
                        }
                    }
                }
               $odata['OrderNo'] = $order_no;
               $odata['OrderDate'] = $order_date;
               $odata['DeliveryTyp'] = $delivery_type;

                $order_items = [];
                if(!empty($order['OrderItems'])) {
                    foreach ($order['OrderItems'] as $oitem) {
                        $order_items[] = [
                           'OrderItemId' => $oitem['ReadOnly']['OrderItemId'],
                           'OrderId' => $order['ReadOnly']['OrderId'],
                           'Sku' => $oitem['ItemIdentifier']['Sku'],
                            'Qualifier' => $oitem['Qualifier'] ?? '',
                            'ExternalId' => $oitem['ExternalId'] ?? '',
                            'Qty' => $oitem['Qty'],
                            'SecondaryQty' => $oitem['SecondaryQty'] ?? '0',
                            'LotNumber' => $oitem['LotNumber'] ?? '',
                            'SerialNumber' => $oitem['SerialNumber'] ?? '',
                            'ExpirationDate' => (!empty($oitem['ExpirationDate']) ? Carbon::parse($oitem['ExpirationDate'])->format('Y-m-d H:i:s') : null),
                            'WeightImperial' => $oitem['WeightImperial'] ?? '0.0000',
                            'WeightMetric' => $oitem['WeightMetric'] ?? '0.0000',
                            'Notes' => $oitem['Notes'] ?? '',
                            'FulfillInvSalePrice'=> $oitem['FulfillInvSalePrice'] ?? '0.0000',
                            'FulfillInvDiscountPct' => $oitem['FulfillInvDiscountPct'] ?? '0.0000',
                            'FulfillInvDiscountAmt' => $oitem['FulfillInvDiscountAmt'] ?? '0.0000',
                            'FulfillInvNote' => $oitem['FulfillInvNote'] ?? '',
                            'SavedElements' => json_encode(($oitem['SavedElements'] ?? [])),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ];
                    }
                }

                // save to tables
                self::updateOrCreate(['OrderId'=>$order['ReadOnly']['OrderId']], $odata);
                if(!empty($order_items)) {
                    OrderItem::where('OrderId', $order['ReadOnly']['OrderId'])->delete();
                    OrderItem::insert($order_items);
                }

           }

        } catch (\Exception $e){
            report($e);
            return false;
        }

        return true;
    }

    /**
     * @param $param
     * @return mixed
     * @throws \Exception
     */
    public static function getOrderList($param){
        DB::enableQueryLog();
        try{
            $per_page = 50;
            $order_by = 'OrderId';
            $sort_by = 'desc';
            $orders =  self::whereRaw('1=1');
            $orders->select('*');

            // status filter
            if(isset($param['status']) && $param['status']!='') {
                switch ($param['status']) {
                    case 0:
                        $orders->where('Status', 0);
                        break;
                    case 1:
                        $orders->where('Status', 1);
                        break;
                    case 2:
                        $orders->where('Status', 2);
                        break;
                    default:
                        break;
                }
            }
            if(!empty($param['keyword'])) {
                $orders->where('OrderId', $param['keyword'])
                    ->orWhere('ReferenceNum', $param['keyword']);
            }

            if(!empty($param['transactionId'])) {
                $orders->where('OrderId', $param['transactionId']);
            }

            if(!empty($param['referenceNo'])) {
                $orders->where('ReferenceNum', $param['referenceNo']);
            }

            if(!empty($param['orderNo'])) {
                $orders->where('OrderNo', $param['orderNo']);
            }

            if(!empty($param['startDate']) && !empty($param['endDate'])) {
                $orders->whereBetween('created_at', [date('Y-m-d H:i', strtotime($param['startDate'])), date('Y-m-d H:i', strtotime($param['endDate']))]);
            }else{
                if(!empty($param['startDate'])){
                    $orders->where('created_at', '>=', date('Y-m-d H:i', strtotime($param['startDate'])));
                }
                else if(!empty($param['endDate'])){
                    $orders->where('created_at', '<=', date('Y-m-d H:i', strtotime($param['endDate'])));
                }
            }

            if(!empty($param['limit'])) {
                $per_page =  $param['limit'];
            }

            if(!empty($param['byColumn'])) {
                $sort_by = $param['byColumn'];
            }

            if(!empty($param['orderBy'])) {
                $order_by = $param['orderBy'];
            }

            $orders->orderBy($order_by, $sort_by);
            //$result=$orders->toSql();dd($result);
            //$query = DB::getQueryLog();dd($query);
            return $orders->paginate($per_page);
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * For getting batches
     * @return mixed
     * @throws \Exception
     */
    public static function getBatches($param){

        try{
            $per_page = 50;
            $query = DB::table('orders')
                ->join('country', 'orders.ShipToCountry', '=', 'country.countrycode')
                ->select(DB::raw("concat(orders.BatchID,'-',orders.GroupType) as PO"), 'orders.BatchID', 'orders.GroupType', DB::raw("count(distinct orders.OrderId) orders"), 'orders.ThreeplBatchID', 'country.pickgroup')
                ->whereNotNull('orders.BatchID');

            // where conditions //
            if(!empty($param['batchID'])) {
                $query->where('orders.BatchID', '>', $param['batchID']);

            }
            if(!empty($param['threepldid'])) {
                $query->where('orders.ThreeplBatchID', '=', $param['threepldid']);

            }
            if(!empty($param['dates'])) {
                $date = date('Y-m-d');
                if($param['dates'] == 2) {
                    $date = date("Y-m-d", strtotime("-1 day"));
                }
                $query->where(DB::raw("CAST(orders.ReadyForUpload AS DATE)"), "=", $date);
            }

            $sort_by = "asc";
            $order_by = "orders.BatchID";
            if(!empty($param['byColumn'])) {
                $sort_by = $param['byColumn'];
            }

            if(!empty($param['orderBy'])) {
                $order_by = $param['orderBy'];
            }

            if(!empty($param['limit'])) {
                $per_page =  $param['limit'];
            }

            $batches = $query->groupBy('BatchID', 'GroupType', 'ThreeplBatchID', 'country.pickgroup')
                ->orderBy($order_by, $sort_by)
                ->paginate($per_page);

            return $batches;

        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Report download data
     * @param $param
     * @return mixed
     * @throws \Exception
     */
    public static function getBatchesDownloadData($param){

        try{

            $query = DB::table('orders')
                ->join('country', 'orders.ShipToCountry', '=', 'country.countrycode')
                ->select(DB::raw("concat(orders.BatchID,'-',orders.GroupType) as PO"), 'orders.BatchID', 'orders.GroupType', DB::raw("count(distinct orders.OrderId) orders"), 'orders.ThreeplBatchID', 'country.pickgroup')
                ->whereNotNull('orders.BatchID');

            // where conditions //
            if(!empty($param['batchID'])) {
                $query->where('orders.BatchID', '>', $param['batchID']);

            }
            if(!empty($param['threepldid'])) {
                $query->where('orders.ThreeplBatchID', '=', $param['threepldid']);

            }
            if(!empty($param['dates'])) {
                $date = date('Y-m-d');
                if($param['dates'] == 2) {
                    $date = date("Y-m-d", strtotime("-1 day"));
                }
                $query->where(DB::raw("CAST(orders.ReadyForUpload AS DATE)"), "=", $date);
            }

            $sort_by = "asc";
            $order_by = "orders.BatchID";
            if(!empty($param['byColumn'])) {
                $sort_by = $param['byColumn'];
            }

            if(!empty($param['orderBy'])) {
                $order_by = $param['orderBy'];
            }

            $batches = $query->groupBy('BatchID', 'GroupType', 'ThreeplBatchID', 'country.pickgroup')
                ->orderBy($order_by, $sort_by)
                ->get()->toArray();

            return $batches;

        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * For getting ageing batch order
     * @return mixed
     * @throws \Exception
     */
    public static function getAgeingBatchOrders(){
        try{
            return DB::select('select count(*) as cntr, convert(OrderDate, date) as created_date FROM orders where BatchID IS NULL and GroupType IS NULL GROUP BY convert(OrderDate, date) order by created_date ASC', ['']);
        }catch(\Exception $e){
            echo $e->getMessage();
            throw new \Exception($e->getMessage());
        }

    }

    /**
     * get count for jobs
     * @param $queuebatchid
     * @param $job
     * @return array
     * @throws \Exception
     */
    public static function getRecordCount($queuebatchid, $job) {

        try {
            $sql = "";
            switch ($job) {

                case 'BatchOrders':
                    $sql = "Select count(*) as cntr from orders where LocalStatus=9 and QueueBatchID=$queuebatchid";
                    break;

                case 'PushBatchesToThreePL':
                    $sql = "SELECT count(distinct batchid) as bcntr, count(*) as cntr  FROM orders where QueueBatchID=$queuebatchid and threeplbatchid is not null;";
                    break;
                default:
                    break;
            }

            return DB::select($sql);

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Update delivery batch no from batch creation api
     * @param $params
     * @param $batchno
     * @return bool
     */
    public static function updateDeliveryBatchNo($batchno, $params) {

        try {
            if(!empty($params['ReadOnly']['BatchOrderId'])) {
                DB::update("UPDATE orders SET threeplbatchid=?, LocalStatus=0 Where batchID=? ", [$params['ReadOnly']['BatchOrderId'], $batchno]);
            } else {
                throw new \Exception('Batch Order ID not found');
            }

        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public static function getExportOrderData(){
        $results=[];
        try{
            $timeframe = config('local.order_download_timeframe');
            $date_time = date('Y-m-d H:i:s', strtotime(" - {$timeframe}hours"));

            $results=self::select('ReferenceNum', 'Carrier' , 'TrackingNumber')->whereRaw("TrackingNumber !=''")->where('LastModifiedDate', '>=', $date_time)->get();
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $results;
    }

    /**
     * get last modified orders in last 24 hours
     * @param int $customer_id
     * @return mixed
     * @throws \Exception
     */
    public static function getOrderReport($customer_id=0){
        $results=[];
        try{
            //$last_24_hours = date("Y-m-d H:i:s", strtotime('-24 hour'));
            $timeframe = config('local.order_download_timeframe');
            $last_24_hours = date('Y-m-d H:i:s', strtotime(" - {$timeframe}hours"));
            $results=self::select('ReferenceNum', 'PODDate',  'OrderCode', 'Recipient','Details','Discrepancy', 'OrderType')
                ->where('PODDate', '>=', $last_24_hours)
                ->where(function($query){
                    $query->where('OrderCode', 1)->orwhere('OrderType', 'like','%failed%');
                });

            if(!empty($customer_id)) {
                $results->where('CustomerId', $customer_id);
            }
            $results = $results->get();

        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $results;
    }

    public static function updateOrders($params)
    {

        if (empty($params)) {
            return false;
        }

        foreach ($params['trackingdetails']['trackingdetail'] as $orders) {
            if(!empty($orders['trackingnumber'])){
                $tracking_number=$orders['trackingnumber'];
                $events = isset($orders['trackingevents']['trackingevent']) ? $orders['trackingevents']['trackingevent'] : [];
                if($events){
                    $rdata=self::getUpdatedOrders($orders['trackingevents']['trackingevent']);
                }
                //Update orders
                if(!empty($rdata)){
                    $date=Carbon::parse($rdata['date'])->format('Y-m-d H:i:s');
                    self::where('TrackingNumber',$tracking_number)
                        ->update([
                            'PODDate'=>$date,
                            'OrderCode'=>$rdata['code'],
                            'OrderType'=>$rdata['type'],
                            'Recipient'=>$rdata['description'],
                            'Details'=>$rdata['description'],
                            'Discrepancy'=>$rdata['description'],
                            'TrackingEvents'=>json_encode($events)
                        ]);
                }

            }


        }

        return true;
    }

    /**
     * Get update orders data
     * @param $params
     * @return array|string[]
     * @throws \Exception
     */
    public static function getUpdatedOrders($params){
        $results=[];
        foreach($params as $row){
            if(isset($row['code']) && $row['code']==1 && strtoupper($row['type'])=='DELIVERED'){
                $results=$row;
                break;
            }
            if(!empty($row['type']) && strpos(strtolower($row['type']), "failed") !== false){
                $event = $results=$params[0];
                unset($row['date']);
                $results=array_merge($event, $row);
                break;
            }
        }

        if(empty($results) &&  isset($params[0])){
            $results=$params[0];
        }

        return $results;
    }

    /**
     * For getting all tracking numbers
     * @return array
     * @throws \Exception
     */
    public static function getTrackingNumbers(){
        $results=[];
        $orders=self::where(function ($qry){
            $qry->where('OrderCode', '!=', 1)->orWhereNull('OrderCode');
        })->where('TrackingNumber', '!=', '')->select('TrackingNumber')->orderby('OrderId', 'asc')->get();

        foreach($orders as $order){
            $results[]= $order->TrackingNumber;
        }

        return $results;
    }

}
