<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class OrderItem extends Model
{
    protected $primaryKey = "OrderItemId";
    public $timestamps=true;
    public $incrementing = false;
    protected $guarded = [];

    /**belong to Order
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order(){
        return $this->belongsTo(\App\Models\Order::class,'OrderId','OrderId');
    }

    /**
     * @param $param
     * @return mixed
     * @throws \Exception
     */
    public static function getOrderItemList($param, $f=false){
        DB::enableQueryLog();
        try{
            $per_page = 50;
            $order_by = 'OrderItemId';
            $sort_by = 'desc';
            $orders =  self::with('order')->whereRaw('1=1')->whereHas('order', function($q) use($param) {
                if(!empty($param['orderNo'])) {
                    $q->where('OrderNo', $param['orderNo']);
                }

                if(!empty($param['ostartDate']) && !empty($param['oendDate'])) {
                    $q->whereBetween('OrderDate', [date('Y-m-d H:i', strtotime($param['ostartDate'])), date('Y-m-d H:i', strtotime($param['oendDate']))]);
                }else{
                    if(!empty($param['ostartDate'])){
                        $q->where('OrderDate', '>=', date('Y-m-d H:i', strtotime($param['ostartDate'])));
                    }
                    else if(!empty($param['oendDate'])){
                        $q->where('OrderDate', '<=', date('Y-m-d H:i', strtotime($param['oendDate'])));
                    }
                }
            })->select('OrderId','Sku','OrderItemId');

            if(!empty($param['referenceNo'])) {
                $orders->where('OrderId', $param['referenceNo']);
            }

            if(!empty($param['sku'])) {
                $orders->where('Sku', $param['sku']);
            }

            if(!empty($param['limit'])) {
                $per_page =  $param['limit'];
            }

            if(!empty($param['byColumn'])) {
                $sort_by = $param['byColumn'];
            }

            if(!empty($param['orderBy'])) {
                $order_by = $param['orderBy'];
            }

            $orders->orderBy($order_by, $sort_by);

            if($f) {
                return $orders->paginate($per_page);
            }

            return $orders->get();
        }catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }


}
