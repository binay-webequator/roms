<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessQueueResults extends Model
{
    protected $table = 'process_queue_results';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'queue_batch_BatchNo', 'JobName', 'Status', 'Result', 'Request'
    ];

    /**
     *  update job status
     * @param $params
     * @throws \Exception
     */
    public static function updateJobStatus($params) {

        try {

            $result = '';
            if(!empty($params['data'])) {
                $result = json_encode($params['data']);
            }
            Self::where('queue_batch_BatchNo', $params['batch'])
                ->where('JobName', $params['command'])
                ->update(['Status' => $params['status'], 'Result'=>$result]);

        } catch (\Exception $e) {
            throw new \Exception($e->getFile());
        }
    }
}
