<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $table = 'products';
    public $primaryKey  = 'ItemId';
    public $incrementing= false;
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * @param string $customer_id
     * @throws \Exception
     */
    public static function getLastModifiedDate($customer_id){
        $last_modified = '';
        try {
            $qb = self::whereRaw('1=1');
            $qb->where('CustomerId', $customer_id);
            $rdata = $qb->max('LastModifiedDate');
            $last_modified = str_replace(' ','T', $rdata);
        } catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return $last_modified;
    }
    /**
     * @param $param
     * @return bool
     * @throws \Exception
     */
    public static function saveData($param){
        try{
            foreach($param as $row){
                $data['ItemId'] = $row['ItemId'];
                $data['Sku'] = $row['Sku'];
                $data['CustomerId'] = $row['ReadOnly']['CustomerIdentifier']['Id'];
                $data['Upc'] = $row['Upc'] ?? '';
                $data['Description'] = $row['Description'] ?? '';
                $data['Description2'] = $row['Description2'] ?? '';
                $data['InventoryCategory'] = $row['InventoryCategory'] ?? '';
                $data['Cost'] = $row['Cost'] ?? '0';
                $data['Price'] = $row['Price'] ?? '0';
                $data['CountryOfManufacture'] = $row['CountryOfManufacture'] ?? '';
                $data['HarmonizedCode'] = $row['HarmonizedCode'] ?? '';
                $data['LastModifiedDate'] = Carbon::parse($row['ReadOnly']['LastModifiedDate'])->format('Y-m-d H:i:s');

                self::updateOrCreate(['ItemId'=>$row['ItemId']], $data);
            }

        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }

        return true;

    }

    /**
     *  get all products
     * @return \App\Models\Product[]|\Illuminate\Database\Eloquent\Collection
     * $f - pagination true
     * @throws \Exception
     */
    public static function getProductList($param , $f = true) {
        try{
            DB::enableQueryLog();
            $per_page = 10;
            $order_by = 'updated_at';
            $sort_by = 'desc';

            if($f) {
                $products = self::whereRaw('1=1');
            }else{
                $products = DB::table('products')
                    ->select('Sku', 'Upc','ItemId','Price','created_at');
            }

            if(!empty($param['sku_type']) && !empty($param['sku'])){
                $search=trim($param['sku']);
                $s_type=$param['sku_type'];
                if($s_type==1){
                    $products->Where('Sku', 'LIKE', "%$search%");
                }elseif($s_type==2){
                    $products->Where('Sku', 'LIKE', "$search%");
                }elseif($s_type==3){
                    $products->Where('Sku', 'LIKE', "%$search");
                }elseif($s_type==4){
                    $products->Where('Sku', '=', $search);
                }
            }

            if(!empty($param['upc_type']) && !empty($param['upc'])){
                $search=trim($param['upc']);
                $s_type=$param['upc_type'];
                if($s_type==1){
                    $products->Where('Upc', 'LIKE', "%$search%");
                }elseif($s_type==2){
                    $products->Where('Upc', 'LIKE', "$search%");
                }elseif($s_type==3){
                    $products->Where('Upc', 'LIKE', "%$search");
                }elseif($s_type==4){
                    $products->Where('Upc', '=', $search);
                }
            }


            //$products->get();
            //print_r(DB::getQueryLog());
            if(!empty($param['limit'])) {
                $per_page =  $param['limit'];
            }
            if(!empty($param['byColumn'])) {
                $sort_by = $param['byColumn'];
            }

            if(!empty($param['orderBy'])) {
                $order_by = $param['orderBy'];
            }
            $products->orderBy($order_by, $sort_by);

            if($f) {
                return $products->paginate($per_page);
            }

            return $products->get();

        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}
