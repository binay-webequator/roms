<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueueBatch extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'BatchNo';

    protected $fillable = ['BatchNo', 'Status'];

    /**
     *  get process result jobs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany('App\Models\ProcessQueueResults');
    }
}
