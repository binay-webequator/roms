<?php

namespace App\Providers;

use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use App\Traits\Util;

class AppServiceProvider extends ServiceProvider
{
    use Util;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Update in database and send email //
        Queue::failing(function (JobFailed $event) {

            $payload = $event->job->payload()['data']['command'];
            if(!empty($payload)) {
                $payload = unserialize($payload);
                if(!empty($payload)) {

                    try {

                        if(!empty($payload->params)) {
                            $payload->batch = $payload->params;
                        }

                        // update job status //
                        if(!empty($payload->batch)) {
                            $params = $payload->batch;
                            $params['status'] = 500;
                            $params['data'] = '';
                            \App\Models\ProcessQueueResults::updateJobStatus($params);
                        }

                        // send email
                        $threepl_info = config("threepl.api_emails");
                        $this->sendSystemErrorEmail(
                            [
                                'to' => $threepl_info['exception'],
                                'message' => $event->exception
                            ]
                        );

                    } catch(\Exception $e) {
                        echo $e->getMessage().$e->getFile().$e->getLine();
                    }
                }

            }
        });
    }
}
