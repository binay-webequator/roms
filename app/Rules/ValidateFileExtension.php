<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

class ValidateFileExtension implements Rule
{
    protected $extensions = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($params = [])
    {
        $this->extensions = $params;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(empty($this->extensions)) {
            return false;
        }

        if(!$value instanceof UploadedFile) {
            return false;
        }

        $file_extension = $value->getClientOriginalExtension();
        if(in_array(strtolower($file_extension), $this->extensions)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $is_are = "is";
        if(count($this->extensions) > 1 ) {
            $is_are = "are";
        }
        return 'The :attribute, allowed file extension '.$is_are.' '.implode(", ", $this->extensions);
    }
}
