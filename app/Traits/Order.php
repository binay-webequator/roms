<?php
namespace App\Traits;

use App\Jobs\CreateOrder;
use App\Models\Country;
use App\ActionLog;
use App\Traits\ThreePL;
use App\Traits\Util;

trait Order
{
    protected $filename = null;
    use Util, ThreePL;

    /**
     * process csv file, create 3pl order data and push to create order job
     * @param $file
     * @return bool
     */
    public function processFile($file)
    {
        $this->filename  = $file;
        $data = $this->csvToArray($file);
        if(!empty($data)) {
            $orders = [];
            foreach ($data as $row) {
                if(empty($orders[$row['OrderNo']])) {
                    $orders[$row['OrderNo']] = $row;
                    $orders[$row['OrderNo']]['items'][] = [
                        'SKU' => $row['SKU'],
                        'Qty' => $row['Qty'],
                        'ItemUnitPrice' => $row['ItemUnitPrice']
                    ];

                } else {
                    $orders[$row['OrderNo']]['items'][] = [
                        'SKU' => $row['SKU'],
                        'Qty' => $row['Qty'],
                        'ItemUnitPrice' => $row['ItemUnitPrice']
                    ];
                }
            }

            // create order and queue to create order job //
            $move_dir = "Archive";
            try {
                $this->create3PLOrders($orders);

            } catch (\Exception $e) {
                report($e);
                $move_dir = "Exception";
            }


            // move file to archive //
            $finfo = pathinfo($file);
            $move_dir = $finfo['dirname'].'/'.$move_dir.'/';
            if(!is_dir($move_dir)) {
                mkdir($move_dir, 0775);
            }

            rename($file, $move_dir.$finfo['basename']);

        } else { // blank data

            return false;
        }

        return true;
    }

    /**
     * create order data and queue to create order data
     * @param $orders
     * @return bool
     */
    public function create3PLOrders($orders)
    {
        if(empty($orders)) {
            return false;
        }

        foreach ($orders as $order) {
            $three_pl_data = $this->create3PLData($order);

            //save in action log //
            $or_data = $three_pl_data;
            $action_log= new ActionLog();
            $action_log->rid=time();
            $action_log->module = "Order";
            $action_log->record_id=$order['OrderNo'];
            $action_log->user_id= 1;
            $action_log->label = json_encode(['OrderNo'=>$order['OrderNo']]);
            $action_log->filename=$this->filename;
            $action_log->order_data=json_encode($or_data);
            $action_log->save();
            $aid=$action_log->id;

            // queue order to post //
            CreateOrder::dispatch([
                'threepl_data' => $three_pl_data,
                'action_log_id'=>$aid
            ]);
        }

        return true;
    }

    /**
     *  create three pl data
     * @param $order
     * @return array
     */
    public function create3PLData($order)
    {
        $carrier_mode = Country::getCarrierAndMode();
        $ship_zip_point = config('threepl.api_credentials')['shipzippoint'];
        $shiptocountry = $order['DelCountryCode'];
        $soldtocountry = $order['InvoiceCountryCode'];
        /*
        if(empty($shiptocountry)) {
            $shiptocountry = $order['DelAddCounty'];
        }
        if(empty($shiptocountry)) {
            $soldtocountry = $order['InvoiceAddCounty'];
        }*/
        $shiptocountry = str_replace('.','', $shiptocountry);
        $soldtocountry = str_replace('.','', $soldtocountry);

        // change ship to country code to GB if NB or NI shiptocountry
        if(in_array(strtolower($shiptocountry), ['nb', 'ni', 'uk', 'united kingdom'])) {
            $shiptocountry = "GB";
        }

        $carrier = 'UPS'; $mode = 'UPS_STANDARD';
        $strtoupper = strtoupper($shiptocountry);
        if(!empty($carrier_mode[$strtoupper])) {
            $carrier = $carrier_mode[$strtoupper]['Carrier'];
            $mode = $carrier_mode[$strtoupper]['Mode'];
        }

        $three_pl_data = [
            "customerIdentifier" => [
                "id" =>config('threepl.api_credentials')['customer_id']
            ],
            "facilityIdentifier" => [
                "id" =>config('threepl.api_credentials')['facility_id']
            ],
            "referenceNum" => $order['OrderNo'],
            "externalId" => $order['OrderNo'],
            "poNum" => $order['PurchaseOrderNo'],
            "routingInfo" => [
                "carrier"=> $carrier,
                "mode"=> $mode,
                "shipPointZip"=> $ship_zip_point
            ],
            "shipTo" => [
                "companyName" => $order['DelCompanyName'] ?? "",
                "name" => ($order['DeliveryTitle'] ?? "").' '.($order['DeliveryForeNames'] ?? "").' '.($order['DeliverySurname'] ?? ""),
                "address1" => $order['DelAdd1'] ?? "",
                "address2" => ($order['DelAdd2'] ?? "").' '.($order['DelAdd3'] ?? ""),
                "city" => $order['DelAddTown'] ?? "",
                "state" => "",
                "zip" => $order['DelPostcode'] ?? "",
                "country" => $order['DelAddCounty'] ?? "",
                'phonenumber' => $order['DeliveryPersonalTel'] ?? "",
                'emailaddress' => $order['DeliveryPersonalEmail'] ?? ""
            ],
            "soldTo" => [
                "companyName" => $order['CompanyName'] ?? "",
                "name" => ($order['Title'] ?? "").' '.($order['ForeNames'] ?? "").' '.($order['Surname'] ?? ""),
                "address1" => $order['InvoiceAdd1'] ?? "",
                "address2" => ($order['InvoiceAdd2'] ?? "").' '.($order['InvoiceAdd3'] ?? ""),
                "city" => $order['InvoiceAddTown'] ?? "",
                "state" => "",
                "zip" => $order['InvoicePostcode'] ?? "",
                "country" => $soldtocountry,
                'phonenumber' => $order['DeliveryPersonalTel'] ?? "",
                'emailaddress' => $order['DeliveryPersonalEmail'] ?? ""
            ],
            'SavedElements' => [
                ['Name' => 'ClientCode', 'Value' => $order['ClientCode'] ?? ''],
                ['Name' => 'CampaignCode', 'Value' => $order['CampaignCode'] ?? ''],
                ['Name' => 'DueDate', 'Value' => $order['DueDate'] ?? ''],
                ['Name' => 'ShippingCode', 'Value' => $order['ShippingCode'] ?? '']
            ]
        ];

        // order items //
        foreach ($order['items'] as $item) {
            $three_pl_data['OrderItems'][] = [
                "itemIdentifier" => [
                    "Sku" => $item['SKU']
                ],
                "Qty" => (int)$item['Qty'],
                "FulfillInvSalePrice" => $order['ItemUnitPrice'] ?? 0.00,
                'SavedElements' => [
                    ['Name' => 'DeclaredValue', 'Value' => $order['ItemUnitPrice'] ?? 0.00],
                    ['Name' => 'Currency', 'Value' => $order['Currency'] ?? ''],
                    ['Name' => 'HTS', 'Value' => ""]
                ]
            ];
        }

        return $three_pl_data;
    }

}