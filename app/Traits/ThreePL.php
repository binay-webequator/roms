<?php
namespace App\Traits;
use Log;

trait ThreePL
{
    protected $access_token='';
    protected $threepl_credentials=[];
    protected $api_emails=[];

    /**
     *  ThreePL config validation and set up
     */
    public function threePLConfigValidation() {

        $errors = [];
        $threepl_info = config("threepl.api_credentials");
        if(empty($threepl_info)) {
            $errors[] = "3PL API details are missing";
        }

        // validate  every credentials //
        foreach ($threepl_info as $k=>$v) {
            if(empty($v)) {
                $errors[] = "3PL API key - ".strtoupper($k)." value are missing";
            }
        }

        $api_emails = config("threepl.api_emails");
        if(empty($api_emails)) {
            $errors[] = "API emails are missing";
        }

        if(empty($api_emails['exception'])) {
            $errors[] = "API exception email value missing";
        }

        if(!empty($errors)) {
            echo $msg = "Following 3PL setting validation failed\n\n";
            echo implode("\n", $errors);

            $message = $msg."<br>".implode("<br>", $errors);
            $this->sendSystemErrorEmail([
                'to' => config('local.admin_email'),
                'subject'=>'3PL settings validation failed',
                'message' => $message
            ]);

            exit(0);
        }

        $this->threepl_credentials = $threepl_info;
        $this->api_emails = $api_emails;

        return true;
    }

    /** Get access token
     * @param $threepl_credentials
     * @param $email_params
     * @return string
     * @throws \Exception
     */
    function getAccessToken($threepl_credentials, $email_params) {

        try{
            $three_pl_params = [
                'url'=>$threepl_credentials['url'].'/AuthServer/api/Token',
                'method'=>'POST',
                'content-type'=>'json',
                'content'=>[
                    'grant_type'=>$threepl_credentials['grant_type'],
                    'tpl'=>$threepl_credentials['tpl'],
                    'user_login_id'=>$threepl_credentials['user_login_id']
                ],
                'header-params'=>[
                    'Authorization'=>'Basic '.base64_encode($threepl_credentials['username'].':'.$threepl_credentials['password']),
                ]
            ];
            $threepl_response=$this->curlRequest($three_pl_params);

            if($threepl_response['status'] == 200 && !empty($threepl_response['response'])) {
                $this->access_token = json_decode($threepl_response['response'], 1)['access_token'];

            } else {

                // throw exception as token error //
                $msg = (!empty($threepl_response['error']) ? $threepl_response['error'] : $threepl_response['response']);
                throw new \Exception($msg);
            }

        } catch(\Exception $e){
            report($e);
            throw new \Exception($e->getMessage());
        }

        return $this->access_token;

    }

    /**
     * Post data to 3pl server
     * @param $params
     * @param $section
     * @return array $response
     */
    function postTo3PL($params, $section=''){
        $response = [];

        try {

            $response=$this->curlRequest($params);
            if ((in_array($response['status'], [200, 201, 203, 204]) && !empty($response['response'])) or
                (in_array($response['status'], [204]) && empty($response['response']))) {

                $rdata = json_decode($response['response'], 1) ?? [];

            } else {

                $msg = "API : {$section}<br>URL: {$params['url']}<br>Request:".json_encode($params)."<br>".(!empty($response['error']) ? $response['error'] : $response['response']);
                Log::error($msg);

                $this->sendSystemErrorEmail([
                    'to' => $this->api_emails['exception'],
                    'subject'=>'Traits/ThreePL',
                    'message' => $msg
                ]);
            }

        } catch (\Exception $e){
            report($e);
            throw new \Exception($e->getMessage());
        }

        return $response;
    }

    /**
     * @param $params array [url and token]
     * All get request can be full filled via this function.
     * @return array
     * @throws \Exception
     */

    function getThreePLData($params){
        $return =[];

        try{

            $three_pl_params  = [
                'url' => $params['url'],
                'method' => 'GET',
                'content-type' => 'application/json',
                'header-params' => [
                    'Authorization' => 'Bearer ' . $params['token'],
                ]
            ];
            $response=$this->curlRequest($three_pl_params);

            if($response['status'] == 200 && !empty($response['response'])) {
                $data = json_decode($response['response'], 1);
                $etag='';
                foreach($response['headers'] as $k=>$v){
                    if(strpos($v, 'ETag') !== false){ ;
                        $etag_array=explode(':', $v);
                        $etag = $etag_array[1];
                    }
                }

                $return =['data'=>$data, 'etag'=>$etag];

            } else {

                $msg = "3PL Get Data API <br><br> Api request : ".json_encode($three_pl_params)."<br><br>Api Response : ".(!empty($response['error']) ? $response['error'] : $response['response']);
                Log::error($msg);

                $this->sendSystemErrorEmail(
                    [
                        'to'=>$this->api_emails['exception'],
                        'subject'=>'Traits/ThreePL',
                        'message'=>$msg
                    ]
                );

            }
        } catch(\Exception $e){
            report($e);
            throw new \Exception($e->getMessage());
        }

        return $return;
    }

}
