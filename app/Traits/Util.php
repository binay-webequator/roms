<?php
namespace App\Traits;

use Illuminate\Support\Facades\File;
use App\Mail\JamesCargoEmail;
use Illuminate\Support\Facades\Mail;
use Log;

trait Util
{
    protected $ftp_in_folder = null;
    protected $ftp_out_folder = null;

    /** API config validation and setup
     * @param $section
     */
    public function apiConfigValidation($section='') {
        if(strtolower($section) == 'in') {
            $this->checkAndSetInFolder();

        } else if(strtolower($section) == 'out') {
            $this->checkAndSetOutFolder();

        } else {
            $this->checkAndSetInFolder();
            $this->checkAndSetOutFolder();
        }
    }

    /*
     * check and set ftp in folder
     */
    public function checkAndSetInFolder() {
        $ftp_in_folder = config('ftp.ftp_in_folder');
        /*if(empty($ftp_in_folder)) {
            $this->error('FTP IN folder name missing');
            exit(0);
        }*/
        $this->ftp_in_folder = $ftp_in_folder;
    }

    /**
     * check and set ftp out folder
     */
    public function checkAndSetOutFolder() {
        $ftp_out_folder = config('ftp.ftp_out_folder');
        if(empty($ftp_out_folder)) {
            $this->error('FTP OUT folder name missing');
            exit(0);
        }
        $this->ftp_out_folder = $ftp_out_folder;
    }

    /**
     * Process curl request
     *
     * @param array $data
     * @return array
     */
    function curlRequest($data)
    {
        // set url
        if (empty($data['url'])) {
            return false;
        }

        $url = $data['url'];

        // set method
        $method = 'GET';
        if (!empty($data['method'])) {
            $method = $data['method'];
        }

        // set content type and header //
        $post_data = $content = '';
        if (!empty($data['content'])) {
            $post_data = $content = $data['content'];
        }

        $content_type = "";
        if (!empty($data['content-type'])) {
            if ($data['content-type'] == 'json' || $data['content-type'] == 'application/json') {
                $content_type = "application/json";
                $post_data = json_encode($post_data);

            } elseif ($data['content-type'] == 'xml') {
                $content_type = "application/xml";
            } else {
                $content_type = $data['content-type'];
            }
        }

        // encode post params
        if (is_array($post_data)) {
            $post_data = http_build_query($post_data);
        }

        // set  header
        $header_params = array("Content-Type: $content_type");
        if (!empty($data['header-params'])) {
            foreach ($data['header-params'] as $k => $v) {
                $header_params[] = "$k: " . $v;
            }
        }
        $curlopt_header = isset($data['curlopt_header']) ? $data['curlopt_header'] : 1;

        // start curl request
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_VERBOSE, 0);
        curl_setopt($curl_handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_HEADER, $curlopt_header);
        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header_params);

        if (strtolower($method) != 'get') {
            curl_setopt($curl_handle, CURLOPT_POST, 1);
            curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $post_data);
        }

        $response = 'fail';
        $http_status = 200;
        $rheader = [];

        // get curl request to process var
        if (config("local.exe_curl")) { // execute only if on live or enable for an user
            try {
                $response = curl_exec($curl_handle);
                $header_size = curl_getinfo($curl_handle, CURLINFO_HEADER_SIZE);
                $rheader = substr($response, 0, $header_size);
                $rheader = explode("\r\n", $rheader);
                $response = substr($response, $header_size);
                $response = trim($response);
                $http_status = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

            } catch (\Exception $e) {
                return array('status' => 500, 'response' => $response, 'error' => $e->getMessage(), 'headers'=>$rheader);
            }
        } else {

            echo "\n\n*********************\nPlease update the config as currently curl execution is disabled\n**************************";

        }
        $error = curl_error($curl_handle);

        return array('status' => $http_status, 'response' => $response, 'error' => $error, 'headers'=>$rheader);
    }

    /**
     * For testing of email sending
     */
    function sendSystemErrorEmail($params)
    {
        $params['subject'] = config('app.name').' - Some thing went wrong'.(!empty($params['subject']) ? '('.$params['subject'].')' : '');

        if(empty($params['template'])){
            $params['template'] = 'jc';
        }

        return $this->sendEmail($params);
    }

    /**
     *  send email
     * @param $params
     * @return bool
     */
    function sendEmail($params) {
        try {
            Mail::to($params['to'])->send(new JamesCargoEmail($params));

        } catch (\Exception $e) {
            report($e);
            echo $e->getMessage();
        }

        return true;
    }

    /**
     * For creating new folder
     * @param $filepath
     * @return bool
     */
    function createFolder($filepath) {

        if(!empty($filepath)){
            try {
                File::isDirectory($filepath) or File::makeDirectory($filepath, 0755, true, true);
            } catch(\Exception $e) {
                return $this->error($e->getMessage());
            }
        }

        return true;
    }

    /**
     *  For recursively files
     * @param $path_dir
     * @param array $files
     * @param string $fprefix
     * @param string $extn
     * @return array
     */
    function getDirFiles($path_dir, &$files=[], $fprefix='', $extn=''){

        if(!file_exists($path_dir)) {
            $this->error($path_dir.' - Directory doesnt exist');
            $this->sendSystemErrorEmail(
                [
                    'to'=>$this->api_emails['exception'],
                    'subject'=>'Traits/Util',
                    'message'=>$path_dir.' - Directory doesnt exist'
                ]
            );

            return false;
        }

        if(!empty($path_dir)){
            if(!empty($fprefix)) {
                $fprefix ="{".$fprefix."}";
            }
            chdir($path_dir);
            $dirs=glob("{$fprefix}*.{xlsx,XLSX,xls,XLS,csv,CSV,xml,XML}", GLOB_BRACE) ;
            if(!empty($extn)) {
                $dirs=glob("{$fprefix}*.{$extn}", GLOB_BRACE) ;
            }

            foreach ($dirs as $dir) {
                if (!in_array(strtolower($dir), array('.', '..'))) {

                    // if current element is dir - then read all files //
                    if (is_dir($path_dir . $dir)) {
                        if(!in_array(strtolower($dir), ['archive', 'error'])) {
                            $this->getDirFiles($path_dir . $dir . DIRECTORY_SEPARATOR, $files, $fprefix, $extn);
                        }

                    } else { // a single file
                        $files[] = $path_dir . $dir;
                    }
                }
            }
        }

        return $files;
    }

    /** Get new filename if already uploaded
     * @param $files
     * @param $f
     * @return string
     */
    function getFilename($files, $f) {
        if(in_array($f, $files)) {
            $finfo = pathinfo($f);
            $farray=explode("_", $finfo['filename']);
            if(count($farray) > 3) {
                $farray = array_reverse($farray);
                $ccntr = (int)$farray[0];
                $f=str_replace('_'.$ccntr.'.'.$finfo['extension'], '', $f).'_'.($ccntr + 1).'.'.$finfo['extension'];
            } else {
                $f = $finfo['filename'].'_1.'.$finfo['extension'];
            }

            return $this->getFilename($files, $f);
        }

        return $f;
    }

    /**
     * CSV to array
     * @param string $filename
     * @param string $delimiter
     * @return array|bool
     */
    function csvToArray($filename = '', $delimiter = ',') {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header) {
                    $header = $row;
                    foreach ($header as $i=>$f) {
                        $f = trim($f);
                        $header[$i] = $f;
                    }
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * @param $path path with expression for multiple files
     *
     * @return array|false
     */
    function getAllFiles($path){
        return glob($this);
    }

}
