<?php

namespace App\WMS;

use App\WMS\Exceptions\ClassInvalidRequest;
use Illuminate\Support\Facades\Http as Guzzle;
use App\WMS\Contracts\RequestableInterface;

class Client implements RequestableInterface {

    /**
     * @var Illuminate\Support\Facades\Http
     */
    public static $client;

    /**
     * @var GuzzleHttp\Request
     */
    protected $request;

    /**
     * @var GuzzleHttp\Response
     */
    protected $response;

    /**
     * API Key
     *
     * The custom API User provided by WMS
     *
     * @var string
     */
    protected $user;

    /**
     * API Key
     *
     * The custom API Password provided by WMS
     *
     * @var string
     */
    protected $password;

    /**
     * Bearer Token
     *
     * @var string
     */
    protected $token;

    /**
     * URL
     *
     * The URL that is set to query the WMS API to get the tracking information
     *
     * @var string
     */
    protected $url;


    /**
     * Currently this package doesn't support XML
     * but overtime this would be part of that support
     *
     * @var string
     */
    protected $dataFormat = 'json';

    protected $tpl;
    protected $customer_id;
    protected $facility_id;
    protected $user_login_id;
    protected $grant_type;

    protected $debug = false;


    /**
     * @param Guzzle $client
     * @param        $credentials
     */
    public function __construct(Guzzle $client, $credentials)
    {
        self::$client = $client;
        $this->url = $credentials['url'];
        $this->user = $credentials['username'];
        $this->password = $credentials['password'];
        $this->tpl = $credentials['tpl'];
        $this->customer_id = $credentials['customer_id'];
        $this->facility_id = $credentials['facility_id'];
        $this->user_login_id = $credentials['user_login_id'];
        $this->grant_type = $credentials['grant_type'];
    }

    /**
     * Get WMS User
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get WMS Password
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get Request should return status 200
     * @param $endpoint
     * @param null $query
     * @param array $headers
     *
     * @return GuzzleHttp\Response|\Illuminate\Http\Client\Response
     */
    public function get($endpoint, $query = null, $headers=[])
    {
        if(!$this->token)
            $this->getAuth();

        try {

            $url = $this->buildUrl($endpoint);
            $this->response = self::$client::withToken($this->token)->withOptions([
                'debug' => $this->debug
            ])->get($url, $query);
            if (!$this->response->ok()) {
                //todo: add options
            }

            return $this->response;

        } catch (\Throwable $e) {
            report($e);
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Post
     *
     * @param $endpoint
     * @param $data
     * @throws \Exception
     * @return Client
     */
    public function post($endpoint, $data)
    {
        if(!$this->token)
            $this->getAuth();

        try {

            $url = $this->buildUrl($endpoint);
            $this->response = self::$client::withToken($this->token)->withOptions([
                'debug' => $this->debug
            ])->post($url, $data);

            return $this->response;

        } catch (\Throwable $e) {
            report($e);
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Put
     *
     * @param $endpoint
     * @param $data
     *
     * @return Client
     */
    public function put($endpoint, $data)
    {
        return $this->buildRequest($endpoint, 'PUT', $data);
    }

    /**
     * Delete
     *
     * @param $endpoint
     *
     * @return Client
     * @internal param $data
     *
     */
    public function delete($endpoint)
    {
        return $this->buildRequest($endpoint, 'DELETE');
    }

    /**
     * Build Request
     *
     * build up request including authentication, body,
     * and string queries if necessary. This is where the bulk
     * of the data is build up to connect to Teamwork with.
     *
     * @param        $endpoint
     * @param string $action
     * @param array $params
     *
     * @param null $query
     * @return $this
     */
    public function buildRequest($endpoint, $action, $params = [], $query = null)
    {
        $headers = ['Content-Type'=>'application/xml'];
        if($this->dataFormat == "json")
            $headers['Accept'] = 'application/json';

        $response = Guzzle::withHeaders($headers);

        if (count($params) > 0)
        {
            $options['form_params'] = json_encode($params);
        }

        if ($query != null)
        {
            $options['query'] = $query;
        }

        /*$this->request = $this->client->request(
            $action,
            $this->buildUrl($endpoint),
            $options
        );*/


        return $response;
    }

    /**
     * Response
     *
     * this send the request from the built response and
     * returns the response as a JSON payload
     */
    public function response()
    {
        return $this->response;
    }


    /**
     * Build Url
     *
     * builds the url to make the request to Teamwork with
     * and passes it into Guzzle. Also checks if trailing slash
     * is present.
     *
     * @param $endpoint
     *
     * @return string
     */
    public function buildUrl($endpoint)
    {
        if (filter_var($endpoint, FILTER_VALIDATE_URL))
        {
            return $endpoint;
        }

        if (substr($this->url, -1) != '/')
        {
            $this->url = $this->url . '/';
        }

        return $this->url . $endpoint ;
    }

    /**
     * Build Query String
     *
     * if a query string is needed it will be built up
     * and added to the request. This is only used in certain
     * GET requests
     *
     * @param $query
     */
    public function buildQuery($query)
    {

        $q = $this->request->getQuery();

        foreach ($query as $key => $value)
        {
            $q[$key] = $value;
        }
    }

    /**
     * Get Request
     *
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getCustomerID()
    {
        return $this->customer_id;
    }

    /**
     * @return mixed
     */
    public function getFacilityID()
    {
        return $this->facility_id;
    }

    /**
     * @param bool $val
     */
    public function setDebug($val = false)
    {
        $this->debug = $val;
    }

    /**
     * @return bool
     */
    public function getDebug()
    {
        return $this->debug;
    }

    /**
     * @return \Illuminate\Http\Client\Response
     * @throws \Exception
     */
    public function getAuth()
    {
        try {

            $url = $this->url . "/AuthServer/api/Token";
            $response = self::$client::withBasicAuth($this->user, $this->password)->post($url, [
                "grant_type" => $this->grant_type,
                "tpl" => $this->tpl,
                "user_login_id" => $this->user_login_id
            ]);
            if ($response->ok()) {
                $data = $response->json();
                $this->token = $data['access_token'];
            } else {
                $data = "Failed to get Access token, check logs for more details\n\n".$response->json();
                throw new \Exception($data);
            }

            return $response;

        } catch (\Throwable $e) {
            report($e);
            throw new \Exception($e->getMessage());
        }
    }
}
