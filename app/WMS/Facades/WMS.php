<?php

namespace App\WMS\Facades;

use App\WMS\Orders;
use Illuminate\Support\Facades\Facade;

/**
 * Class WMS
 * @package App\WMS\Facades
 *
 * @method static Orders Orders()
 */
class WMS extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'WMS3PLC';
    }
}
