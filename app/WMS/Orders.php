<?php


namespace App\WMS;


class Orders extends WMS
{
    public function getOrders($last_updated='', $customer_id=0, $facility_id =0)
    {
        $auth = $this->client->getAuth();
    }

    /**
     * Pull the shipped orders from 3PL for given customer and facility based on the last modified date
     *
     * @param date $last_modified
     * @param int $page
     * @param int $customer_id
     * @param int $facility_id
     */
    public function getShipments($last_modified, $page=1, $customer_id=0, $facility_id=0)
    {
        $customer_id = $customer_id > 0? $customer_id : $this->client->getCustomerID();

        //Enable verbose
        //$this->client->setDebug(true);

        $response = $this->client->get('orders', [
            'detail' => 'All',
            'pgnum' => $page,
            'sort' => 'LastModifiedDate',
            'rql' => "ReadOnly.CustomerIdentifier.Id=={$customer_id};ReadOnly.isClosed==True". (
                $last_modified ? ";LastModifiedDate=ge={$last_modified}" : ""
                )
        ]);
        return $response;
    }


    /**
     * Push order to WMS and get the response back
     *
     * @param $data
     * @return mixed
     */

    public function pushOrder($data)
    {
        return $this->client->post('orders', $data);
    }

}