<?php

namespace App\WMS;

use App\WMS\Contracts\RequestableInterface;

abstract class WMS
{
    /**
     * @var RequestableInterface
     */
    protected $client;

    protected $endpoint = "trackingcore/dpd/";
    /**
     * Desk constructor.
     *
     * @param RequestableInterface $client
     * @param $id
     */
    public function __construct(RequestableInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Are Arguments Valid
     *
     * @param array    $args
     * @param string[] $accepted
     *
     * @return null|bool
     */
    protected function areArgumentsValid($args, array $accepted)
    {
        if ($args == null)
        {
            return;
        }

        foreach ($accepted as $accept)
        {
            if (array_key_exists($accept, $args))
            {
                return true;
            }
        }

        throw new \InvalidArgumentException('This call only accepts these arguments: ' . implode(" | ",$accepted));
    }
}