<?php

namespace App\WMS;

use App\WMS\Client;
use Illuminate\Support\Facades\Http as Guzzle;
use Illuminate\Support\ServiceProvider;

class WMSServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('WMS3PLC', function($app)
        {
            $client = new Client(new Guzzle,
                $app['config']->get('threepl.api_credentials')
            );

            return new \App\WMS\Factory($client);
        });

        $this->app->bind('App\WMS\Factory', 'WMS3PLC');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
