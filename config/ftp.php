<?php

return [
    'ftp_in_folder'=>env('JC_IN_XML_FOLDER', 'inbound'),
    'ftp_out_folder'=>env('JC_OUT_XML_FOLDER', 'outbound')
];