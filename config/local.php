<?php
return [
    'exe_curl' => env('APP_EXE_CURL', false),
    'three_pl_customer_id'=>env('3PL_API_CUSTOMER_ID','14'),
    'order_download_timeframe' => env('ORDER_DOWNLOAD_TIMEFRAME',24),
    'admin_email'=>env('ADMIN_EMAIL',''),
];
