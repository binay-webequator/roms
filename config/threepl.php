<?php

return [
    'api_credentials' => [
        'url'=>env('3PL_API_BASE_URL'),
        'grant_type' => env('3PL_API_GRANT_TYPE'),
        'tpl' => env('3PL_API_TPL'),
        'username'=>env('3PL_API_USERNAME'),
        'password'=>env('3PL_API_PASSWORD'),
        'user_login_id' => env('3PL_API_USER_LOGIN_ID'),
        'customer_id'=>env('3PL_API_CUSTOMER_ID'),
        'facility_id'=>env('3PL_API_FACILITY_ID'),
        'shipzippoint'=>env('3PL_API_SHIP_ZIP_POINT', '')
    ],
    'api_emails'=>[
        'empty_file'=>env('3PL_API_EMPTY_FILE_EMAIL'),
        'corrupt_file'=>env('3PL_API_CORRUPT_FILE_EMAIL'),
        'ftp_down'=>env('3PL_API_FTP_DOWN_EMAIL'),
        'params_missing'=>env('3PL_API_PARAMS_MISSING_EMAIL'),
        'duplicate_file'=>env('3PL_API_DUPLICATE_FILE_EMAIL'),
        'exception'=>env('3PL_API_EXCEPTION_EMAIL'),
        'notification'=>env('3PL_API_NOTIFICATION_EMAIL'),
    ]

];
