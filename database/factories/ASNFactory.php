<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ASN;
use Faker\Generator as Faker;

$factory->define(ASN::class, function (Faker $faker) {
    return [
        'ReceiverId' =>$faker->regexify('[0-9]{8}'),
        'ReferenceNum'=>$faker->regexify('[0-9]{8}'),
        'LastModifiedDate'=> \Carbon\Carbon::parse(now())->format('Y-m-d H:i:s'),
    ];
});
