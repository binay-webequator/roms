<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\ASNItem::class, function (Faker $faker) {
    return [
        'ReceiveItemId' =>$faker->regexify('[0-9]{6}'),
        'ReceiverId' =>$faker->regexify('[0-9]{8}')
    ];
});
