<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
       'OrderId' =>$faker->regexify('[0-9]{8}'),
        'ReferenceNum'=>$faker->regexify('[0-9]{8}'),
        'ShipToCountry'=>$faker->randomElement(['AD','AE','AF']),
        'BatchID' =>$faker->regexify('[0-8]{9}'),
        'GroupType'=>$faker->randomElement(['abc','xyz','stu']),
        'ThreeplBatchID'=>$faker->regexify('[0-7]{9}'),
        'LastModifiedDate'=> \Carbon\Carbon::parse(now())->format('Y-m-d H:i:s'),
    ];
});
