<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'OrderItemId' =>$faker->regexify('[0-9]{6}'),
        'OrderId' =>$faker->regexify('[0-9]{8}'),
        'Sku' =>$faker->regexify('[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}')
    ];
});
