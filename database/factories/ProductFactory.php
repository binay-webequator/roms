<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        "ItemId"=>$faker->regexify('[0-9]{8}'),
        "Sku" =>$faker->regexify('[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}'),
        "LastModifiedDate" => \Carbon\Carbon::parse(now())->format('Y-m-d H:i:s'),
        "CustomerId" =>config('local.three_pl_customer_id'),
        "Upc" =>$faker->randomDigit(16),
    ];
});
