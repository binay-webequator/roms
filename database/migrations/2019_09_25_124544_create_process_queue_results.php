<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessQueueResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_queue_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('queue_batch_BatchNo', 20);
            $table->string('JobName', 100);
            $table->smallInteger('Status')->default(false);
            $table->text('Request')->nullable();
            $table->text('Result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_queue_results');
    }
}
