<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBatches extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function(Blueprint $table)
        {
            $table->bigInteger('batchID', true);
            $table->timestamp('batchcreated')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('batch');//->unique('batch');
            $table->string('batchtype', 50)->nullable()->index();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('batches');
    }

}
