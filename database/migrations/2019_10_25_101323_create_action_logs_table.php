<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rid');
            $table->integer('user_id')->nullable();
            $table->string('module', 100)->nullable();
            $table->bigInteger('record_id')->nullable();
            $table->string('label',100)->nullable();
            $table->smallInteger('status')->nullable()->default('0');
            $table->string('filename', 255)->nullable();
            $table->text('result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_logs');
    }
}
