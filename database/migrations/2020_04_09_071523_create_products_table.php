<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  //TODO Field length is taken from api as Abdul suggest
        Schema::create('products', function (Blueprint $table) {
            $table->integer('ItemId')->comment('3PL Id')->primary();
            $table->string('Sku',40)->nullable();
            $table->integer('CustomerId')->default('0')->nullable();
            $table->string('Upc',255)->nullable();
            $table->string('Description',255)->nullable();
            $table->string('Description2',255)->nullable();
            $table->string('InventoryCategory',255)->nullable();
            $table->decimal('Cost',8,2)->default('0')->nullable();
            $table->decimal('Price',8,2)->default('0')->nullable();
            $table->string('CountryOfManufacture',255)->nullable();
            $table->string('HarmonizedCode',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
