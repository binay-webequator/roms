<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigInteger('OrderId')->comment('3PL Id')->primary();
            $table->integer('CustomerId')->default(0)->nullable();
            $table->string('ReferenceNum', 255)->nullable();
            $table->string('ExternalId', 10)->nullable();
            $table->text('Description')->nullable();
            $table->string('PoNum', 255)->nullable();
            $table->dateTime('EarliestShipDate')->nullable();
            $table->dateTime('ShipCancelDate')->nullable();
            $table->text('Notes')->nullable();
            $table->decimal('NumUnits1')->default('0.00')->nullable();
            $table->decimal('NumUnits2')->default('0.00')->nullable();
            $table->decimal('TotalWeight')->default('0.00')->nullable();
            $table->decimal('TotalVolume')->default('0.00')->nullable();
            $table->string('BillingCode',255)->nullable();
            $table->string('AsnNumber',255)->nullable();
            $table->decimal('UpsServiceOptionCharge')->default('0.00')->nullable();
            $table->decimal('UpsTransportationCharge')->default('0.00')->nullable();
            $table->boolean('AddFreightToCod')->default(false);
            $table->boolean('UpsIsResidential')->default(false);
            $table->text('ShippingNotes')->nullable();
            $table->string('MasterBillOfLadingId', 100)->nullable();
            $table->string('InvoiceNumber', 100)->nullable();

            $table->string('ShipToCompanyName', 255)->nullable();
            $table->string('ShipToTitle', 20)->nullable();
            $table->string('ShipToName', 100)->nullable();
            $table->string('ShipToAddress1', 255)->nullable();
            $table->string('ShipToAddress2', 255)->nullable();
            $table->string('ShipToCity', 100)->nullable();
            $table->string('ShipToZip', 10)->nullable();
            $table->string('ShipToCountry', 3)->nullable();
            $table->string('ShipToPhoneNumber', 20)->nullable();
            $table->string('ShipToEmailAddress', 150)->nullable();
            $table->string('ShipToFax', 20)->nullable();

            $table->string('SoldToCompanyName', 255)->nullable();
            $table->string('SoldToTitle', 20)->nullable();
            $table->string('SoldToName', 100)->nullable();
            $table->string('SoldToAddress1', 255)->nullable();
            $table->string('SoldToAddress2', 255)->nullable();
            $table->string('SoldToCity', 100)->nullable();
            $table->string('SoldToZip', 10)->nullable();
            $table->string('SoldToCountry', 3)->nullable();
            $table->string('SoldToPhoneNumber', 20)->nullable();
            $table->string('SoldToEmailAddress', 150)->nullable();
            $table->string('SoldToFax', 20)->nullable();

            $table->mediumText('PackageInfo')->nullable();
            $table->tinyInteger('Status')->default(0);
            $table->dateTime('LastModifiedDate')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
