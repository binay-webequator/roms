<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigInteger('OrderItemId')->comment('3PL ID')->primary();
            $table->bigInteger('OrderId')->index();
            $table->string('Sku', 40);
            $table->string('Qualifier', 255)->nullable();
            $table->string('ExternalId', 50)->nullable();
            $table->integer('Qty')->default(0);
            $table->integer('SecondaryQty')->default(0);
            $table->string('LotNumber', 255)->nullable();
            $table->string('SerialNumber', 255)->nullable();
            $table->dateTime('ExpirationDate')->nullable();
            $table->decimal('WeightImperial', 8,4)->default(0.0000);
            $table->decimal('WeightMetric', 8,4)->default(0.0000);
            $table->text('Notes')->nullable();
            $table->decimal('FulfillInvSalePrice', 8,4)->default(0.0000);
            $table->decimal('FulfillInvDiscountPct', 8, 4)->default(0.0000);
            $table->decimal('FulfillInvDiscountAmt', 8,4)->default(0.0000);
            $table->text('FulfillInvNote')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
