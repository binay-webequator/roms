<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asns', function (Blueprint $table) {
            $table->bigInteger('ReceiverId')->comment('3PL Id')->primary();
            $table->string('ReferenceNum',255);
            $table->integer('CustomerId')->default('0')->nullable();
            $table->string('PoNum',255)->nullable();
            $table->string('ExternalId',255)->nullable();
            $table->string('ReceiptAdviceNumber',255)->nullable();
            $table->dateTime('ArrivalDate')->nullable();
            $table->dateTime('ExpectedDate')->nullable();
            $table->text('Notes')->nullable();
            $table->string('ScacCode',255)->nullable();
            $table->string('Carrier',255)->nullable();
            $table->string('BillOfLading',255)->nullable();
            $table->string('DoorNumber',255)->nullable();
            $table->string('TrackingNumber',255)->nullable();
            $table->string('TrailerNumber',255)->nullable();
            $table->string('SealNumber',255)->nullable();
            $table->decimal('NumUnits1','8','2')->default('0')->nullable();
            $table->decimal('NumUnits2','8','2')->default('0')->nullable();
            $table->decimal('TotalWeight','8','2')->default('0')->nullable();
            $table->decimal('TotalVolume','8','2')->default('0')->nullable();
            $table->dateTime('LastModifiedDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asns');
    }
}
