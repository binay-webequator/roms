<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsnItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn_items', function (Blueprint $table) {
            $table->bigInteger('ReceiveItemId')->comment('3PL ID')->primary();
            $table->bigInteger('ReceiverId')->index();
            $table->dateTime('FullyShippedDate')->nullable();
            $table->decimal('ExpectedQty','8','2')->default('0')->nullable();
            $table->dateTime('OnHoldDate')->nullable();
            $table->string('Qualifier',255)->nullable();
            $table->string('ExternalId',255)->nullable();
            $table->decimal('Qty','8','2')->default('0')->nullable();
            $table->decimal('SecondaryQty','8','2')->default('0')->nullable();
            $table->string('LotNumber',255)->nullable();
            $table->string('SerialNumber',255)->nullable();
            $table->dateTime('ExpirationDate')->nullable();
            $table->decimal('Cost','8','2')->default('0')->nullable();
            $table->decimal('WeightImperial','8','2')->default('0')->nullable();
            $table->decimal('WeightMetric','8','2')->default('0')->nullable();
            $table->tinyInteger('OnHold')->default('0')->nullable();
            $table->string('OnHoldReason',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn_items');
    }
}
