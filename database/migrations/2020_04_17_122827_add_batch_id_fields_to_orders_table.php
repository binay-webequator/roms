<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBatchIdFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->bigInteger('BatchID')->nullable()->after('PackageInfo');
            $table->string('GroupType',255)->nullable()->after('BatchID');
            $table->bigInteger('ThreeplBatchID')->nullable()->after('GroupType');
            $table->dateTime('ReadyForUpload')->nullable()->after('ThreeplBatchID');
            $table->string('OrderNo',70)->nullable()->after('ReadyForUpload');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
