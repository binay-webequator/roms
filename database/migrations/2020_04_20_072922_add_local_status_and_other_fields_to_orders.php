<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocalStatusAndOtherFieldsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->tinyInteger('LocalStatus')->nullable()->default('0')->after('Status');
            $table->integer('QueueBatchID')->nullable()->index()->after('ThreeplBatchID');
            $table->string('UniqueIdentifier')->nullable()->after('GroupType');
            $table->dateTime('Uploaded3PL')->nullable()->index()->after('ReadyForUpload');
            $table->index('ReadyForUpload');
            $table->string('XMLNameREST',255)->nullable()->after('Uploaded3PL');
            $table->char('DeliveryTyp',4)->nullable()->after('PackageInfo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('LocalStatus');
            $table->dropColumn('QueueBatchID');
            $table->dropColumn('UniqueIdentifier');
            $table->dropColumn('Uploaded3PL');
            $table->dropColumn('XMLNameREST');
            $table->dropColumn('DeliveryTyp');
        });
    }
}
