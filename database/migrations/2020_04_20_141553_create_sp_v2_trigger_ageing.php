<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpV2TriggerAgeing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V2_Trigger_Ageing');

        $sql = "CREATE DEFINER=`octopus`@`%` PROCEDURE `V2_Trigger_Ageing`(in dt text)
BEGIN

-- update ship to and sold to country with GB where country code is NB or NI
	update orders set SoldToCountry='GB' where SoldToCountry IN('NB', 'NI');
	update orders set ShipToCountry='GB' where ShipToCountry IN('NB', 'NI');

    set @dd = concat('^(',dt,')');

-- update order initial local status for batching
	update orders set LocalStatus=1  where batchid is null and uniqueidentifier is null and readyforupload is null and uploaded3pl is null  and convert(OrderDate, date) REGEXP @dd;

-- Batching for Single SKU Multi Qty orders

	CALL `V3_GroupSingleLineSingleOrders`();



-- Batching for Single SKU Multi Qty orders

	CALL `V3_GroupSingleLineMultiQtyOrders`();



-- Batching Two Line Orders

	CALL `V3_GroupTwoLineMultiQtyOrders`();



-- Batching for multi orders

	CALL `V3_GroupMultiOrders`();



-- FOR MAPFORCE

	 CALL V2_XMLLoop(); -- to create csvs of 500 per file (didn




END";
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V2_Trigger_Ageing');
    }
}
