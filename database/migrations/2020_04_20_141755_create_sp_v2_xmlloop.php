<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpV2Xmlloop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V2_XMLLoop');

        $sql = "CREATE DEFINER=`octopus`@`%` PROCEDURE `V2_XMLLoop`()
    COMMENT 'Creates JSON with 1000 orders'
BEGIN

	DECLARE ML int DEFAULT 500; 				/* max lines will be 500*/

	DECLARE RowCount int;					/* NUMBER OF LINES IN TOTAL*/

	DECLARE NumPasses int;					/* number of times to loop*/

	DECLARE CompPass int DEFAULT 0;

	DECLARE CurrentRow int DEFAULT 1;

 /* SET ROW COUNT*/





		select count(o.OrderId) into RowCount	from orders o join order_items l on o.OrderId = l.OrderId

 		where l.SKU is not null and date(o.readyforupload) = date(now()) and o.uploaded3PL is null and o.batchID is not null and o.readyforupload is not null

		 	and XMLNameREST is null  ;


	select CEILING(RowCount/ML) into NumPasses;



	drop table if exists TempRESTLoop;

	CREATE TEMPORARY TABLE TempRESTLoop (apixml varchar(500), OrderId bigint, TempID INT(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`TempID`)) AUTO_INCREMENT=1;



   WHILE CompPass < NumPasses DO

 	select ML*CompPass into CurrentRow;



-- /*STORE TO TABLE*/



		insert into TempRESTLoop (apixml,OrderId)

		select concat(DATE_FORMAT(now(), \"%Y%c%d%k\"),'-',convert((CurrentRow/ML),decimal(10,0))), o.OrderId

		from orders o join order_items l on o.OrderId = l.OrderId

		where l.SKU is not null and date(o.readyforupload) = date(now())	and o.uploaded3PL is null and o.batchID is not null and o.readyforupload is not null

			and XMLNameREST is null

		order by o.batchid, o.OrderId asc

		limit CurrentRow,ML;



     SET CompPass = CompPass + 1;

   END WHILE;



/*update orders table*/



 	update orders o, TempRESTLoop r

	set o.XMLNameREST = r.apixml

	where o.OrderId	 = r.OrderId and XMLNameREST is null;



  truncate table  TempRESTLoop;

 									/*Cust 2 = TEST / 5 = SUPREME*/

    Select count(distinct OrderId) as cntr From (Select o.XMLNameREST 'APIname', 5 'CustomerID', 1 'FacilityID',o.grouptype,
	o.batchID, o.OrderId,o.orderno,o.orderdate,o.SoldToCompanyName,
	o.SoldToName,o.SoldToAddress1,o.SoldToCity,o.SoldToZip ,o.SoldToCountry,o.SoldToPhoneNumber,o.SoldToEmailAddress,c.upscode 'shiptomethod',
	l.OrderItemId,l.SKU,l.Qty, o.ShipToCompanyName, o.ShipToName, o.ShipToAddress1, o.ShipToAddress2, o.ShipToZip, o.ShipToCity,
 o.ShipToCountry, o.ShipToPhoneNumber, o.ShipToEmailAddress
	from orders o join order_items l on o.OrderId = l.OrderId	join country c on o.ShipToCountry = c.countrycode
	where l.SKU is not null
	and date(o.readyforupload) = date(now())
	and o.uploaded3PL is null and o.batchID is not null and o.readyforupload is not null
	order by XMLNameREST, o.batchid, o.OrderId asc) as PL ;



END";
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V2_XMLLoop');
    }
}
