<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpV3GroupMultiOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V3_GroupMultiOrders');


        $sql = "CREATE DEFINER=`octopus`@`%` PROCEDURE `V3_GroupMultiOrders`()
    COMMENT 'Groups multi line orders into batches of 50 items'
BEGIN
-- FOR THE LOOP
	DECLARE ML int DEFAULT 50; 			/* max lines will be 50*/
	DECLARE NumPasses int;					/* number of times to loop*/
	DECLARE CompPass int DEFAULT 0;
	DECLARE CurrentRow int DEFAULT 1;


/*PREP WORK*/
	drop table if exists Temp;
	CREATE TEMPORARY TABLE Temp (SKU varchar(50), Vol int, `Weighting` INT(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`Weighting`)) AUTO_INCREMENT=1;

	insert into Temp (SKU, Vol)
	select SKU, count(OrderItemId)
	from order_items
	where OrderId in (select OrderId from orders where batchid is null and uniqueidentifier is null and LocalStatus=1)
	group by SKU
	order by 2 desc;

		drop table if exists TempGrouped;
		CREATE TEMPORARY TABLE TempGrouped (OrderId bigint, OrderNo varchar(50), Batchid int, SKU text, `Weighting` text, MainSKU varchar(50), Qty int);

-- Temp table to store cumulative sum of QTY required to batch into groups of 50 skus
		drop table if exists TempRunTot;
		CREATE TEMPORARY TABLE TempRunTot (OrderId bigint, OrderNo varchar(50), SKU text, Weighting text, MainSKU varchar(50), Qty int , RunTot int);


-- SAVER
		insert into TempGrouped
		select s.OrderId, s.orderno, s.batchid,
		 group_concat(l.SKU) 'SKU', group_concat(t.Weighting) 'Weighting',	concat('Saver',' ',left(	group_concat(t.Weighting),LENGTH(SUBSTRING_INDEX(Group_concat(t.Weighting), ',', 2)))) 'MainSKU',
		 sum(l.Qty) 'Qty'
		from orders s 		join order_items l on s.OrderId = l.OrderId 		join country c on s.ShipToCountry = c.countrycode		join Temp t on l.SKU = t.SKU
		where grouptype is null and uniqueidentifier is null and c.upscode = 'UPS Saver'  and s.LocalStatus=1
		group by s.orderno, s.uniqueidentifier, s.grouptype, s.batchid, s.XMLNameREST, c.upscode, s.OrderId
		order by 6,4 asc;

-- Store Cumulative Value
		set @csum := 0;
		insert into TempRunTot
		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , (@csum := @csum + Qty)
		from TempGrouped
		order by MainSKU, SKU asc;

		drop table if exists TempBatch;
		CREATE TEMPORARY TABLE TempBatch (OrderId bigint, OrderNo varchar(50), SKU text, Weighting text, MainSKU varchar(50), Qty int , Batch varchar(100));

-- The LOOP    /*added 1 to count on 2019-03-09 to see if it resolves the missing batching DM*/
		select ceiling(max(RunTot)/ML)+1 from TempRunTot into NumPasses;

		   WHILE CompPass <= NumPasses DO
		 	select ML*CompPass into CurrentRow;

	 			insert into TempBatch
		 		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , concat(convert(CurrentRow/ML,decimal(10,0)),DATE_FORMAT(now(), \"%Y%c%d%k%i\"))
				from TempRunTot
			 	where RunTot between CurrentRow+1 and ML+CurrentRow;

				insert into batches (batch, batchtype)
				select  concat(convert(CurrentRow/ML,decimal(10,0)),DATE_FORMAT(now(), \"%Y%c%d%k%i\")), 	DATE_FORMAT(now(), \"%Y%c%d%k%i\");

			SET CompPass = CompPass + 1;
			END WHILE;

 	update orders s
 	join TempBatch t on s.OrderId = t.OrderId
 	join batches b on concat(t.Batch) = b.batch and DATE_FORMAT(now(), \"%Y%c%d%k%i\") = b.batchtype
	join country c on s.ShipToCountry = c.countrycode
	set s.batchid = b.batchID, s.uniqueidentifier = 'MixedBatch50', readyforupload = now(), grouptype ='MixedBatch50', s.LocalStatus=9
	where s.batchid is null and s.uniqueidentifier is null and c.upscode = 'UPS Saver' ;

delete from TempGrouped;
delete from TempBatch;
delete from TempRunTot;

set CompPass=0;

-- STANDARD
		insert into TempGrouped
		select s.OrderId, s.orderno, s.batchid,
		 group_concat(l.SKU) 'SKU', group_concat(t.Weighting) 'Weighting',	concat('Standard',' ',left(	group_concat(t.Weighting),LENGTH(SUBSTRING_INDEX(Group_concat(t.Weighting), ',', 2)))) 'MainSKU',
		 sum(l.Qty) 'Qty'
		from orders s 		join order_items l on s.OrderId = l.OrderId 		join country c on s.ShipToCountry = c.countrycode		join Temp t on l.SKU = t.SKU
		where grouptype is null and uniqueidentifier is null and c.upscode = 'UPS STANDARD' and s.LocalStatus=1
		group by s.orderno, s.uniqueidentifier, s.grouptype, s.batchid, s.XMLNameREST, c.upscode, s.OrderId
		order by 6,4 asc;

-- Store Cumulative Value
		set @csum := 0;
		insert into TempRunTot
		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , (@csum := @csum + Qty)
		from TempGrouped
		order by MainSKU, SKU asc;

-- The LOOP
		select ceiling(max(RunTot)/ML) from TempRunTot into NumPasses;

		   WHILE CompPass <= NumPasses DO
		 	select ML*CompPass into CurrentRow;

	 			insert into TempBatch
		 		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , concat(convert(CurrentRow/ML,decimal(10,0)),DATE_FORMAT(now(), \"%Y%c%d%k%i\"))
				from TempRunTot
		 		where RunTot between CurrentRow+1 and ML+CurrentRow;

				insert into batches (batch, batchtype)
				select  concat(convert(CurrentRow/ML,decimal(10,0)),DATE_FORMAT(now(), \"%Y%c%d%k%i\")), 	DATE_FORMAT(now(), \"%Y%c%d%k%i\");

			SET CompPass = CompPass + 1;
			END WHILE;

		 	update orders s
		 	join TempBatch t on s.OrderId = t.OrderId
		 	join batches b on concat(t.Batch) = b.batch and DATE_FORMAT(now(), \"%Y%c%d%k%i\") = b.batchtype
			join country c on s.ShipToCountry = c.countrycode
			set s.batchid = b.batchID, s.uniqueidentifier = 'MixedBatch50', readyforupload = now(), grouptype ='MixedBatch50', s.LocalStatus=9
			where s.batchid is null and s.uniqueidentifier is null and c.upscode = 'UPS STANDARD' ;

END";
        DB::unprepared($sql);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V3_GroupMultiOrders');
    }
}
