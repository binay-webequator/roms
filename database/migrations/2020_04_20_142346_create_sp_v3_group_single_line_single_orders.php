<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpV3GroupSingleLineSingleOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V3_GroupSingleLineSingleOrders');

        $sql = "CREATE DEFINER=`octopus`@`%` PROCEDURE `V3_GroupSingleLineSingleOrders`()
    COMMENT 'March 2019 Code - group singles if there are more than 10.  If left then group them into mixed batches of 50'
BEGIN
-- FOR THE LOOP
	DECLARE ML int DEFAULT 50; 			/* max lines will be 50*/
	DECLARE NumPasses int;					/* number of times to loop*/
	DECLARE CompPass int DEFAULT 0;
	DECLARE CurrentRow int DEFAULT 1;

	Select DATE_FORMAT(now(), \"%Y%m%d%H%i\") into @Suffix;

-- (Single orders of more than 10 matches)

	insert into batches (batch, batchtype)
	select concat(c.upscode,'_','SingleSKU_',di.sku,'_',@Suffix), 'Same Single SKU'
	from orders d join order_items di on d.OrderId = di.OrderId join country c on d.ShipToCountry = c.countrycode
    where d.OrderId in (select s.OrderId from orders s join order_items l on s.OrderId = l.OrderId join country c on s.ShipToCountry = c.countrycode where s.batchid is null
	and s.uniqueidentifier is null and s.readyforupload is null and s.uploaded3pl is null and s.LocalStatus=1 group by s.OrderId having count(l.OrderItemId) = 1 and sum(l.Qty) = 1 )
	group by concat(c.upscode,'_','SingleSKU_',di.sku,'_',@Suffix)
	having count(distinct d.OrderId) > 9
	order by count(distinct d.OrderId)  desc;


    -- create shipment temp --
    drop table if exists Temporders;
	CREATE TEMPORARY TABLE Temporders (OrderId bigint) ;
    insert into Temporders (OrderId)
    select d.OrderId from orders d join order_items di on d.OrderId = di.OrderId join country c on d.ShipToCountry = c.countrycode where d.batchid is null
			 and d.uniqueidentifier is null and d.readyforupload is null and d.uploaded3pl is null and d.LocalStatus=1 group by d.OrderId having count(di.OrderItemId) = 1 and sum(di.Qty) = 1  ;

    -- Group Single Item Picks
	   update orders d join order_items di on d.OrderId = di.OrderId join country c on d.ShipToCountry = c.countrycode join batches b on b.batch collate utf8mb4_unicode_ci= concat(c.upscode,'_','SingleSKU_',di.sku,'_',@Suffix)
		set d.uniqueidentifier = concat(c.upscode,'_','SingleSKU_',di.sku,'_',@Suffix), d.batchid = b.batchID ,d.readyforupload = now(), d.grouptype = 'Single SKU', d.LocalStatus=9
	where d.OrderId in (Select OrderId from Temporders);

-- SINGLE LINE SKUS WHERE THERE ARE LESS THAN 10 MATCHES (GROUP INTO BATCHES OF 50)

/*PREP WORK*/
	drop table if exists Temp;
	CREATE TEMPORARY TABLE Temp (SKU varchar(50), Vol int, `Weighting` INT(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`Weighting`)) AUTO_INCREMENT=1;
-- GET A LIST OF SKUS FOR THE ORDERS WHICH MATCH AND ORDER THEM BY THE MOST USED SKU
	insert into Temp (SKU, Vol)
	 select di.sku, count(di.OrderItemId)
	from orders d join order_items di on d.OrderId = di.OrderId join country c on d.ShipToCountry = c.countrycode
	where d.OrderId in (select s.OrderId from orders s join order_items l on s.OrderId = l.OrderId join country c on s.ShipToCountry = c.countrycode where s.batchid is null
										 and s.uniqueidentifier is null and s.readyforupload is null and s.uploaded3pl is null and s.LocalStatus=1 group by s.OrderId having count(l.OrderItemId) = 1 and sum(l.Qty) = 1  )
	group by di.sku
	order by 2 desc;

		drop table if exists TempGrouped;
		CREATE TEMPORARY TABLE TempGrouped (OrderId bigint, OrderNo varchar(50), Batchid int, SKU varchar(1000), `Weighting` varchar(100), MainSKU varchar(50), Qty int);

-- Temp table to store cumulative sum of QTY required to batch into groups of 50 skus
		drop table if exists TempRunTot;
		CREATE TEMPORARY TABLE TempRunTot (OrderId bigint, OrderNo varchar(50), SKU varchar(1000), Weighting varchar(100), MainSKU varchar(50), Qty int , RunTot int);


-- SAVER
		insert into TempGrouped
		select s.OrderId, s.orderno, s.batchid, l.sku, t.Weighting, l.sku 'MainSKU',  sum(l.Qty) 'Qty'
		from orders s join order_items l on s.OrderId = l.OrderId 		join country c on s.ShipToCountry = c.countrycode	join Temp t on l.sku = t.SKU
		where s.OrderId in (select s.OrderId from orders s join order_items l on s.OrderId = l.OrderId 		join country c on s.ShipToCountry = c.countrycode where batchid is null and uniqueidentifier is null and readyforupload is null and uploaded3pl is null and s.LocalStatus=1 group by s.OrderId having count(l.OrderItemId) = 1 and sum(l.Qty) = 1 )
		and c.upscode = 'UPS Saver'
		group by s.OrderId, s.orderno, s.batchid, l.sku, t.Weighting
		order by 4 asc;

-- Store Cumulative Value
		set @csum := 0;
		insert into TempRunTot
		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , (@csum := @csum + Qty)
		from TempGrouped
		order by MainSKU, SKU asc;

		drop table if exists TempBatch;
		CREATE TEMPORARY TABLE TempBatch (OrderId bigint, OrderNo varchar(50), SKU varchar(1000), Weighting varchar(100), MainSKU varchar(50), Qty int , Batch varchar(100));

-- The LOOP /*added 1 to count on 2019-03-09 to see if it resolves the missing batching DM*/
		select ceiling(max(RunTot)/ML) from TempRunTot into NumPasses;

		   WHILE CompPass <= NumPasses DO
		 	select ML*CompPass into CurrentRow;

	 			insert into TempBatch
		 		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , concat(convert(CurrentRow/ML,decimal(10,0)),'_Saver_',DATE_FORMAT(now(), \"%Y%c%d%k%i\")) as 'batch'
				from TempRunTot
			 	where RunTot between CurrentRow+1 and ML+CurrentRow;

				insert into batches (batch, batchtype)
				select  concat(convert(CurrentRow/ML,decimal(10,0)),'_Saver_',DATE_FORMAT(now(), \"%Y%c%d%k%i\")), 	DATE_FORMAT(now(), \"%Y%c%d%k%i\");

			SET CompPass = CompPass + 1;
			END WHILE;

 	update orders s
 	join TempBatch t on s.OrderId = t.OrderId
 	join batches b on concat(t.Batch) = b.batch and DATE_FORMAT(now(), \"%Y%c%d%k%i\") = b.batchtype
	join country c on s.ShipToCountry = c.countrycode
	set s.batchid = b.batchID, s.uniqueidentifier = 'SingleBatch50', readyforupload = now(), grouptype ='SingleBatch50',  s.LocalStatus=9
	where s.batchid is null and s.uniqueidentifier is null and c.upscode = 'UPS Saver' ;


delete from TempGrouped;
delete from TempBatch;
delete from TempRunTot;

set CompPass = 0;

-- STANDARD
		insert into TempGrouped
		select s.OrderId, s.orderno, s.batchid, l.sku, t.Weighting, l.sku 'MainSKU',  sum(l.Qty) 'Qty'
		from orders s join order_items l on s.OrderId = l.OrderId 		join country c on s.ShipToCountry = c.countrycode		join Temp t on l.sku = t.SKU
		where s.OrderId in (select s.OrderId from orders s join order_items l on s.OrderId = l.OrderId join country c on s.ShipToCountry = c.countrycode where batchid is null
										 and uniqueidentifier is null and readyforupload is null and uploaded3pl is null and s.LocalStatus=1 group by s.OrderId having count(l.OrderItemId) = 1 and sum(l.Qty) = 1  )
		and c.upscode = 'UPS Standard'
		group by s.OrderId, s.orderno, s.batchid, l.sku, t.Weighting
		order by 4 asc;

-- Store Cumulative Value
		set @csum := 0;
		insert into TempRunTot
		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , (@csum := @csum + Qty)
		from TempGrouped
		order by MainSKU, SKU asc;

		drop table if exists TempBatch;
		CREATE TEMPORARY TABLE TempBatch (OrderId bigint, OrderNo varchar(50), SKU varchar(1000), Weighting varchar(100), MainSKU varchar(50), Qty int , Batch varchar(100));

-- The LOOP
		select ceiling(max(RunTot)/ML) from TempRunTot into NumPasses;

		   WHILE CompPass <= NumPasses DO
		 	select ML*CompPass into CurrentRow;

	 			insert into TempBatch
		 		select OrderId, OrderNo, SKU, Weighting, MainSKU, Qty , concat(convert(CurrentRow/ML,decimal(10,0)),'_Standard_',DATE_FORMAT(now(), \"%Y%c%d%k%i\")) as 'batch'
				from TempRunTot
			 	where RunTot between CurrentRow+1 and ML+CurrentRow;

				insert into batches (batch, batchtype)
				select  concat(convert(CurrentRow/ML,decimal(10,0)),'_Standard_',DATE_FORMAT(now(), \"%Y%c%d%k%i\")), 	DATE_FORMAT(now(), \"%Y%c%d%k%i\");

			SET CompPass = CompPass + 1;
			END WHILE;

 	update orders s
 	join TempBatch t on s.OrderId = t.OrderId
 	join batches b on concat(t.Batch) = b.batch and DATE_FORMAT(now(), \"%Y%c%d%k%i\") = b.batchtype
	join country c on s.ShipToCountry = c.countrycode
	set s.batchid = b.batchID, s.uniqueidentifier = 'SingleBatch50', readyforupload = now(), grouptype ='SingleBatch50', s.LocalStatus=9
	where s.batchid is null and s.uniqueidentifier is null and c.upscode = 'UPS Standard';
END";
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V3_GroupSingleLineSingleOrders');
    }
}
