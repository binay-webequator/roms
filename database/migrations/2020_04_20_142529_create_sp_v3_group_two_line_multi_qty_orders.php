<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpV3GroupTwoLineMultiQtyOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V3_GroupTwoLineMultiQtyOrders');

        $sql = "CREATE DEFINER=`octopus`@`%` PROCEDURE `V3_GroupTwoLineMultiQtyOrders`()
    COMMENT 'Two SKU Orders'
BEGIN
-- FOR THE LOOP
	DECLARE ML int DEFAULT 50; 			/* max lines will be 50*/
	DECLARE NumPasses int;				/* number of times to loop*/
	DECLARE CompPass int DEFAULT 0;
	DECLARE CurrentRow int DEFAULT 1;

	Select DATE_FORMAT(now(), \"%Y%m%d%H%i\") into @Suffix;

/*PREP WORK - Get list of each unique combination*/
	DROP TABLE IF EXISTS Temp;
	CREATE TEMPORARY TABLE Temp (SKU VARCHAR(500), Vol INT, OrderId bigint, `Weighting` INT(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`Weighting`)) AUTO_INCREMENT=1;
	insert into Temp (SKU, Vol, OrderId)
	SELECT REPLACE(CONCAT(c.upscode,'_','TwoSKU_',GROUP_CONCAT(l.sku),'_',@Suffix),',','|'), 1, s.OrderId
	FROM orders s JOIN order_items l ON s.`OrderId` = l.`OrderId` JOIN country c ON s.ShipToCountry = c.countrycode
	WHERE s.OrderId IN (SELECT s.OrderId FROM orders s JOIN order_items l ON s.OrderId = l.OrderId JOIN country c ON s.ShipToCountry = c.countrycode
				WHERE batchid IS NULL AND uniqueidentifier IS NULL AND readyforupload IS NULL AND uploaded3pl IS NULL and s.LocalStatus=1
				GROUP BY s.OrderId HAVING COUNT(l.OrderItemId) = 2 AND SUM(l.Qty) =2  )
	GROUP BY s.OrderId, c.upscode
	ORDER BY 2 ASC;

/*PREP WORK - work out the order (most common pair)*/
	DROP TABLE IF EXISTS TempT;
	CREATE TEMPORARY TABLE TempT (SKU VARCHAR(500), Vol INT, `Weighting` INT(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`Weighting`)) AUTO_INCREMENT=1;
	INSERT INTO TempT (SKU, Vol)
	SELECT SKU, sum(Vol) from Temp group by SKU order by 2 desc;

/*Clear out groups with less than 10 orders*/
	delete from TempT where vol < 10;

/*Log into Batches Table*/
	INSERT INTO batches (batch, batchtype)
	select SKU, 'TwoLineSKU' from TempT order by Weighting asc;

-- Update orders with UniqueID
	update orders s join order_items l on s.OrderId = l.OrderId join country c on s.ShipToCountry = c.countrycode
			JOIN Temp T on s.OrderId = T.OrderId join batches b on b.batch= T.SKU
		set uniqueidentifier = T.SKU, s.batchid = b.batchID, readyforupload = now(), grouptype = 'Two SKU', s.LocalStatus=9 Where 1;

END";
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS V3_GroupTwoLineMultiQtyOrders');
    }
}
