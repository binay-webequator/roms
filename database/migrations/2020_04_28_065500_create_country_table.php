<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('country');

        Schema::create('country', function (Blueprint $table) {
            $table->string('countrycode', 5)->primary();
            $table->string('countryname', 200);
            $table->string('countryiso', 5)->index('countryiso');
            $table->boolean('pickgroup')->index('pickgroup');
            $table->string('upscode', 50)->nullable();
        });

        DB::unprepared( file_get_contents( __DIR__.DIRECTORY_SEPARATOR."sql".DIRECTORY_SEPARATOR."country.sql" ) );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}
