<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier_mapping', function (Blueprint $table) {
            $table->id();
            $table->string('3pl', 5);
            $table->string('aftership', 10);
        });

        DB::table('courier_mapping')->insert([
                '3pl' => 'DPD',
                'aftership' => 'DPD UK'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_mapping');
    }
}
