<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrackingNumberToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('TrackingNumber',20)->nullable()->index()->after('PoNum');
            $table->string('Carrier',70)->nullable()->after('TrackingNumber');
            $table->string('Mode',50)->nullable()->after('Carrier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('TrackingNumber');
            $table->dropColumn('Carrier');
            $table->dropColumn('Mode');
        });
    }
}
