<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReceivedOnReceivedByTagNotesToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('ReceivedOn')->nullable()->after('Mode');
            $table->text('ReceivedBy')->nullable()->after('ReceivedOn');
            $table->string('Tag',100)->nullable()->after('LastModifiedDate');
            $table->string('TrackingNumber',100)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('ReceivedOn');
            $table->dropColumn('ReceivedBy');
            $table->dropColumn('Tag');
            $table->string('TrackingNumber',20)->change();
        });
    }
}
