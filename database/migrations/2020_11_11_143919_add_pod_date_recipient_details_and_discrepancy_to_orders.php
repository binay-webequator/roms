<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPodDateRecipientDetailsAndDiscrepancyToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dateTime('PODDate')->nullable()->after('Tag');
            $table->string('Recipient',255)->nullable()->after('PODDate');
            $table->string('Details',255)->nullable()->after('Recipient');
            $table->string('Discrepancy',255)->nullable()->after('Details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('PODDate');
            $table->dropColumn('Recipient');
            $table->dropColumn('Details');
            $table->dropColumn('Discrepancy');
        });
    }
}
