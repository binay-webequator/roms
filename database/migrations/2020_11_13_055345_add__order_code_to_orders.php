<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderCodeToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('OrderCode')->nullable()->after('Tag');
            $table->string('OrderType',100)->nullable()->after('OrderCode');
            $table->text('TrackingEvents')->nullable()->after('Discrepancy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('OrderCode');
            $table->dropColumn('OrderType');
            $table->dropColumn('TrackingEvents');
        });
    }
}
