<?php

use Illuminate\Database\Seeder;

class ASNsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\ASN::class, 20)->create()->each(function ($asn) {
            factory(\App\Models\ASNItem::class)->create(['ReceiverId'=>$asn->ReceiverId]);
        });
    }
}
