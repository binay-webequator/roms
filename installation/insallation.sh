#!/bin/bash

sudo yum update -y
sudo yum install git

#install nginx
sudo amazon-linux-extras install nginx1.12

#enable php version
sudo amazon-linux-extras enable php7.4
#install extensions
sudo yum install php-cli php-pdo php-fpm php-json php-mysqlnd php-mbstring php-intl php-xml php-odbc

sudo yum install npm
echo "installing composer"
cd ~
sudo curl -sS https://getcomposer.org/installer | sudo php
sudo mv composer.phar /usr/local/bin/composer
sudo ln -s /usr/local/bin/composer /usr/bin/composer

echo "creating the directory"
sudo mkdir -p /var/www/royale-oms
sudo chown ec2-user:ec2-user /var/www/royale-oms

cd /var/www/royale-oms
echo "closing the bitbucket repo"
git clone https://abdul-webequator@bitbucket.org/webequator/jc-oms-royale.git .
#
echo "copy the evn file"
pc_name=$(hostname)
cp .env.example ".env.${pc_name}"

echo "move the host file to the correct location"
sudo cp installation/nginx-server.conf /etc/nginx/conf.d/royale-oms.conf
#restart the nginx service
sudo service nginx stop
sudo service nginx start

#post installation steps:

#update env.{pc} with credentials following readme file
echo "*************************"
echo "update the env.${pc_name} configs as per instructions in readme file"
echo "*************************"

