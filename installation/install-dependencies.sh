#!/bin/bash
cd /var/www/royale-oms

composer install

php artisan key:generate

php artisan migrate

php artisan passport:install

#sudo useradd -G ec2-user apache
sudo chgrp -R apache storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache

#restart php-fpm
sudo systemctl restart php-fpm
