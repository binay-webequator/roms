#!/bin/bash

#install supervisor
sudo curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo python get-pip.py
sudo pip install supervisor
sudo pip install setuptools

##########
#https://ls3.io/post/supervisor_on_aws_linux_ami/

# Switch over to a root user.

sudo sudo -

# Install the supervisor package.
sudo pip install -U supervisor

# Generate a default config file.
sudo mkdir -p /etc/supervisor/conf.d


sudo echo_supervisord_conf > /etc/supervisor/supervisord.conf

# Add in the setting to look for supervisor app files in /etc/supervisor/conf.d
sudo cat << EOF >> /etc/supervisor/supervisord.conf
[include]
files = /etc/supervisor/conf.d/*.conf
EOF

#copy batching to start supervisor
sudo cp batching.conf /etc/supervisor/conf.d/royale-oms-batch.conf

sudo mkdir -p /var/log/worker

sudo chown apache:apache /var/log/worker

echo "********* start the supervisor ******"
# Start up supervisord (ane make sure no errors)
sudo supervisord

#auto start
#sudo chkconfig --add supervisord
#sudo chkconfig supervisord on

#when new file
supervisorctl reread
supervisorctl update
supervisorctl start royale-oms-batch:*
