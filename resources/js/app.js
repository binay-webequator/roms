
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./filters');

import Vue from 'vue';
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
//import {ServerTable, ClientTable, Event} from 'vue-tables-2';
import settings from "./config/Settings";

//import vSelect from 'vue-select';
import VueDateTimePicker from 'vue-bootstrap-datetimepicker';
import vuetable from 'vuetable-2';
import VModal from 'vue-js-modal';
import JsonCSV from 'vue-json-csv';
import UFileUpload from 'vue-upload-component';

window.Vue = require('vue');
Vue.prototype.$user = window.user;
Vue.prototype.$app = window.application;
Vue.prototype.$settings = settings;
Vue.prototype.$filters = Vue.options.filters
//https://bootstrap-vue.js.org/docs/misc/settings/
Vue.use(BootstrapVue, {
    BModal: {
        titleTag: 'h4',
        okVariant:'success',
        cancelVariant: 'secondary mr-auto'
    }
});
//Vue.component('v-select', vSelect);
Vue.component('datetime-picker', VueDateTimePicker);
Vue.component('vuetable',  vuetable);
Vue.component('downloadCsv', JsonCSV);
Vue.component('file-upload', UFileUpload);

Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: false }, injectModalsContainer: true });
$.extend(true, $.fn.datetimepicker.defaults, {
    icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-arrow-up',
        down: 'fa fa-arrow-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-calendar-check',
        clear: 'fa fa-trash-alt',
        close: 'fa fa-times-circle'
    },
    format: settings.FROMFUllFROMAT,
    showClear: true,
    showClose: true,
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//Vue.component('orders', require('./components/Orders.vue').default);

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Router
 */
import routes from './config/Routes'

Vue.use(VueRouter)

const router = new VueRouter({
    //mode: 'history',
    routes // short for `routes: routes`
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const appname = window.application.name
router.beforeEach((to, from, next) => {
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
    if(nearestWithTitle) {
        document.title = nearestWithTitle.meta.title+' - '+appname;
    }
    return next();
});

//Global variables
import App from './App.vue'

new Vue({
    render: h => h(App),
    router
}).$mount('#app')
