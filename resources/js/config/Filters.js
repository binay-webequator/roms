/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 04/10/2019
 * Time: 19:37
 */
const reportFilters = {batchID: '', threePLID:'', startDate: '', endDate: ''};
var data= [];
data['reports_filters'] = reportFilters;

export default {

    getFilter(id){
        let filters = JSON.parse(localStorage.getItem(id)) || filters[id] != undefined ? filters[id] : {};
        return filters;
    },
    setFilter(id, value){
        localStorage.setItem(id, JSON.stringify(value));
    },
    resetFilter(id){
        let filters = filters[id] != undefined ? filters[id] : {};
        localStorage.setItem(id, JSON.stringify(filters));
    }
}