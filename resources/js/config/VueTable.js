/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 30/09/2019
 * Time: 07:44
 */
export default {
    table: {
        tableWrapper: '',
        tableHeaderClass: '',
        tableBodyClass: '',
        tableClass: 'table table-bordered table-hover table-striped table-condensed',
        loadingClass: 'loading',
        ascendingIcon: 'fa fa-sort-asc pull-right',
        descendingIcon: 'fa fa-sort-desc  pull-right',
        ascendingClass: 'sorted-asc ',
        descendingClass: 'sorted-desc  ',
        sortableIcon: 'fa fa-sort  pull-right',
        detailRowClass: 'vuetable-detail-row',
        handleIcon: 'fa fa-bars text-secondary',
        renderIcon(classes, options) {
            return `<i class="${classes.join(' ')}"></span>`
        }
    },
    pagination: {
        wrapperClass: 'btn-group pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        pageClass: 'btn btn-default',
        linkClass: 'btn btn-default',
        paginationClass: 'pagination',
        paginationInfoClass: 'float-left',
        dropdownClass: 'form-control',
        icons: {
            first: 'fa fa-chevron-left',
            prev: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            last: 'fa fa-chevron-right',
        }
    }
}