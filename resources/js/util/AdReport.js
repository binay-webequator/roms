/**
 * Created by PhpStorm.
 * User: DG
 * Date: 24/10/2019
 * Time: 04:22
 */

export default {
    filter: {
        'sku': '',
        'sku_type': '1',
        'TransactionID': '',
        'Reference': '',
        'AdjustmentType': '',

    },
    get(){
        return JSON.parse(localStorage.getItem('adreport_filters')) || this.filter;
    },
    set(val){
        localStorage.setItem('adreport_filters', JSON.stringify(val));
    },
    reset(){
        localStorage.setItem('adreport_filters', JSON.stringify(this.filter));
    }
}