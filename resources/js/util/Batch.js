/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 07/10/2019
 * Time: 12:54
 */

export default {
    filter: {
        batchID: '',
        threepldid: '',
        dates: '',
        mp: ''
    },
    get(){
        return JSON.parse(localStorage.getItem('batch_filters')) || this.filter;
    },
    set(val){
        localStorage.setItem('batch_filters', JSON.stringify(val));
    },
    reset(){
        localStorage.setItem('batch_filters', JSON.stringify(this.filter));
    }
}