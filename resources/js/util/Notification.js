/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 07/10/2019
 * Time: 12:54
 */

export default {
    filter: {
        'startDate': '',
        'endDate': '',
        'user_name': '',
        'status': '',
    },
    get(){
        return JSON.parse(localStorage.getItem('noti_filters')) || this.filter;
    },
    set(val){
        localStorage.setItem('noti_filters', JSON.stringify(val));
    },
    reset(){
        localStorage.setItem('noti_filters', JSON.stringify(this.filter));
    }
}