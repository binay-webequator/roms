/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 07/10/2019
 * Time: 12:54
 */

export default {
    filter: {
        'startDate': '',
        'endDate': '',
        'keyword': '',
        'orderNo': '',
        'transactionId': '',
        'referenceNo': '',
        'status': '',
    },
    get(){
        return JSON.parse(localStorage.getItem('order_filters')) || this.filter;
    },
    set(val){
        localStorage.setItem('order_filters', JSON.stringify(val));
    },
    reset(){
        localStorage.setItem('order_filters', JSON.stringify(this.filter));
    }
}