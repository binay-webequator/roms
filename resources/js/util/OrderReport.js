/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 07/10/2019
 * Time: 12:54
 */

export default {
    filter: {
        'ostartDate': '',
        'oendDate': '',
        'orderNo': '',
        'sku': '',
        'referenceNo': ''
    },
    get(){
        return JSON.parse(localStorage.getItem('order-report-filter')) || this.filter;
    },
    set(val){
        localStorage.setItem('order-report-filter', JSON.stringify(val));
    },
    reset(){
        localStorage.setItem('order-report-filter', JSON.stringify(this.filter));
    }
}