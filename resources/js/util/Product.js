/**
 * Created by PhpStorm.
 * User: Abdul
 * Date: 07/10/2019
 * Time: 12:54
 */

export default {
    filter: {
        'sku': '',
        'sku_type': '1',
        'name': '',
        'name_type': '1',
        'upc_type': '1',
        'upc': '',
        'status': '',
    },
    get(){
        return JSON.parse(localStorage.getItem('product_filters')) || this.filter;
    },
    set(val){
        localStorage.setItem('product_filters', JSON.stringify(val));
    },
    reset(){
        localStorage.setItem('product_filters', JSON.stringify(this.filter));
    }
}