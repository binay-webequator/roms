@extends('layouts.login')
@section('title', 'Authentication')

@section('content')
    <div class="col-sm-12"><br></div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>{{ config('app.name', 'JC-UK-2020') }} - Authentication</h3>
                <small></small>
            </div>

            <div class="hpanel">
                <div class="panel-body">
                    @if (session('msg') && session('class'))
                        <div class="alert alert-{{session('class')}}" style="margin-bottom: 10px">
                            {{ session('msg') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" placeholder="example@gmail.com" class="form-control @error('email') error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <label class="error" role="alert">
                                {{ $message }}
                            </label>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">{{ __('Password') }}</label>
                            <input id="password" placeholder="******" type="password" class="form-control @error('password') error @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                            <label class="error" role="alert">
                                {{ $message }}
                            </label>
                            @enderror
                        </div>

                        {{--<div class="checkbox">
                            <input class="i-checks" type="checkbox"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="control-label" for="remember" style="padding-left: 5px">
                                {{ __('Remember Me') }}
                            </label>
                        </div>--}}

                        <button type="submit" class="btn btn-success btn-block">
                            {{ __('Login') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}" style="padding-left:0px ">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

