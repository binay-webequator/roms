@extends('layouts.login')
@section('title', 'Forgot Password')
@section('content')

    <div class="col-sm-12"><br></div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>{{config('app.name', 'JC UK')}} - Forgot Password</h3>
                <small></small>
            </div>

             <div class="hpanel">
                <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert" style="margin-bottom: 10px;">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" placeholder="example@gmail.com" class="form-control @error('email') error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <label class="error" role="alert">
                                {{ $message }}
                            </label>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">
                            {{ __('Send Password Reset Link') }}
                        </button>

                        <a class="btn btn-link" href="{{ route('login') }}" style="padding-left:0px ">
                            {{ __('Back to Login') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
        </div>
    </div>

@endsection
