@extends('layouts.login')

@section('content')
    <div class="col-sm-12"><br></div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>{{ config('app.name', 'James Cargo') }} - Password Change</h3>
                <small></small>
            </div>

            <div class="hpanel">
                <div class="panel-body">

                    @if (session('msg') && session('class'))
                        <div class="alert alert-{{session('class')}}" style="margin-bottom: 10px">
                            {{ session('msg') }}
                        </div>
                    @endif

                    @if (!$errors->any() && session()->get('pwd_uid')!="")
                    <div class="alert alert-info" style="margin-bottom: 15px">
                        <p>Your password has expired, please change it.</p>
                    </div>
                    @else
                        @if ($errors->has('404'))
                            <div class="alert alert-danger" style="margin-bottom: 15px">
                                <p>{{$errors->first('404') }}</p>
                            </div>
                        @endif
                    @endif

                    <form method="POST" action="{{ route('password.post_expired') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                                <label for="current_password" class="control-label">Current Password</label>
                                <input id="current_password" type="password" class="form-control" name="current_password" required="" placeholder="Current password">

                                @if ($errors->has('current_password'))
                                <label class="error" role="alert">
                                    {{ $errors->first('current_password') }}
                                </label>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">New Password</label>
                                <input id="password" type="password" class="form-control" name="password" required="" placeholder="New password">

                                @if ($errors->has('password'))
                                    <label class="error" role="alert">
                                    {{ $errors->first('password') }}
                                </label>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="control-label">Confirm New Password</label>
                                 <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required="" placeholder="Confirm new password">

                                @if ($errors->has('password_confirmation'))
                                <label class="error" role="alert">
                               {{ $errors->first('password_confirmation') }}
                                </label>
                                @endif
                            </div>

                            <div class="form-group">

                                    <button type="submit" class="btn btn-success btn-block">
                                        Reset Password
                                    </button>

                            </div>
                        </form>
                        @if(auth()->user())

                                <a class="btn btn-link " href="{{ url('/') }}" style="padding-left:0px ">
                                    <i data-v-2f18ce02="" class="fa fa-arrow-left"></i>  {{ __('Back to dashboard') }}
                                </a>

                        @endif
                </div>
            </div>
        </div>
    </div>
@endsection