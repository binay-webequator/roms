@extends('layouts.login')
@section('title', 'Reset Password')

@section('content')
    <div class="col-sm-12"><br></div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center m-b-md">
                    <h3>{{config('app.name', 'JC UK')}} - Reset Password</h3>
                    <small></small>
                </div>

                <div class="hpanel">
                    <div class="panel-body">
                    <form method="POST" action="{{ route('password.update') }}" >
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email" class="control-label">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control @error('email') error @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <label class="error" role="alert">
                                    {{ $message }}
                                </label>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">{{ __('Password') }}</label>
                            <input id="password" placeholder="******" type="password" class="form-control @error('password') error @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <label class="error" role="alert">
                                    {{ $message }}
                                </label>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" placeholder="******" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>

                        <div class="form-group ">

                            <button type="submit" class="btn btn-success btn-block">
                                {{ __('Reset Password') }}
                            </button>

                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>

@endsection
