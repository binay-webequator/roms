<!-- Vendor scripts -->
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sparkline/index.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert/lib/sweet-alert.min.js') }}"></script>
<script src="{{ asset('assets/vendor/toastr/build/toastr.min.js') }}"></script>

@yield('jsfiles')

<!-- App scripts -->
<script src="{{ asset('assets/scripts/homer.js') }}"></script>
<script type="text/javascript">
    @yield ('scripts')
    var es_label_color = new Array('label-warning', 'label-danger');
    var unexpected_error_msg='{{config('local.unexpected_error_msg')}}';

    // For notification queue in header
    $('document').ready(function(){

        // Toastr options
        toastr.options = {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-top-center",
            "closeButton": true,
            "toastClass": "animated fadeInDown",
            "timeOut": "50000",
        };

    });

</script>
