<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title', 'Welcome') | {{ config('app.name', 'JC-UK-2020') }}</title>

    <!-- Styles -->
    <link href="{{ asset('assets/vendor/fontawesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/metisMenu/dist/metisMenu.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/animate.css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/styles/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert/lib/sweet-alert.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/toastr/build/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/styles/static_custom.css') }}">


    @yield('cssfiles')

    <script type="text/javascript">
        var baseURL = "{{ url('/') }}";
    </script>
</head>
