<nav role="navigation">
    <ul class="nav navbar-nav">
        <li>
            <a href="{{ route('home') }}"> <span class="nav-label">Dashboard</span> </a>
        </li>
        <li>
            <a href="{{ route('orders') }}"> <span class="nav-label">Orders</span> </a>
        </li>
    </ul>
    <div class="mobile-menu">
        <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
            <i class="fa fa-chevron-down"></i>
        </button>
        <div class="collapse mobile-navbar" id="mobile-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a class="" href="profile.html">Profile</a>
                </li>
                <li>
                    <a class="" href="{{ route('logout') }}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="navbar-right">
        <ul class="nav navbar-nav no-borders">
            <li class="dropdown">
                <a id="navbarDropdown" class="dropdown-toggle label-menu-corner nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="font-size:13px; font-weight:bold;">
                    {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu animated flipInX m-t-xs">
                    <!--li><a href="javascript:void(0)" onclick="manageUser({{Auth::user()->id}})">Profile</a></li>
                    <li class="divider"></li-->
                    <li><a href="{{route('logout')}}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a></li>
                </ul>

            </li>
        </ul>
    </div>
</nav>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
