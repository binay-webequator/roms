@include('elements/header')

<body class="">
<div class="color-line"></div>
<div class="error-container">
    <i class="pe-7s-way text-success big-icon"></i>
    <h1>429</h1>
    <strong>Too Many Requests</strong>
    <p>
        Too Many Requests
    </p>
    <a href="{{ route('home') }}" class="btn btn-xs btn-success">Go back to dashboard</a>

</div>
</body>
</html>

