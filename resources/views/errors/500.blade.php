@include('elements/header')

<body class="">
<div class="error-container">
    <i class="pe-7s-way text-success big-icon"></i>
    <h1>500</h1>
    <strong>Internal Server Error</strong>
    <p>
        The server encountered something unexpected that didn't allow it to complete the request. We apologize.
    </p>
    <a href="{{ route('home') }}" class="btn btn-xs btn-success">Go back to dashboard</a>
</div>
</body>
</html>