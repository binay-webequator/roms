@include('elements/header')

<body class="">
    <div class="error-container">
        <i class="pe-7s-way text-success big-icon"></i>
        <h1>503</h1>
        <strong>Site Maintenance</strong>
        <p>
            Hi, {{ config('app.name', 'James Cargo') }} is currently undergoing scheduled maintenance. We should be back shortly. Sorry for the inconvenience!
        </p>

    </div>
</body>
</html>
