@include('elements/header')
<body class="fixed-navbar sidebar-scroll">
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="header">
    <div class="color-line"></div>
    <div id="logo" class="dark-version">
            <span>
                {{ config('app.name', 'James Cargo') }}
            </span>
    </div>

    <!--Top Navigation -->
    @include('elements/top-nav')
</div>

<!--Top Navigation -->
{{--@include('elements/sidebar')--}}

<!-- Main Wrapper -->
<div id="wrapper" style="margin:0px;">
    <div class="content" id="app">
        @yield('content')
    </div>

    <footer class="footer">
        <span class="pull-left">
                <strong>{{ config('app.name', 'James Cargo') }}</strong> - Copyright @ 2020
            </span>
    </footer>
</div>

<!-- Scripts -->
@include('elements/footer')
</body>
</html>
