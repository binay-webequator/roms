@include('elements/header')
<body class="blank">
    <div class="color-line"></div>
    <div class="login-container" id="app">
    @yield('content')
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>{{ config('app.name', 'JC-UK-2020') }}</strong> - Copyright @ 2020
        </div>
    </div>
    <!-- Scripts -->
    @include('elements/footer')
</body>
</html>
