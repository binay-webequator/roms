<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <title>{{ config('app.name', 'James Cargo') }}</title>

    <!-- Styles -->
    <link href="{{ asset('assets/vendor/fontawesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .navbar-nav {
            flex-direction: row;
        }
        .VueTables {
            width: 100%;
        }
        .VueTables__table thead > tr > th{
            border-top: 1px solid #ddd !important;
        }
        .VueTables__search {
            float: right;
            margin-left: 15px;
        }
        .VuePagination {
            padding:0px;
            margin-left: 0px;
        }
        .VuePagination__count {
            width:50%;
            text-align: left !important;
            padding: 0;
        }
        .VuePagination__pagination {
            float: right;
            margin-top: 5px;
        }
        nav.text-center {
            width: 100%;
        }
        .no-padding {
            padding: 0px !important;
        }
        .VueTables__search-field label {
            float: left;
            padding-top: 6px;
            padding-right: 7px;
        }
        .VueTables__limit {
            float: left;
        }
        .VueTables__limit-field label{
            float: left;
            padding-top: 6px;
            padding-right: 7px;
        }
        .table-striped tbody tr:nth-of-type(2n+1) {

            background-color: #f9f9f9;

        }
        #menu {
            width: 250px;
        }
        .hpanel {
            width: 100%;
        }
        .hpanel .hbuilt.panel-heading {
            border-bottom: 1px solid #eaeaea;
        }
        .progress {
            height: 10px;
            margin: 7px 0;
            overflow: hidden;
            background: #e1e1e1;
            z-index: 1;
            cursor: pointer;
        }
        .task-info .percentage{
            float:right;
            height:inherit;
            line-height:inherit;
            font-size: 12px;
        }
        .task-desc{
            font-size:12px;
        }
        .wrapper-dropdown-3 .dropdown li a:hover span.task-desc {
            color:#65cea7;
        }
        .bg-primary {
            background-color:#3490dc!important
        }
        a.bg-primary:focus,
        a.bg-primary:hover,
        button.bg-primary:focus,
        button.bg-primary:hover {
            background-color:#2176bd!important
        }
        .bg-secondary {
            background-color:#6c757d!important
        }
        a.bg-secondary:focus,
        a.bg-secondary:hover,
        button.bg-secondary:focus,
        button.bg-secondary:hover {
            background-color:#545b62!important
        }
        .bg-success {
            background-color:#38c172!important
        }
        a.bg-success:focus,
        a.bg-success:hover,
        button.bg-success:focus,
        button.bg-success:hover {
            background-color:#2d995b!important
        }
        .bg-info {
            background-color:#6cb2eb!important
        }
        a.bg-info:focus,
        a.bg-info:hover,
        button.bg-info:focus,
        button.bg-info:hover {
            background-color:#3f9ae5!important
        }
        .bg-warning {
            background-color:#ffed4a!important
        }
        a.bg-warning:focus,
        a.bg-warning:hover,
        button.bg-warning:focus,
        button.bg-warning:hover {
            background-color:#ffe817!important
        }
        .bg-danger {
            background-color:#e3342f!important
        }
        a.bg-danger:focus,
        a.bg-danger:hover,
        button.bg-danger:focus,
        button.bg-danger:hover {
            background-color:#c51f1a!important
        }
        .bg-light {
            background-color:#f8f9fa!important
        }
        a.bg-light:focus,
        a.bg-light:hover,
        button.bg-light:focus,
        button.bg-light:hover {
            background-color:#dae0e5!important
        }
        .bg-dark {
            background-color:#343a40!important
        }
        a.bg-dark:focus,
        a.bg-dark:hover,
        button.bg-dark:focus,
        button.bg-dark:hover {
            background-color:#1d2124!important
        }
        .bg-white {
            background-color:#fff!important
        }
        .bg-transparent {
            background-color:transparent!important
        }
        .progress-bar {

            background-color: #f1f3f6;
            text-align: right;
            padding-right: 10px;
            color: #6a6c6f;
            padding: 3px 0px;
        }
        .progress .bar {
            z-index: 2;
            height:15px;
            font-size: 12px;
            color: white;
            text-align: center;
            float:left;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            -ms-box-sizing: content-box;
            box-sizing: content-box;
            -webkit-transition: width 0.6s ease;
            -moz-transition: width 0.6s ease;
            -o-transition: width 0.6s ease;
            transition: width 0.6s ease;
        }
        .progress-striped .yellow{
            background:#f0ad4e;
        }
        .progress-striped .green{
            background:#5cb85c;
        }
        .progress-striped .light-blue{
            background:#4F52BA;
        }
        .progress-striped .red{
            background:#d9534f;
        }
        .progress-striped .blue{
            background:#428bca;
        }
        .progress-striped .orange {
            background:#e94e02;
        }
        .progress-striped .bar {
            background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
            background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
            -webkit-background-size: 40px 40px;
            -moz-background-size: 40px 40px;
            -o-background-size: 40px 40px;
            background-size: 40px 40px;
        }
        .progress.active .bar {
            -webkit-animation: progress-bar-stripes 2s linear infinite;
            -moz-animation: progress-bar-stripes 2s linear infinite;
            -ms-animation: progress-bar-stripes 2s linear infinite;
            -o-animation: progress-bar-stripes 2s linear infinite;
            animation: progress-bar-stripes 2s linear infinite;
        }
        @-webkit-keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }
            to {
                background-position: 0 0;
            }
        }
        @-moz-keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }
            to {
                background-position: 0 0;
            }
        }
        @-ms-keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }
            to {
                background-position: 0 0;
            }
        }
        @-o-keyframes progress-bar-stripes {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 40px 0;
            }
        }
        @keyframes progress-bar-stripes {
            from {
                background-position: 40px 0;
            }
            to {
                background-position: 0 0;
            }
        }
        #overlay {
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }
        .modal {
            display: block !important;
            padding: 30px;
            overflow-y: auto !important;
        }
        .modal-backdrop{opacity: 0.4;}
        .dropdown-menu .nav-label {
            color:  #6a6c6f;
        }
        .dropdown-menu .active .nav-label {
            color:#fff;
        }
        .batching .show {
            display: inline-block !important;
        }
        .batching ul {
            width: 100%;
        }
        .batching ul > li {
            padding: 3px 10px;
        }
        /*.shipped-danger a, .cancelled-danger a {
            color:#fff;
        }*/

    </style>

</head>
<body class="fixed-navbar">
    <noscript>
        <div class="" style="text-align: center">
            <h2>You need to enable JavaScript to run this app.</h2>
        </div>

    </noscript>
    <div id="app">
    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <script type="text/javascript">
        window.application = @json(['name'=>config('app.name', 'Royal International China')]);
        window.app_url = "{{url('/')}}";
    </script>
    @auth
        <script>
            window.user = @json(auth()->user());
        </script>
    @endauth
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
