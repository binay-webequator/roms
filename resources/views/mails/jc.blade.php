Hi <i>Administrator</i>,<br/>
<p>Following error occurred ...</p>
<p>
    @if (is_object($emailData['message']))
       @if(!empty($emailData['message']->getFile()))
            File:{{$emailData['message']->getFile()}}<br>
       @endif
       @if(!empty($emailData['message']->getLine()))
           At Line:{{$emailData['message']->getLine()}}<br>
       @endif
       @if(!empty($emailData['message']->getMessage()))
           Error:{{$emailData['message']->getMessage()}}
       @endif
    @else
    {!!$emailData['message']!!}
    @endif

</p>
<br/>
Regards,
<br/>
<i>System Team</i>