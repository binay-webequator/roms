Hi <i>Administrator</i>,<br/>
<p>Upload orders to 3PL by excel file report as below ...</p>

<p>Filename : {{$emailData['message']['file_name']}}</p>
<p>Total Orders : {{$emailData['message']['total_orders']}}</p>
<p>Total Lines : {{$emailData['message']['total_lines']}}</p>
<p>Total Orders Uploaded to 3PL : {{$emailData['message']['total_orders_success']}}</p>
<p>Total Orders fail to Uploaded to 3PL : {{$emailData['message']['total_orders_fail']}}<br>
Waybill : <br><?php
    if($emailData['message']['total_orders_fail'] > 0)
    { echo implode('<br> ',$emailData['message']['order_failed']); } ?>
</p>
<br/>
Regards,
<br/>
<i>System Team</i>
