<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>['auth:api'], 'prefix' => '/v1', 'namespace' => 'API\V1', 'as' => 'api.'], function () {

    Route::get('/orders', 'OrdersController@index')->name('orders.list');
    Route::get('/orders/details/{id}', 'OrdersController@details');
    Route::get('/products', 'ProductsController@index')->name('products.list');
    Route::post('/product-upload-csv', 'ProductsController@productUploadCSV');
    Route::get('/products/update-is-machine-packing', 'ProductsController@updateIsMachinePacking');
    Route::get('/orders/exception-details/{id}', 'OrdersController@details');
    Route::get('/orders/{id}/exceptions', 'OrdersController@getExceptions');
    Route::get('/orders/{id}/pending', 'OrdersController@getPending');
    Route::post('/orders/cancel-order', 'OrdersController@cancelOrder');

    Route::get('/get-action-logs', 'OrdersController@getActionLogs');

    // batch orders and re-queue //
    Route::get('/start-order-batching', 'HomeController@queues');
    Route::get('/get-queue-jobs', 'HomeController@getQueuejobs');
    Route::put('/requeue', 'HomeController@requeue');


    // reports
    Route::get('/report/batches', 'ReportController@batches');

    Route::get('/get-dashboard-data', 'HomeController@dashboard');

    Route::get('/get-dashboard-graph-data', 'HomeController@dashboardGraph');

    Route::get('/get-dashboard-details', 'HomeController@dashboardDetails');


    // reports
    Route::get('/report/orders', 'ReportController@orders');
    Route::get('/get-ageing-batch-order', 'HomeController@getAgeingBatchOrder');
    Route::get('/notifications', 'HomeController@getNotifications');
    Route::post('/import-orders', 'ImportOrdersController@index');

});
