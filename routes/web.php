<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth')->name('home');

Auth::routes();
Auth::routes(['register' => false]);

Route::get('/change-password','Auth\PasswordExpiredController@index')->name('changepasswordform')->middleware('auth');
Route::get('password/expired', 'Auth\PasswordExpiredController@index')->name('password.expired');
Route::post('password/post_expired', 'Auth\PasswordExpiredController@postExpired')->name('password.post_expired');


Route::resource('users', 'UserController');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth'], 'namespace' => 'API\V1'], function () {
    Route::get('/products/download', 'ProductsController@download');
    Route::get('/report/download', 'ReportController@download');
    Route::get('/report/order-download', 'ReportController@orderDownload');
    Route::get('/waybills/download/{filename}', 'ImportOrdersController@download');
    Route::get('/import-orders/download-sample-file', 'ImportOrdersController@downloadSampleFile');
});
