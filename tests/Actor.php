<?php
namespace Tests;

use App\User;

trait Actor {

    public function actAsLogged(){
        $this->actingAs(factory(User::class)->create());
    }
}
