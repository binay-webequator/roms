<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * For login page load
     * @test
     */
    public function checkLoginLoad(){
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    /**
     * Check login with in valid data
     * @test
     */
    public function checkLoginInvalidData(){
        $post = $this->post(route('login'));
        $post->assertStatus(302);
        $response = $this->get(route('login'));
        $response->assertSee('These credentials do not match our records.');
        $response->assertStatus(200);

    }



}
