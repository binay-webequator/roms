<?php

namespace Tests\Feature;

use App\Models\Order;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderDetailsTest extends TestCase
{
    /**
     * Check Details page with valid id and its data
     * @test
     */
    public function checkRecordIDHaveValidData()
    {
        $this->actingAs(factory(User::class)->create(), 'api');
        $order_id=3333;
        $order_data=factory(Order::class)->create(['OrderId'=>$order_id]);
        $response = $this->get('/api/v1/orders/details/'.$order_data['OrderId']);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'OrderId' => $order_id,
            'ExternalId'=>null,
            'Description'=>null
        ]);
    }

}
