<?php

namespace Tests\Feature;

use App\Lib\Order\GetOrders;
use App\Models\Order;
use App\Models\OrderItem;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    protected  $order = null;
    protected $order_data = [
        "OrderId"=>7878787878,
        "ReferenceNum" => '45454545',
        "OrderItems" =>[
            [
                "OrderItemId" => 45454545,
                "OrderId"=>7878787878,
                "Sku"=>'GHGH-GHG-78'
            ]
        ]
    ];

    public function setUp():void
    {
        parent::setUp();
        $this->order = new GetOrders();
    }

    /**
     * test init executed properly
     * @test
     */
    public function testInit()
    {
        $order = $this->mock(GetOrders::class, function ($mock) {
            $mock->shouldReceive('threePLConfigValidation')->andReturn(true); //Check Configuration validation called
            $mock->shouldReceive('getAccessToken')->once(); //Check get access token
            $mock->shouldReceive('getOrders')->once(); //Check get orders called
        })->makePartial();
        $order->init();
    }

    /**
     * For validating 3PL Configuration
     * @test
     */
    public function checkConfigValidations()
    {
        $this->assertTrue($this->order->threePLConfigValidation());
    }

    /**
     * check last modified
     * @test
     */
    public function checkLastModified() {
        $customer_id=14;
        $this->order->threePLConfigValidation();
        factory(Order::class,10)->create(['CustomerId'=>$customer_id]);
        $return1 = Order::getLastUpdatedDate($customer_id);
        $return2=Order::where('CustomerId',$customer_id)->max('LastModifiedDate');
        $return2 = str_replace(' ','T', $return2);
        $this->assertTrue(!empty($return1));
        $this->assertTrue(!empty($return2));
        $this->assertEquals($return1,$return2);
    }

    /**
     * Return false
     * @test
     */
    public function checkLastModifiedEmpty(){
        $customer_id=14;
        $this->order->threePLConfigValidation();
        $r_data = Order::getLastUpdatedDate($customer_id);
        $this->assertTrue(empty($r_data));
    }

    /**
     * For validating access token return access token
     * @test
     */
    public function checkGetAccessToken(){
        $this->assertTrue(true);

    }

    /**
     * validate three pl api params
     * @test
     */
    public function check3plApiParams()
    {
        $this->order->threePLConfigValidation();
        $params = $this->order->createApiParams(14);
        $this->assertStringContainsString('pgsiz=1000', $params['url']);
        $this->assertStringContainsString('pgnum=1', $params['url']);
        $this->assertStringContainsString('CustomerIdentifier.Id==14', $params['url']);

    }
    /**
     * Return data
     * @test
     */
    public function checkGetThreePLData(){
       $customer_id = 14;
       $params = [
         'url'=> '',
         'token' => ''
       ];

       $order = $this->mock(GetOrders::class, function($mock) use($customer_id, $params) {
           $mock->shouldReceive('createApiParams')->with([$customer_id])->andReturn($params);
           $mock->shouldReceive('getThreePLData')->andReturn($params);
           $mock->shouldReceive('saveOrders')->andReturn(true);
       })->makePartial();

       $order->getOrders();

    }

    /**
     * Return no data
     * @test
     */
    public function checkGetThreePLDataEmpty(){
        $this->mock(GetOrders::class, function ($mock) {
            $mock->shouldReceive('getOrders')->with([14])->andReturn(['Status'=>200, 'ResourceList'=>[]]);
        });
    }

    /**
     * return true
     * @test
     */
    public function checkSaveOrderTrue(){

        $order = $this->Mock(GetOrders::class, function ($mock) {
            $mock->shouldReceive('saveOrders')->once()->andReturn(true);
        })->makePartial();

        $order->saveOrders($this->order_data);

        $this->assertTrue($this->order->saveOrders($this->order_data));
        $order_data = factory(Order::class)->create(['OrderId'=>time()]);
        $order_item_data = factory(OrderItem::class)->create(['OrderId'=>$order_data->OrderId]);
        $this->assertDatabaseHas('orders', ['OrderId'=>$order_data->OrderId]);
        $this->assertDatabaseHas('order_items', ['OrderItemId'=>$order_item_data->OrderItemId]);
    }

    /**
     * return false
     * @test
     */
    public function checkSaveOrderFalse(){
        $this->assertFalse($this->order->saveOrders([]));
    }

    /**
     * Listing with valid data
     * @test
     */
    public function checkListingWithValidData(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $order_id=44444;
        factory(Order::class)->create(['OrderId'=>$order_id]);
        $response = $this->get(route('api.orders.list'));
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'OrderId' => $order_id,
            'total'=>1,
            'count'=>1
        ]);
    }

    /**
     * Listing with no data
     * @test
     */
    public function checkListingWithNoData(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $response = $this->get(route('api.orders.list'));
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'total'=>0,
            'count'=>0
        ]);
    }

}
