<?php

namespace Tests\Feature;

use App\Lib\Product\GetProducts;
use App\Models\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\Actor;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use Actor;
    protected  $product = null;
    protected $product_data = [
        [
            "ItemId"=>106881,
            "Sku" => 'SS19A43-BLK-L',
            "CustomerId" =>'24',
            "Upc" =>'0888977526211',
            "ReadOnly"=>[
                "CustomerIdentifier"=>[
                    "Id"=>14
                ],
                "LastModifiedDate" => '2020-04-10 12:12:12'
            ]
        ]
    ];

    public function setUp():void
    {
        parent::setUp();
        $this->product = new GetProducts();
    }

    /**
     * For validating 3PL Configuration
     * @test
     */
    public function checkConfigValidations()
    {
        $this->assertTrue($this->product->threePLConfigValidation());
    }

    /**
     * check last modified
     * @test
     */
    public function checkLastModified() {
        $customer_id=14;
        $this->product->threePLConfigValidation();
        factory(Product::class,10)->create(['CustomerId'=>$customer_id]);
        $return1 = Product::getLastModifiedDate($customer_id);
        $return2=Product::where('CustomerId',$customer_id)->max('LastModifiedDate');
        $return2 = str_replace(' ','T', $return2);
        $this->assertTrue(!empty($return1));
        $this->assertTrue(!empty($return2));
        $this->assertEquals($return1,$return2);
    }

    /**
     * Return false
     * @test
     */
    public function checkLastModifiedEmpty(){
        $customer_id=14;
        $this->product->threePLConfigValidation();
        $r_data = Product::getLastModifiedDate($customer_id);
        $this->assertTrue(empty($r_data));
    }

    /**
     * Return data
     * @test
     */
    public function checkGetThreePLData(){
        //$this->assertTrue(true);
        $this->mock(GetProducts::class, function($mock) {
            $cpram= [
                "url" => "https://secure-wms.com/customers/14/items?pgsiz=100&pgnum=1&LastModifiedDate=2020-04-15T16:00:19",
                "token" => "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOiIxNTg4MTI5ODE1IiwiaXNzIjoiaHR0cDovL3NlY3VyZS13bXMuY29tL0F1dGhTZXJ2ZXIiLCJhdWQiOiJo
dHRwOi8vc2VjdXJlLXdtcy5jb20iLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJPcmRlckltcG9ydCBPcmRlckVkaXQ
gT3JkZXJWaWV3IEN1c3RvbWVyRWRpdCBDdXN0b21lclZpZXcgSW52ZW50b3J5RGV0YWlsVmlldyBJdGVtZWRpdCBJdGVtdmlldyBSZWNlaXZlckVkaXQgUmVjZWl2ZXJWaWV3IFBPRWRpdC
BQT1ZpZXcgUmVhZFByb3BlcnRpZXNUaGlyZFBhcnR5IEZhY2lsaXR5RWRpdCBGYWNpbGl0eVZpZXciLCJodHRwOi8vd3d3LjNwbENlbnRyYWwuY29tL0F1dGhTZXJ2ZXIvY2xhaW1zL3VzZ
XJpbmZvIjoiZXlKRGJHbGxiblJKWkNJNk5EVTNMQ0pEYkdsbGJuUWlPaUpVVUV3eE5USXhVMUZNUkdGMFlXSmhjMlVpTENKVWFISmxaVkJzUjNWcFpDSTZJakEzTW1KbFpHUXlMV1ZtTkRJ
dE5EUTJOaTA0TmpreUxUSmtaV1ZsTm1ReU1qYzFZU0lzSWxSb2NtVmxVR3hKWkNJNk1UVXlNU3dpVlhObGNreHZaMmx1U1dRaU9qSjkifQ.yWwpEBRij9Ba0WV4KhwRycE3NHGwihu-nKGo
fdIyZQc"];

            $mock->shouldReceive('set3PLApiParam')->with([14])->andReturn($cpram);
            //$mock->shouldReceive('getThreePLData')->with([14])->andReturn(['url',]);

        })->makePartial();

    }

    /**
     * Return no data
     * @test
     */
    public function checkGetThreePLDataEmpty(){
        $this->assertTrue(true);
    }

    /**
     * return true
     * @test
     */
    public function checkSaveProductTrue(){
        //$this->assertTrue($this->product->saveProduct($this->product_data));
        $product=factory(Product::class)->create();
        $this->assertDatabaseHas('products',['ItemId'=>$product->ItemId]);
    }

    /**
     * return false
     * @test
     */
    public function checkSaveProductFalse(){
        $this->assertFalse($this->product->saveProduct([]));
    }

    /**
     * Listing with valid data
     * @test
     */
    public function checkListingWithValidData(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $product_id=111222;
        factory(Product::class)->create(['ItemId'=>$product_id]);
        $response = $this->get(route('api.products.list'));
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'ItemId' => $product_id,
            'total'=>1,
            'count'=>1
        ]);
    }

    /**
     * Listing with no data
     * @test
     */
    public function checkListingWithNoData(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $response = $this->get(route('api.products.list'));
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'total'=>0,
            'count'=>0
        ]);
    }

}
