<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\OrderItem;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReportTest extends TestCase
{
    /**
     * For report order listing
     * @test
     */
    public function reportOrderListing(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $orderitemid=4444;
        $order_id=55555;
        $odata=factory(Order::class)->create(['OrderId'=>$order_id]);
        $oidata=factory(OrderItem::class)->create(['OrderItemId'=>$orderitemid,'OrderId'=>$odata['OrderId']]);
        $response = $this->get('/api/v1/report/orders');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'OrderId'=>$odata['OrderId'],
            'OrderItemId'=>$orderitemid,
            'total'=>1
        ]);
    }

    /**
     * For report order download
     * @test
     */
    public function reportOrderDownload(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $orderitemid=4444;
        $order_id=55555;
        $odata=factory(Order::class)->create(['OrderId'=>$order_id]);
        factory(OrderItem::class)->create(['OrderItemId'=>$orderitemid,'OrderId'=>$odata['OrderId']]);
        $response = $this->get('/report/order-download');
        $response->assertStatus(200);

    }

    /**
     * For report batch listing
     * @test
     */
    public function reportBatchListing(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $batch_id=176384262;
        $odata=factory(Order::class)->create(['BatchID'=>$batch_id]);
        $response = $this->get('/api/v1/report/batches');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'BatchID'=>$odata['BatchID'],
            'total'=>1
        ]);
    }

    /**
     * For report batch download
     * @test
     */
    public function reportBatchDownload(){
        $this->actingAs(factory(User::class)->create(), 'api');
        $batch_id=176384262;
        $order_id=14546836;
        factory(Order::class)->create(['OrderId'=>$order_id,'BatchID'=>$batch_id]);
        $response = $this->get('/report/download');
        $response->assertStatus(200);
    }

}
