const mix = require('laravel-mix');
//const TargetPlugins = require('targets-webpack-plugin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.extract();
/*mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .webpackConfig(webpack => {
        return({
            resolve: {extensions: ['.js']},
            plugins:[
                new TargetPlugins({
                    browsers: ['last 2 versions', 'chrome >= 41', 'IE 11']
                })
            ]
        })
    });*/

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .version();